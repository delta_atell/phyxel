# Phyxel Engine (v1.3)

![logo-demo](https://gitlab.com/delta_atell/phyxeldemos/-/raw/main/screenshots/phyxel-logo-demo.png?ref_type=heads)

- [What is Phyxel (list of features)](#phyxel-engine-v13)
- [Demos](#demo)
- [How to install](#how-to-install)
- [How to set things up](#how-to-set-things-up)
- [Building a basic simulation](#basic-simulation)
- [Screenshots (very cool)](#screenshots)
- [License, usage & contact](#license-usage--contact)

Phyxel is an open-source 2D physics engine that allows you to run cellular physics simulations (what we call phyxel physics) easily. It is portable and written entirely in C++. You can create games that are similar to the famous detailed world-simulating computer games, as well as cellular automata physics simulations. The set of features includes:
- Basic behaviour of gases, powders, liquids, and solid materials. There are no pre-defined materials, so you can have any set of materials you would like. You define the materials yourself and can provide different masses and properties.
- Temperature system with possible state transitions.
- Adjustable chemistry system that allows detailed interactions between any materials of your choice. Chemical reactions can have different probabilities and have a temperature threshold.
- Custom discrete force field. This means gravity can point in 8 directions. Furthermore, the direction is defined for each phyxel locally.
- Burning simulation.
- Some (physically inaccurate yet good-looking) imitation of electricity.
- Rigid body system that is compatible with every system above. This part of our engine uses the [Box2D](https://box2d.org/documentation/index.html) library to calculate interactions between rigid bodies, so you'll need to have it installed for this feature. Archimedes' force, the chemistry between rigids and things like that are supported.
- Huge space for customisation: we allow the user to put custom data and insert custom routines into our update functions without touching the inner code of the Phyxel Engine.
- [added in v1.1] Smart update system based on chunks. As a user, you might not want to dive deep into this, but we now have a "smart system" that determines which regions of the scene to update and ignores everything else for the sake of optimisation.
- [added in v1.2] It's possible to run updates on different chunks in parallel with `updateAllParallel`.
- [added in v1.3] It's possible to create a (possibly) much bigger world attached to a scene `WorldManager`. The world is saved by chunks to the disk and allows moving the scene around. This system can also be used to save simulation data and the state of the scene to the disk and load it back when needed. This part of our engine requires <nlohmann/json.hpp> and the C++17 standard. 
- And many more...

Besides that, most of these systems can be turned off in the config file, so that they're excluded at compile time. So **the engine will, for instance, also work without Box2D if you simply disable the rigid body system**.

For more features and updates, check out release messages in the [Releases](https://gitlab.com/delta_atell/phyxel/-/releases) tab in the repository.

## [DEMO](https://roma160.github.io/phyxel_gui/)

Thanks to [Roman Mishchuk](https://github.com/roma160), there's a small [web-based sandbox built entirely with the Phyxel Engine v1.0](https://roma160.github.io/phyxel_gui/). Despite this being a tiny fraction of what one can expect to build using our engine, we are extremely grateful for his contribution, for this is a wonderful demo that works in your web browser.

You can see the [Screenshots](#screenshots) section for more feature demonstration.

If you want a full working example with almost all features demonstrated, you can take a look at [our test simulation — a very messy .cpp file that compiles into a sandbox simulation "game" with a bunch of modes to test every possible system](https://gitlab.com/delta_atell/phyxeldemos/-/blob/main/test-sandbox/maintest.cpp?ref_type=heads). This is what we use to test the engine. It's not well documented but it's usable if you want to read a code example of how the Phyxel Engine can be used to its (almost) full capacity. For a simpler code example, see [Building a basic simulation](#basic-simulation).

## How to install

There's basically no installation. In order to install Phyxel Engine you just need to download a version of it as a source code from the [releases section](https://gitlab.com/delta_atell/phyxel/-/releases) of our repo. We suggest that you only use the **versions marked as stable**. Then unpack it into your project folder:
```
YourProjectFolder/
    ├ Phyxel/
    │    ├ Core/
    │    │   ...
    │    ├ Entities/
    │    │    ...
    │    ├ Maths/
    │    │    ...
    │    │ ...
    │    ├ phyxel.hpp
    │    ├ phyxel_config.h
    │    └ ...
    ├ your_main_file.cpp
    └ ...
```
Now simply include the engine's header file into the main file of your project (or wherever needed):
```C
#include "%YOUR_RELATIVE_PATH_TO_IT%/phyxel.hpp"
```

## How to set things up

In general, you should only change the `phyxel_config.h` file. Everything else in the Phyxel Engine should just work based on these configurations. For instance, this is the config setup that we used for the example sandbox simulation in the section below:

```C
/* file phyxel_config.h from the Phyxel Engine */

...
#define PHX_ENABLE_CHEMISTRY 1 // enabled
#define PHX_ENABLE_CUSTOMGRAVITY 0 // disabled
#define PHX_ENABLE_TEMPERATURE 0
#define PHX_ENABLE_ELECTRICITY 0
#define PHX_ENABLE_BURNING 0
#define PHX_ENABLE_RIGIDBODIES 0

// technical
#define PHX_ENABLE_PARALLEL_UPDATE 0 // enabling this allows you to use parallel update methods, but might require linking the thread library
...
```

## Basic simulation

The code snippet below provides an example of how one can implement a simple simulation sandbox game with Phyxel Engine and SFML for graphics. Phyxel does not have a built-in graphics system in order to remain flexible and independent, so you can use whatever graphics library you wish (like OpenGL).

The following program is basically a window, in which you can set different materials to phyxels with your mouse, select different materials with 0-5 number keys, and pause the simulation with space if needed. You can see how easy it is to define different materials and chemical reactions between them (here, for example, acid will dissolve metal and produce some gas). This simulation can easily be extended to include temperature, electricity, rigid bodies, and other Phyxel's features as well.

```C++
// Example code: Basic sandbox game with Phyxel Engine v1.3 using SFML graphics
// it's suggested to disable rigid bodies, burning, electricity, and temperature in the config file for this example (see above)
#include "%YOUR_RELATIVE_PATH_TO_IT%/phyxel.hpp" // this code is written for Phyxel Engine v1.3
#include <SFML/Graphics.hpp> // in this example, we use sfml for graphics, but you can use whatever you want

int main() {
    phx::Scene scene; // define scene
    // define materials
    auto air = phx::MaterialsList::addMaterial("breathable air", 1, PHX_MTYPE_GAS, phx::Color(0,0,0,0));
        air->isRemovable = true;
    auto sand = phx::MaterialsList::addMaterial("dry sand", 4, PHX_MTYPE_POD, phx::Color(255,225,180));
        sand->addColor(phx::Color(245,245,170));
        sand->addColor(phx::Color(190,175,170));
    auto water = phx::MaterialsList::addMaterial("liquid water", 3, PHX_MTYPE_LIQ, phx::Color(50,150,255));
        water->viscosity = 3000;
    auto metal = phx::MaterialsList::addMaterial("unknown metal", 5, PHX_MTYPE_SOL, phx::Color(170,160,170));
    auto toxic_gas = phx::MaterialsList::addMaterial("toxic gas", 0.4, PHX_MTYPE_GAS, phx::Color(155,255,0,60));
    auto acid = phx::MaterialsList::addMaterial("mysterious acid", 3, PHX_MTYPE_LIQ, phx::Color(100,255,100));
        acid->addReaction(metal, air, toxic_gas);

    // this is important. otherwise you're going to have a segfault right at the start
    scene.fill(air);

    // some variables for the UI
    char materialID = 0;
    bool mousePressed = false;
    bool play = true;

    sf::RenderWindow window(sf::VideoMode(PHX_SCENE_SIZE_X, PHX_SCENE_SIZE_Y), "PhyxelEngine 1.3");
    sf::RectangleShape pixelRenderer(sf::Vector2f(1, 1));

    while (window.isOpen()) { // window loop
        window.setFramerateLimit(30);
        // handle sfml events
        sf::Event event;
        while (window.pollEvent(event)) { 
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyPressed) {
                if (sf::Keyboard::Num0 <= event.key.code && event.key.code <= sf::Keyboard::Num5) // you can select materials by pressing keys 0-5
                    materialID = event.key.code - sf::Keyboard::Num0;
                else if (sf::Keyboard::Space == event.key.code) // hit space for pause
                    play = !play;
            } else if (event.type == sf::Event::MouseButtonPressed) {
                if (sf::Mouse::Left == event.key.code)
                    mousePressed = true;
            } else if (event.type == sf::Event::MouseButtonReleased) {
                if (sf::Mouse::Left == event.key.code)
                    mousePressed = false;
            }
        }
        
        if (mousePressed) { // if mouse is pressed this is going to put the selected material there
            auto mousePos = sf::Mouse::getPosition(window);
            auto windowSize = window.getSize();
            float y = mousePos.y/(float)windowSize.y*PHX_SCENE_SIZE_Y;
            float x = mousePos.x/(float)windowSize.x*PHX_SCENE_SIZE_X;
            auto m = phx::MaterialsList::get(materialID+1);
            scene.setMaterial(x, y, m, rand());
        }

        if (play) // update physics
            scene.updateAll();

        // draw phyxels (this doesn't handle particles and other visuals)
        window.clear();
        for (unsigned j = 0; j < PHX_SCENE_SIZE_X; ++j) 
            for (unsigned i = 0; i < PHX_SCENE_SIZE_Y; ++i) { // loop through all phyxels
                phx::Color c = scene.getColor(j, i);
                pixelRenderer.setFillColor(sf::Color(c.r, c.g, c.b, c.a));
                pixelRenderer.setPosition(j, i);
                window.draw(pixelRenderer);
            }
        pixelRenderer.setPosition(0,0);
        window.display();
    }
    return 0;
}
```

Unfortunately, it is impossible to demonstrate in a short `README.md` file the huge amount of work and all the features we've put into this hobby project of ours to provide such a wide variety of systems that do not break when turned on simultaneously. We encourage the curious to explore the engine through the code documentation themselves, as we do not have the capacity to write full tutorials to all the features we have (believe me, **the number of possibilities for different game mechanics we provide in these several thousands lines of code is beyond your imagination**). We will be happy to answer any questions in the issues, in case someone decides to make use of our Phyxel Engine.

## Screenshots

Here are some screenshots of the test simulations we've built in Phyxel. These don't have any fancy graphics that you can obviously add yourself if you wish.

1) Waterfall when removing a wall of a container filled with water:

![waterfall](https://gitlab.com/delta_atell/phyxeldemos/-/raw/main/screenshots/phyxel-waterflow.png?ref_type=heads)

2) Comically green acid going through some metal block:

![acid](https://gitlab.com/delta_atell/phyxeldemos/-/raw/main/screenshots/phyxel-acid-through-metal.png?ref_type=heads)

3) This is how oil can burn. This actually produces some CO2 (that you can see in the image) and heats up the environment. You can literally **boil** water with this without programming much additionally, just define the boiling point of water:

![burning-oil](https://gitlab.com/delta_atell/phyxeldemos/-/raw/main/screenshots/phyxel-burning-oil.png?ref_type=heads)

4) Here you can see some cartoonish lightning passing through the metal walls (its starting point has been set on the right). As water also has some conductivity in this simulation, the charge also goes through it. It then travels even more to the left and passes through some metal blocks (rigid bodies can also conduct electricity if their material allows them to):

![electricity](https://gitlab.com/delta_atell/phyxeldemos/-/raw/main/screenshots/phyxel-electricity-with-water-and-metal-cubes.png?ref_type=heads)

## License, Usage & Contact

MIT License, Atell Krasnopolski, Petro Zarytskyi, 2023-2025.

That is, you are free to use Phyxel Engine in any commercial or public projects of yours for any purpose, **but please** mention Phyxel Engine in your creation if you do so. 

We would also be happy to have a page listing our users' projects built using Phyxel Engine later to help you promote your work and help us show the usability of the engine. So feel free to [contact](https://gitlab.com/delta_atell) us to let us know about your project or with any questions regarding the usage of Phyxel Engine. 