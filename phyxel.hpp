// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this is the main includable file of our incredible library
// everything is wrapped into the phx namespace
#pragma once
#include <cstdlib>
#include <cstdarg>
#include <string>
#include <vector>
#include <map>
#include <functional>

#include "phyxel_config.h"
#include "Maths/phyxel_maths.hpp"

#define PRIVATE_DEF(D) private: D; public: // for code clarity, defines private fields in the middle of the public space.
#define NBR_DOWN neighbours[0]
#define NBR_RIGHT neighbours[1]
#define NBR_UP neighbours[2]
#define NBR_LEFT neighbours[3]
#if PHX_ENABLE_CUSTOMGRAVITY
    #define PHX_FORCE_DOWN 0
    #define PHX_FORCE_DOWNRIGHT 1
    #define PHX_FORCE_RIGHT 2
    #define PHX_FORCE_UPRIGHT 3
    #define PHX_FORCE_UP 4
    #define PHX_FORCE_UPLEFT 5
    #define PHX_FORCE_LEFT 6
    #define PHX_FORCE_DOWNLEFT 7
#endif
#if PHX_ENABLE_ELECTRICITY
    #define PHX_CHARGE_DIR_NODIR 0
    #define PHX_CHARGE_DIR_DOWN 1
    #define PHX_CHARGE_DIR_DOWNRIGHT 2
    #define PHX_CHARGE_DIR_RIGHT 3
    #define PHX_CHARGE_DIR_UPRIGHT 4
    #define PHX_CHARGE_DIR_UP 5
    #define PHX_CHARGE_DIR_UPLEFT 6
    #define PHX_CHARGE_DIR_LEFT 7
    #define PHX_CHARGE_DIR_DOWNLEFT 8
#endif
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
#endif

// main phyxel files
#include "Core/Color.hpp"
#include "Core/Material.hpp"
#include "Core/MaterialsList.hpp"
#include "Core/PhyxelData.hpp"
#if PHX_ENABLE_WORLD_SAVE // requires nlohmann/json.hpp to be installed and linked. also requires C++17
    #include <sstream>
    #include <fstream>
    #include <filesystem>
    #include <type_traits>
    #include <nlohmann/json.hpp>
    #include "Core/World.hpp"
#endif
#include "Entities/Entity.hpp"
#include "Entities/Particle.hpp"
#include "Entities/Objects.hpp"
#include "Core/Scene.hpp"
#include "Core/Utils.hpp"
#include "Entities/EntityUtils.hpp"
#if PHX_ENABLE_CHEMISTRY
    #include "Core/Chemistry.hpp"
#endif
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for Box2D v2.4.1)
    #include "Entities/RigidBodyUtils.hpp"
#endif
