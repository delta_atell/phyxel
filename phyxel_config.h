// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// do not delete any definitions from this file. the values can be changed freely unless told otherwise.

// sizes
#define PHX_NUM_CHUNKS_X 2
#define PHX_NUM_CHUNKS_Y 2
#define PHX_CHUNK_SIZE_X 64 // this should only be a power of 2
#define PHX_CHUNK_SIZE_Y 64 // this should only be a power of 2
#define PHX_SCENE_SIZE_X (PHX_CHUNK_SIZE_X*PHX_NUM_CHUNKS_X) // do NOT change
#define PHX_SCENE_SIZE_Y (PHX_CHUNK_SIZE_Y*PHX_NUM_CHUNKS_Y) // do NOT change

// physics
/// you can set these to 0 to disable certain Phyxel's subsystems if you don't need them, as these features are computationally heavy
#define PHX_ENABLE_CHEMISTRY 1
#define PHX_ENABLE_CUSTOMGRAVITY 1
#define PHX_ENABLE_TEMPERATURE 1
#define PHX_ENABLE_ELECTRICITY 1 // only an imitation, not physically accurate
#define PHX_ENABLE_BURNING 1
#define PHX_ENABLE_RIGIDBODIES 0 // requires Box2D to be installed and linked (designed for Box2D v2.4.1). set to 0 if you don't have it

// technical
/// set to 0 to disable
#define PHX_ENABLE_WORLD_SAVE 0 // requires <nlohmann/json.hpp> and the C++17 standard. disable it to be able to use C++11 or C++14 and compile without the JSON package. enabling this allows using WorldManager (defined in `World.hpp`) to save/load your scene to/from the disk. enabling this also allows the creation of JSON objects from some Phyxel classes.
#define PHX_ENABLE_PARALLEL_UPDATE 1 // you can still call the normal serial update functions if this is enabled. turning this off prevents the definition of anything related to parallelism, so that you could disable it in case of any issues. turning this on might require linking the thread library. see `Scene::updateAllParallel` defined in "Core/Utils.hpp"

/// additional parameters (please use float literals, i.e. 1.0f instead of 1.0, when applicable)
#define PHX_GAS_DIFF 400 // 0-1023
#define PHX_LIQ_DIFF 400 // 0-1023
#if PHX_ENABLE_TEMPERATURE // additional temperature settings that are only defined if the temperature system is enabled
    #define PHX_MAX_TEMP 1500.0f // max temperature
    #define PHX_DEFAULT_TEMP 22.7f // default temperature is used to initialise scene.envTemperature
    #define PHX_ENABLE_ENV_TEMP 0 // if this parameter is 1 the temperature of the system would gradually return to PHX_DEFAULT_TEMP if nothing happens (enabling this affects performance and turns off smart updates for temperature). setting this to 0 forbids this behaviour and allows smart updates for the temperature system.
    #define PHX_MIN_TEMP -273.15f // min temperature
    #define PHX_MIN_DELTA_TEMP 0.1f // temperatures that differ by less than this constant are considered equal and the corresponding phyxels don't exchange heat. this is used in smart updates
    #if PHX_ENABLE_BURNING
        #define PHX_BURNING_HEAT 30 // the amount of heat every burning phyxel produces each frame
    #endif
    #if PHX_ENABLE_ELECTRICITY
        #define PHX_ELECTRIC_HEAT 2 // the amount of heat every phyxel with electric current produces each frame
    #endif
#endif
#if PHX_ENABLE_RIGIDBODIES // additional rigid body system settings
    #define PHX_BOX2D_FREEFALL_ACCELERATION 15.0f
    #define PHX_METERS2PHYXELS 5.0f // Phyxel Engine tries to wrap Box2D as much as possible, so we try to only use phyxel-wise coordinates on the user side not to confuse the user
    #define PHX_PHYXELS2METERS (1.0f / PHX_METERS2PHYXELS) // do not change, change PHX_METERS2PHYXELS instead
#endif
#define PHX_FREEFALL_ACCELERATION 4.0f // used for particles' movement
#if PHX_ENABLE_ELECTRICITY // additional electricity settings
    #define PHX_ELECTRIC_SPREAD 10 // this should not exceed half a chunk width or height!
#endif
