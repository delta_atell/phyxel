// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file defines what a Phyxel material is. see MaterialsList for creating new materials

#pragma once
#include <string>
#include <vector>
#include <map>
#include <functional>

#if PHX_ENABLE_CHEMISTRY
    #include "Chemistry.hpp"
#endif
#include "Color.hpp"
#include "CustomFields.hpp"

#define PHX_MTYPE_GAS 1
#define PHX_MTYPE_LIQ 2
#define PHX_MTYPE_POD 4
#define PHX_MTYPE_SOL 8

namespace phx {

class Scene; // defined in "Core/Scene.hpp"
class Material;
// ONLY USE THIS TYPE TO REFER TO MATERIALS:
using MaterialObj = Material*; // safe pointer to the unique definition of a Phyxel Material
                               // do NOT use delete on these

/// this class describes the structure of a material
/// a single instance of this class describes a material
class Material {
private:
    // creation this way is prohibited. use MaterialsList::addMaterial(...) to create materials instead (defined in "Core/MaterialsList.hpp")
    Material() = delete; 
    Material(const Material&) = delete;
    Material(Material&&) = delete;
    Material& operator=(const Material&) = delete;
    Material& operator=(Material&&) = delete;

    unsigned id; // corresponds to the index in the global list of materials
protected:
    Material(unsigned id, const std::string& name, float mass, uint8_t type,
        #if PHX_ENABLE_TEMPERATURE
        float thermalDiff,
        #endif
        const Color& color,
        #if PHX_ENABLE_BURNING
        bool isFlammable=false,
        #endif
        bool isPassive=false,
        bool isRemovable=false
    ):
        id(id),
        name(name),
        mass(mass),
        invMass(1/mass),
        type(type),
        isPassive(isPassive)
        #if PHX_ENABLE_TEMPERATURE
        ,thermalDiff(thermalDiff),
        lowerTempState(this),
        higherTempState(this),
        stateMinTemp(PHX_MIN_TEMP),
        stateMaxTemp(PHX_MAX_TEMP)
        #endif
        #if PHX_ENABLE_CHEMISTRY
        ,chemTable(NULL)
        #endif
        #if PHX_ENABLE_BURNING
        ,isFlammable(isFlammable)
        #endif
        ,isRemovable(isRemovable)
    {
        colors.push_back(color);
    }
public:
    ~Material() {
        #if PHX_ENABLE_CHEMISTRY
        if (chemTable) delete chemTable;
        #endif
    }

    // although the fields are made public, we suggest not changing them
    // main fields
    std::string name;
    float mass;
    float invMass; // stores 1 / mass, used to avoid such division
    uint8_t type;
    uint16_t viscosity = 0; // 8191 is the max viscosity (almost doesn't flow at all)
    bool isPassive; // only used for movement updates, other systems still work with passives
    bool isRemovable; // removable material can be removed if they become a virtual particle and are looking for a place. this should be FALSE FOR NON-GASES. basically, put true here only for very common gases.
    std::function<void(Scene*, unsigned, unsigned, bool)> customUpdate = [](Scene* scene, unsigned X, unsigned Y, bool loopDir){return;}; // a functor to handle custom update routine
    unsigned getID() {
        return id;
    }

    // electricity
    #if PHX_ENABLE_ELECTRICITY
    float resistance = 2.0f; // if C is the number of steps a charge makes in this environment, resistance should be 1/C. or more than 1 if it's not conductive at all. use setConductivity for simplicity

    void setConductivity(float C) { // C is the number of steps a charge makes in this environment
        if (C <= 0) resistance = 2.0f;
        else resistance = 1.0f/C;
    }
    #endif

    // state transition & temperature
    #if PHX_ENABLE_TEMPERATURE
    float thermalDiff; // thermal diffusivity (0-0.5)
    float stateMinTemp;
    float stateMaxTemp;
    MaterialObj lowerTempState;
    MaterialObj higherTempState;
    virtual void setStateTransitions(float stateMinTemp, float stateMaxTemp, MaterialObj lowerTempState, MaterialObj higherTempState) {
        this->stateMinTemp = stateMinTemp;
        this->stateMaxTemp = stateMaxTemp;
        this->lowerTempState = lowerTempState;
        this->higherTempState = higherTempState;
    }
    #endif

    // visuals
    std::vector<Color> colors;
    virtual Color getColor(unsigned i) {
        return colors[i];
    }
    virtual void addColor(const Color & color) {
        colors.push_back(color);
    }

    // chemistry
    #if PHX_ENABLE_CHEMISTRY
    std::map<MaterialObj, ChemicalReaction>* chemTable;
    
    virtual void addReaction(
        MaterialObj target, // which material to react with
        MaterialObj newSelf, // change self to this
        MaterialObj newOther, // change other to this
        #if PHX_ENABLE_TEMPERATURE
        float deltaSelfTemperature=0, // add this to the temperature
        float deltaOtherTemperature=0, // add this to the temperature
        float requiredTemperature=PHX_MIN_TEMP, // temperature required to be in self for the reation to start
        #endif
        uint16_t prob=8191 // probability of the reaction (0-8191)
    ) { // adds a chemical reaction to this material. if a chemical reaction involves two materials, it's enough to add this reaction to one of them
        if (!chemTable) chemTable = new std::map<MaterialObj, ChemicalReaction>();
        struct ChemicalReaction reaction = {
            newSelf, newOther,
            #if PHX_ENABLE_TEMPERATURE
            deltaSelfTemperature, 
            deltaOtherTemperature,
            requiredTemperature, 
            #endif
            prob
        };
        (*chemTable)[target] = reaction;
    }
    #endif

    // burning
    #if PHX_ENABLE_BURNING
    bool isFlammable;
    bool containsO2=false; // can this material be used in burning as oxygen, it should NOT be flamable then
    time_t burningFrames=0;
    MaterialObj burntState=NULL;
    MaterialObj burningGas=NULL;
    #if PHX_ENABLE_TEMPERATURE
    float burningTemperature=PHX_MAX_TEMP;
    #endif
    virtual void setBurningParams(bool containsO2, time_t burningFrames, MaterialObj burntState, MaterialObj burningGas
        #if PHX_ENABLE_TEMPERATURE
        ,float burningTemperature
        #endif
    ) {
        this->containsO2=containsO2;
        this->burningFrames=burningFrames;
        this->burntState=burntState;
        this->burningGas=burningGas;
        #if PHX_ENABLE_TEMPERATURE
        this->burningTemperature=burningTemperature;
        #endif
    }

    // the visuals are not handled by Phyxel automatically. these fields are for your convenience only
    std::vector<Color> fireColors = {};
    virtual Color getFireColor(unsigned i) {
        return fireColors[i];
    }
    virtual void addFireColor(const Color & color) {
        fireColors.push_back(color);
    }

    std::function<void(Scene*, unsigned, unsigned)> customBurning = [](Scene* scene, unsigned X, unsigned Y){return;}; // a functor to handle custom burning routine. the visuals MUST be handled here. a routine to handle visuals could be the following:
    // [](phx::Scene* scene, unsigned X, unsigned Y) {
    //     auto& phyx = (*scene)(X, Y);
    //     auto m = (/*phyx.isRigidFree*/ true ? phyx.material : NULL /*phyx.coveringRigid->material*/);
    //     int rnd = rand();
    //     if (m->fireColors.size() && !(rnd%2)) {
    //         auto flame = scene->addParticle(NULL, m->getFireColor(rnd%(m->fireColors.size())), X, Y, 0, -1.5, false, true, true);
    //         flame->customUpdate = [rnd](phx::Particle* self, void*) {
    //             if ((self->X > PHX_SCENE_SIZE_X || self->X < 0 || self->Y < 0 || self->Y > PHX_SCENE_SIZE_Y) || (self->X+0.5*self->Vx > PHX_SCENE_SIZE_X || self->X+0.5*self->Vx < 0 || self->Y+0.5*self->Vy < 0 || self->Y+0.5*self->Vy > PHX_SCENE_SIZE_Y) || self->lifetime > 5+rnd%10) {
    //                 self->toDelete = true;
    //                 return true;
    //             }
    //             self->color.a -= 0.05*255;
    //             return false;
    //         };
    //     }
    // }
    #endif

    // custom data (not used in any way by Phyxel)
    CustomFields* customFields = NULL;

    // friend classes
    friend class MaterialsList; // defined in "Core/MaterialsList.hpp"
};

}
