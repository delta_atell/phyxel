// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

#pragma once
#include <string>
#include <vector>

#include "Material.hpp"

namespace phx {

/// this class describes the list of phyxel materials as a class with static fields and methods
/// such a list is unique. only use this class to create new materials. materials created outside of this list will not be included in the phyxel engine system
class MaterialsList {
    friend class WorldManager;
    static std::vector<MaterialObj> materials; // only contains objects of type PureMaterial

    MaterialsList() = delete; // MaterialsList shouldn't be created except for the one used

    static void clear() {
        for (MaterialObj mat : materials)
            delete mat;
        materials.clear();
    }
public:
    // indexing
    static MaterialObj get(unsigned id) {
        return materials[id];
    }
    // getter
    static unsigned getSize() {
        return materials.size();
    }

    // creating materials is only allowed this way. you can modify the material however you like afterwards. don't change its id though.
    static MaterialObj addMaterial(const std::string& name, float mass, uint8_t type, 
        const Color& color, 
        #if PHX_ENABLE_TEMPERATURE
        float thermalDiff=0.25, 
        #endif
        #if PHX_ENABLE_BURNING
        bool isFlammable=false, 
        #endif
        bool isPassive=false
    ) { // other fields can be set directly to the material
        MaterialObj tmp = new Material(materials.size(), name, mass, type, 
                                               #if PHX_ENABLE_TEMPERATURE
                                               thermalDiff, 
                                               #endif
                                               color, 
                                               #if PHX_ENABLE_BURNING
                                               isFlammable, 
                                               #endif
                                               isPassive);
        materials.push_back(tmp);
        return tmp;
    }
};

std::vector<MaterialObj> MaterialsList::materials = std::vector<MaterialObj>{};

}
