// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this declares (and partially defines) the scene, see documentation for it below

#pragma once
#include <cstdlib>
#include <vector>
#include <unordered_map>

#include "../phyxel_config.h"
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
#endif
#include "Color.hpp"
#include "Material.hpp"
#if PHX_ENABLE_CHEMISTRY
    #include "Chemistry.hpp"
#endif
#include "PhyxelData.hpp"
#include "../Entities/Particle.hpp"
#include "../Entities/Objects.hpp"
#include "Chunk.hpp"
#include "CustomFields.hpp"

namespace phx {
class WorldManager; // defined in "Core/World.hpp"
class Entity; // defined in "Entities/Entity.hpp"

/// a scene is everything we update each frame
/// it consists of chunks, which in turn are tables of phyxels
/// TODO: better comment
class Scene {
    // these should be private and forbidden to use
    Scene(const Scene&) = delete;
    Scene(Scene&&) = delete;
    Scene& operator=(const Scene&) = delete;
    Scene& operator=(Scene&&) = delete;

    static constexpr uint16_t verticalLiqDiffusion = 1023 - PHX_LIQ_DIFF;
    static constexpr uint16_t gasDiffusion = 1023 - PHX_GAS_DIFF;
    static constexpr float minDeltaMass = 1.1 * gasDiffusion / 1023.0f;
    const uint16_t logChunkSizeX = log2(PHX_CHUNK_SIZE_X); // theoretically static
    const uint16_t logChunkSizeY = log2(PHX_CHUNK_SIZE_Y); // theoretically static

    WorldManager* world;
    Chunk** chunks;
    unsigned numChunksX;
    unsigned numChunksY;
    unsigned chunkSizeX;
    unsigned chunkSizeY;
    unsigned sizeX;
    unsigned sizeY;

    // a hack used to bound the scene with a non-interactable material
    static PhyxelData* bedrock;
    static MaterialObj bedrockMaterial;

    std::unordered_map<Entity*, unsigned> existingEntities; // this includes instances, particles, rigid bodies. this set is only used to check if smth exists, for updates we use specialised vectors. maps entity to its id
    void logEntity(Entity* entity) {
        existingEntities.insert(std::make_pair(entity, entity->getID()));
    }
    void unlogEntity(Entity* entity) {
        existingEntities.erase(entity);
    }

    friend class Chunk;
    friend class Entity;
    friend class PhyxelData;
    friend class WorldManager;
public:
    std::vector<Particle*> particles; // DO NOT PUSH HERE, USE addParticle to create new ones
    std::vector<AbstractInstance*> instances; // contains any instances NOT including rigid objects (if PHX_ENABLE_RIGIDBODIES); DO NOT PUSH HERE, USE addInstance to create new ones
    CustomFields* customFields = NULL;

    float particlesTimeStep = 0.5; // feel free to adjust this parameter, this only affects particle behaviour

    #if PHX_ENABLE_TEMPERATURE
    float envTemperature=PHX_DEFAULT_TEMP;
    float envThermalDiff=0.00001;
    #endif

    #if PHX_ENABLE_RIGIDBODIES
    std::vector<RigidBody*> rigidBodies; // only contains references to rigid bodies; DO NOT PUSH HERE, USE addRigidBody to create new ones
    b2World* box2DLayer; // box2d world
    float box2DTimeStep = 1 / 60.f;
    int32 box2DVelocityIterations = 10;
    int32 box2DPositionIterations = 8;
    b2BodyDef box2DBodyDef;
    b2BodyDef box2DStaticBodyDef;
    b2FixtureDef box2DFixtureDef;
    b2FixtureDef box2DStaticFixtureDef;

    MaterialObj defaultMaterial = NULL; // this should be the only removable gas that rigid bodies will produce when moving in liquids
    
    // technical class to interact with Box2D
    class box2DContactListener : public b2ContactListener {
    public:
        std::function<void(b2Contact* contact, const b2Manifold* oldManifold)> preSolve = [](b2Contact* contact, const b2Manifold* oldManifold){return;}; // a functor to handle custom PreSolve
        std::function<void(b2Contact* contact, const b2ContactImpulse* impulse)> postSolve = [](b2Contact *contact, const b2ContactImpulse* impulse){return;}; // a functor to handle custom PostSolve
        std::function<void(b2Contact* contact)> beginContact = [](b2Contact* contact){return;}; // a functor to handle custom BeginContact
        std::function<void(b2Contact* contact)> endContact = [](b2Contact* contact){return;}; // a functor to handle custom EndContact

        void PreSolve(b2Contact* contact, const b2Manifold* oldManifold) {
            auto* fixData1 = reinterpret_cast<FixtureData*>(contact->GetFixtureA()->GetUserData().pointer);
            auto* fixData2 = reinterpret_cast<FixtureData*>(contact->GetFixtureB()->GetUserData().pointer);

            if ((fixData1->type==0 && reinterpret_cast<b2Fixture*>(fixData1->data)!=contact->GetFixtureB())
            || (fixData2->type==0 && reinterpret_cast<b2Fixture*>(fixData2->data)!=contact->GetFixtureA())) {
                contact->SetEnabled(false);
            }

            preSolve(contact, oldManifold);
        }
        virtual void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse) {
            postSolve(contact, impulse);
        }
        virtual void BeginContact(b2Contact* contact) {
            beginContact(contact);
        }
        virtual void EndContact(b2Contact* contact) {
            endContact(contact);
        }
    };
    #endif

    // constructor (the users are expected to create a scene themselves since Phyxel Engine v1.1; multiple scenes are allowed since v1.2)
    Scene() {
        // create technical bedrock
        if (!bedrock) {
            bedrockMaterial = MaterialsList::addMaterial("phx::technical::_bedrock_hack", 0, PHX_MTYPE_SOL, Color(255,0,255,255)
            #if PHX_ENABLE_TEMPERATURE
            ,0
            #endif
            );
            bedrock = new PhyxelData();
            bedrock->material = bedrockMaterial;
            bedrock->NBR_UP = bedrock;
            bedrock->NBR_LEFT = bedrock;
            bedrock->NBR_DOWN = bedrock;
            bedrock->NBR_RIGHT = bedrock;
            bedrock->myChunk = new Chunk(*this, 0, 0);
            delete[] bedrock->myChunk->phyxels;
            bedrock->myChunk->phyxels = NULL;
        }

        // set world manager to NULL
        world = NULL;

        // create chunks
        numChunksX = PHX_NUM_CHUNKS_X;
        numChunksY = PHX_NUM_CHUNKS_Y;
        chunkSizeX = PHX_CHUNK_SIZE_X;
        chunkSizeY = PHX_CHUNK_SIZE_Y;
        sizeX = numChunksX * chunkSizeX;
        sizeY = numChunksY * chunkSizeY;
        unsigned tmp_size = numChunksY * numChunksX;
        chunks = new Chunk*[tmp_size];
        for(unsigned i = 0; i < tmp_size; ++i)
            chunks[i] = new Chunk((*this), i%numChunksX*chunkSizeX, i/numChunksX*chunkSizeY);
        for(unsigned i = 0; i < numChunksX; ++i)
            for(unsigned j = 0; j < numChunksY; ++j) {
                unsigned idx = numChunksX*j + i;
                Chunk** nbr = chunks[idx]->neighbours;
                if (j != numChunksY-1) nbr[0] = chunks[idx + numChunksX];
                if (j != 0) nbr[2] = chunks[idx - numChunksX];
                if (i != numChunksX-1) nbr[1] = chunks[idx + 1];
                if (i != 0) nbr[3] = chunks[idx - 1];

                chunks[idx]->setPhyxelNeighbours();
            }
        

        #if PHX_ENABLE_RIGIDBODIES
        b2Vec2 gravity(0.0f, PHX_BOX2D_FREEFALL_ACCELERATION);
        box2DLayer = new b2World(gravity);
        box2DLayer->SetContactListener(new box2DContactListener());
        box2DBodyDef.type = b2_dynamicBody;
        box2DBodyDef.position.Set(0.0f, 0.0f);
        box2DBodyDef.linearVelocity.Set(0.0f, 0.0f);
        box2DStaticBodyDef.type = b2_staticBody;
        box2DStaticBodyDef.position.Set(0.0f, 0.0f);
        b2PolygonShape* boxShape = new b2PolygonShape();
        boxShape->SetAsBox(0.5*sizeX*PHX_PHYXELS2METERS, 0.5*PHX_PHYXELS2METERS);
        box2DStaticFixtureDef.shape = boxShape;
        box2DStaticFixtureDef.userData.pointer = reinterpret_cast<uintptr_t>(new FixtureData(bedrock, -1));
        b2Body* upperBound = box2DLayer->CreateBody(&box2DStaticBodyDef);
        upperBound->CreateFixture(&box2DStaticFixtureDef);
        upperBound->SetTransform(
                b2Vec2{static_cast<float>((sizeX * 0.5) * PHX_PHYXELS2METERS), static_cast<float>((-0.5) * PHX_PHYXELS2METERS)}, 0.f);

        b2Body* lowerBound = box2DLayer->CreateBody(&box2DStaticBodyDef);
        lowerBound->CreateFixture(&box2DStaticFixtureDef);
        lowerBound->SetTransform(
                b2Vec2{static_cast<float>((sizeX * 0.5) * PHX_PHYXELS2METERS), static_cast<float>((sizeY + 0.5) * PHX_PHYXELS2METERS)}, 0.f);

        boxShape->SetAsBox(0.5*PHX_PHYXELS2METERS, 0.5*sizeY*PHX_PHYXELS2METERS);
        box2DStaticFixtureDef.shape = boxShape;
        b2Body* leftBound = box2DLayer->CreateBody(&box2DStaticBodyDef);
        leftBound->CreateFixture(&box2DStaticFixtureDef);
        leftBound->SetTransform(
                b2Vec2{static_cast<float>((-0.5) * PHX_PHYXELS2METERS), static_cast<float>((sizeY * 0.5) * PHX_PHYXELS2METERS)}, 0.f);

        b2Body* rightBound = box2DLayer->CreateBody(&box2DStaticBodyDef);
        rightBound->CreateFixture(&box2DStaticFixtureDef);
        rightBound->SetTransform(
                b2Vec2{static_cast<float>((sizeX + 0.5) * PHX_PHYXELS2METERS), static_cast<float>((sizeY * 0.5) * PHX_PHYXELS2METERS)}, 0.f);
        boxShape->SetAsBox(0.5*PHX_PHYXELS2METERS,0.5*PHX_PHYXELS2METERS);
        box2DStaticFixtureDef.shape = boxShape;
        #endif
    }

    // destructor
    ~Scene() {
        #if PHX_ENABLE_RIGIDBODIES
        for(unsigned i = 0; i < rigidBodies.size(); ++i)
            delete rigidBodies[i];

        delete box2DLayer;
        #endif

        for(unsigned i = 0; i < numChunksY*numChunksX; ++i)
            delete chunks[i];
        delete [] chunks;

        for(unsigned i = 0; i < particles.size(); ++i)
            delete particles[i];
        for(unsigned i = 0; i < instances.size(); ++i)
            delete instances[i];

        // bedrock is not cleared during the destruction, as you may want to have multiple scenes
        // delete bedrock->myChunk;
        // delete bedrock;
    }

    // a helper function to convert coordinates inside the scene
    // to the index of the Chunk and the coordinates inside it.
    std::tuple<unsigned, unsigned, unsigned> getChunkCoords (unsigned X, unsigned Y) const {
        return std::tuple<unsigned, unsigned, unsigned>(
                (Y >> logChunkSizeY) * PHX_NUM_CHUNKS_X + (X >> logChunkSizeX),
                (PHX_CHUNK_SIZE_X - 1) & X,
                (PHX_CHUNK_SIZE_Y - 1) & Y);
    }
    std::tuple<unsigned, float, float> getChunkCoords (float X, float Y) const {
        return std::tuple<unsigned, float, float>(
                ((int)Y >> logChunkSizeY) * PHX_NUM_CHUNKS_X + ((int)X >> logChunkSizeX),
                X - (int)X,
                Y - (int)Y);
    }


    // getters for the most common properties. use scene(X,Y).property or scene[i].property if there's no getter provided
    const MaterialObj& getMaterial(unsigned X, unsigned Y) const {
        return (*this)(X, Y).material;
    }
    const Color& getColor(unsigned X, unsigned Y) const {
        return (*this)(X, Y).color;
    }
    #if PHX_ENABLE_TEMPERATURE
    float getTemperature(unsigned X, unsigned Y) const {
        return (*this)(X, Y).temperature;
    }
    #endif
    #if PHX_ENABLE_ELECTRICITY
    float getCharge(unsigned X, unsigned Y) const {
        return (*this)(X, Y).charge;
    }
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    bool getIsRigidFree(unsigned X, unsigned Y) const {
        return (*this)(X, Y).isRigidFree;
    }
    Color getRigidColor(unsigned X, unsigned Y) const {
        return (*this)(X, Y).coveringRigid->color;
    }
    RigidBody* getCoveringRigid(unsigned X, unsigned Y) const {
        return (*this)(X, Y).coveringRigid;
    }
    #endif
    const PhyxelData & operator()(unsigned X, unsigned Y) const { // double indexing
        unsigned i;
        std::tie(i, X, Y) = getChunkCoords(X, Y);
        return chunks[i]->phyxels[PHX_CHUNK_SIZE_X*Y+X];
    }
    unsigned getSizeX() const {
        return sizeX;
    }
    unsigned getSizeY() const {
        return sizeY;
    }
    #if PHX_ENABLE_CUSTOMGRAVITY
    uint8_t getForce(unsigned X, unsigned Y) const {
        return (*this)(X, Y).forceDir;
    }
    #endif

    // setters for the most common properties. use scene(X,Y).property or scene[i].property if there's no setter provided
    void setMaterial(unsigned X, unsigned Y, MaterialObj m, int rnd=0) {
        (*this)(X, Y).setMaterial(m, rnd);
    }
    void setColor(unsigned X, unsigned Y, Color val) {
        (*this)(X, Y).setColor(val);
    }
    PhyxelData & operator()(unsigned X, unsigned Y) { // double indexing
        if (X >= sizeX || Y >= sizeY)
            return *bedrock;
        unsigned i, X1, Y1;
        std::tie(i, X1, Y1) = getChunkCoords(X, Y);
        return chunks[i]->phyxels[PHX_CHUNK_SIZE_X*Y1+X1];
    }
    #if PHX_ENABLE_TEMPERATURE
    void setTemperature(unsigned X, unsigned Y, float tau){
        (*this)(X, Y).setTemperature(tau);
    }
    void addTemperature(unsigned X, unsigned Y, float delta_tau) {
        (*this)(X, Y).addTemperature(delta_tau);
    }
    #endif
    #if PHX_ENABLE_ELECTRICITY
    void setCharge(unsigned X, unsigned Y, float dQ) {
        (*this)(X, Y).setCharge(dQ);
    }
    void addCharge(unsigned X, unsigned Y, float dQ) {
        (*this)(X, Y).addCharge(dQ);
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void setForce(unsigned X, unsigned Y, uint8_t forceDir) {
        (*this)(X, Y).setForce(forceDir);
    }
    #endif

    // adding entities to the scene
    Particle* addParticle(
            MaterialObj material, Color color,
            float X, float Y,
            float Vx, float Vy,
            bool isAffectedByGravity = true,
            bool checkCollisions = true
            #if PHX_ENABLE_BURNING
            ,bool isBurning = false
            #endif
    ) {
        Particle* p = new Particle(material, color, X, Y, Vx, Vy, this, isAffectedByGravity, checkCollisions
                #if PHX_ENABLE_BURNING
                ,isBurning
                #endif
        );
        particles.push_back(p);
        return p;
    }
    void addVirtualParticle(
            MaterialObj material, Color color,
            float X, float Y,
            float Vx, float Vy
    ) {
        if (!material)
            throw std::string("trying to create a virtual particle but material was NULL. for non-materialistic particles just create a regular particle with NULL as its material");
        if (material->isRemovable) return;
        auto p = addParticle(material, color, X, Y, Vx, Vy);
        p->isVirtual = true;
    }
    AbstractInstance* addInstance(AbstractInstance* i) { // should not be used for rigid bodies (in case you have PHX_ENABLE_RIGIDBODIES), use addRigidBody for them. only use this to add some other instances
        instances.push_back(i);
        return i;
    }
    #if PHX_ENABLE_RIGIDBODIES // (coordinates are expected in phyxels and not Box2D's metre units)
    RigidBody* addRigidBody(float x0, float y0, float _sizeX, float _sizeY, MaterialObj material) {
        return addRigidBody<void>(x0, y0, _sizeX, _sizeY, material, nullptr);
    }
    template<typename T> RigidBody* addRigidBody(float x0, float y0, float _sizeX, float _sizeY, MaterialObj material, T* userData, int userDataType=1) { // this can be used to add userdata to the inner box2d body of phyxel rigid bodies
        box2DBodyDef.position.Set(x0*PHX_PHYXELS2METERS, y0*PHX_PHYXELS2METERS);
        b2Body* body = box2DLayer->CreateBody(&box2DBodyDef);
        RigidBody* rigidBody = new RigidBody(this, box2DLayer, body, _sizeX, _sizeY, material);
        #if PHX_ENABLE_TEMPERATURE
        rigidBody->temperature = envTemperature;
        #endif
        b2PolygonShape dynamicBox;
        dynamicBox.SetAsBox(_sizeX*0.5*PHX_PHYXELS2METERS, _sizeY*0.5*PHX_PHYXELS2METERS);
        box2DFixtureDef.shape = &dynamicBox;
        box2DFixtureDef.density = 1.0f;
        box2DFixtureDef.friction = 0.3f;
        if (!userDataType)  // userDataType=0 is reserved for tmp bodies.
            userDataType = 1;
        box2DFixtureDef.userData.pointer = reinterpret_cast<uintptr_t>(new FixtureData(userData, userDataType));
        body->CreateFixture(&box2DFixtureDef);
        rigidBody->gravForce = PHX_BOX2D_FREEFALL_ACCELERATION * body->GetMass();
        rigidBodies.push_back(rigidBody);
        return rigidBody;
    }
    #endif

    // swaps
    void swap(unsigned x1, unsigned y1, unsigned x2, unsigned y2, bool loopDir) { // swap phyxels (only the necessary parts) /// THIS FUNCTION ISN'T SYMMETRIC
        auto& phyx1 = (*this)(x1, y1);
        auto& phyx2 = (*this)(x2, y2);

        phyx1.lockMask = 1;
        if (phyx2.lockMask & 1) return;
        swapNoLock(phyx1, phyx2);

        if(loopDir == (y1 < y2 || (y1 == y2 && x1 < x2)))
            phyx2.lockMask = 3;
        else
            phyx2.lockMask = 1;
    }

    void swapNoLock(PhyxelData& phyxel1, PhyxelData& phyxel2) { // swap phyxels without locking them
        phyxel1.flagAreaActive();
        phyxel2.flagAreaActive();
        std::swap(phyxel1.material, phyxel2.material);
        std::swap(phyxel1.color, phyxel2.color);

        #if PHX_ENABLE_RIGIDBODIES
        for (auto* body : phyxel1.box2DTmpBodies) {
            auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
            delete fixData;
            box2DLayer->DestroyBody(body);
        }
        phyxel1.box2DTmpBodies.clear();
        phyxel1.deleteTmpBody.clear();

        for (auto* body : phyxel2.box2DTmpBodies) {
            auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
            box2DLayer->DestroyBody(body);
        }
        phyxel2.box2DTmpBodies.clear();
        phyxel2.deleteTmpBody.clear();
        #endif
        #if PHX_ENABLE_TEMPERATURE
        std::swap(phyxel1.temperature, phyxel2.temperature);
        #endif
        // no swap for electricity
        #if PHX_ENABLE_BURNING
        std::swap(phyxel1.burningStage, phyxel2.burningStage);
        std::swap(phyxel1.burningTimer, phyxel2.burningTimer);
        #endif
        std::swap(phyxel1.customFields, phyxel2.customFields);
    }
    void swapNoLock(unsigned X1, unsigned Y1, unsigned X2, unsigned Y2) {
        unsigned i1, i2;
        std::tie(i1, X1, Y1) = getChunkCoords(X1, Y1);
        std::tie(i2, X2, Y2) = getChunkCoords(X2, Y2);
        swapNoLock(chunks[i1]->phyxels[PHX_CHUNK_SIZE_X*Y1+X1],
                   chunks[i2]->phyxels[PHX_CHUNK_SIZE_X*Y2+X2]);
    }

    // fill in the scene (call it after init)
    void fill(MaterialObj m) {
        for(unsigned i=0; i<PHX_NUM_CHUNKS_X*PHX_NUM_CHUNKS_Y; ++i)
            chunks[i]->fill(m);
    }
    void fillFrame(MaterialObj m) {
        makeRectangle(m, 0, 0, PHX_SCENE_SIZE_X-1, PHX_SCENE_SIZE_Y-1, false);
    }
    #if PHX_ENABLE_TEMPERATURE
    void fillTemperature(float tau) { 
        for (unsigned i = 0; i < PHX_NUM_CHUNKS_X*PHX_NUM_CHUNKS_Y; ++i)
            chunks[i]->fillTemperature(tau);
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void fillForceField(uint8_t forceDir) { // fills in the force field over the entire scene
        for (unsigned i = 0; i < PHX_NUM_CHUNKS_X*PHX_NUM_CHUNKS_Y; ++i)
            chunks[i]->fillForceField(forceDir);
    }
    #endif
    void makeRectangle(MaterialObj m, unsigned X1, unsigned Y1, unsigned X2, unsigned Y2, bool filled = true, int rnd = 0) { // creates a rectangle of the given materain in the scene. can be filled or hollow. (X1, Y1) is the left top angle, (X2, Y2) is the right bottom one (both included)
        if (filled)
            for (unsigned i = X1; i <= X2; ++i)
                for (unsigned j = Y1; j < Y2; ++j) {
                    setMaterial(i, j, m, (rnd ? rnd : rand()));
                }
        else {
            for (unsigned i = X1; i <= X2; ++i) {
                setMaterial(i, Y1, m, rnd+i);
                setMaterial(i, Y2, m, rnd+i+1);
            }
            for (unsigned i = Y1+1; i < Y2; ++i) {
                setMaterial(X1, i, m, rnd+i+2);
                setMaterial(X2, i, m, rnd+i+3);
            }
        }
    }

    // check if something (particle, rigid, instance, etc.) exists. you should NOT call it like that:
    /// WRONG: scene.exists(myEntity, myEntity->getID());
    // because this essentially only checks whether this is a valid entity pointer or not.
    // instead, you should, save your entity ID and then provide it to the function. this will also check whether the pointer still points to *the same* entity as the one you saved.
    bool exists(Entity* entity, unsigned savedID) {
        auto found = existingEntities.find(entity);
        return (found != existingEntities.end() && found->second == savedID);
    }

    // updates (defined in "Core/Utils.hpp")
    void updateAll( // updates everything, can be called with 0 arguments like scene.updateAll(). combined (temperature + chemistry + burning + electricity + movement) & entity update (rigids, particles, other instances) & balanced (bidirectional loop) update for every phyxel in the scene. in most cases, it's the only thing you need
        void* custom_arguments_particles,
        void* custom_arguments_instances
        #if PHX_ENABLE_RIGIDBODIES
        ,void* custom_arguments_rigids
        #endif
    );
    #if PHX_ENABLE_PARALLEL_UPDATE
    void updateAllParallel( // updates everything, can be called with 1 argument like scene.updateAllParallel(N). combined (temperature + chemistry + burning + electricity + movement) & balanced bidirectional loop update for every phyxel in the scene that is run in parallel for chunks & update of other entities (particles, rigids, other instances). in most cases, it's the only thing you need
        unsigned n_threads,
        void* custom_arguments_particles,
        void* custom_arguments_instances
        #if PHX_ENABLE_RIGIDBODIES
        ,void* custom_arguments_rigids
        #endif
        ,bool use_threads // (debug)
    );
    #endif
    void updateParticles(void* custom_arguments); // only updates particles
    void updateInstances(void* custom_arguments); // only updates other instances (not including rigid bodies)
    #if PHX_ENABLE_RIGIDBODIES
    void rigidBodyFrameLoop(RigidBody* body);
    void updateRigidBodies(void* custom_arguments); // only updates rigid bodies
    #endif
};

PhyxelData* Scene::bedrock = NULL;
MaterialObj Scene::bedrockMaterial = NULL;

}
