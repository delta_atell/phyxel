// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// TODO: add comments to methods and the class

#pragma once
#if PHX_ENABLE_WORLD_SAVE
#include <fstream>
#include <nlohmann/json.hpp>
#endif

namespace phx {
#if PHX_ENABLE_WORLD_SAVE
using json = nlohmann::json;

class WorldManager; // defined in "Core/World.hpp"
#endif

/// CustomFields is (kind of like) an abstract class that you should inherit when defining your own types to put additional data (as custom fields) into built-in types of the Phyxel. Classes like `PhyxelData`, `Material` and all the `Entity`-based types (also `Scene` and `Chunk`) all have a special `CustomFields* customFields` field initialised with NULL by default. You can define your own types, inheriting from `CustomFields` to put any data into these classes. Don't forget to properly define the destructor in your type.
class CustomFields {
public:
    virtual ~CustomFields() {}; // don't forget to define this in your CustomFields type
    
    // ignore the following methods if you're not using WorldManager to save/load data.
    #if PHX_ENABLE_WORLD_SAVE
    // these need to be defined if your CustomFields type is put into .dat files during the serialisation via WorldManager, that is, if your type is a CustomFields type for PhyxelData:
    virtual void appendToBitfile(std::ofstream& file) {}
    virtual void readSectionFromBitfile(std::ifstream& file) {}
    // these need to be defined if your CustomFields type is put into .json files during the serialisation via WorldManager, that is, if your type is a CustomFields type for entities, materials, particles, chunks:
    virtual json getAsJSON() { return {}; }
    virtual void setFromJSON(const json& custom_fields) {}
    CustomFields() {}; // this constructor also needs to be defined in your type to be able to load and save the data with JSON
    #endif
};

}