// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

#pragma once
#include <sstream>
#include <fstream>
#include <filesystem>
#include <nlohmann/json.hpp>

namespace phx {
using json = nlohmann::json;

class Scene; // defined in "Core/Scene.hpp"

// The WorldManager class is used to serialise data and for saving/loading in general. specifically, it allows managing the scene inside a possibly much bigger world attached to it that is saved on disk and is not being updated constantly.
//    ```
//    phx::Scene scene;
//    phx::WorldManager world(folderPathString, &scene, 0, 0, 50, 50);
//    ```
// 
// the code above specifies the 50x50 chunk world saved in `folderPathString` and attaches the scene to its top-left corner. if the world already exists under the specified path, you can omit the size of the world in its creation:
// 
//    ```
//    phx::Scene scene;
//    phx::WorldManager world(folderPathString, &scene, 0, 0);
//    ```
// 
// pay attention to the initialisation that you need to do by calling `WorldManager::fillIfEmpty` when creating a world.
// the world is saved like this:
// ```
// {folderPath}:
//      materials.json -> (text file) the list of materials associated with the world
//      meta.dat -> (binary file) world size
//      phyxel_config.json -> (text file) saves `phyxel_config.h` macros to check
//      scene.json -> (text file) some data from the scene that can be changed by the user
//      chunk_{X}_{Y}:
//          chunk_customdata.json -> (text file) custom fields of the chunk
//          entity_{T}.json -> (text file) entity that was in this chunk
//          phyxels.dat -> (binary file) phyxel-wise chunk
//          phyxels_customdata.dat -> (binary file) phyxel-wise custom data
/// ```
// WARNING: if you're using WorldManager to serialise and save your simulation data, please note that we don't save or load any lambda functions / functors / functional data (like `customUpdate` values) in general. take care of them yourself.
// WARNING: another limitation is that the WorldManager doesn't save general non-particle entities or any instances other than rigid bodies (so, it's going to save the rigid bodies and particles, but no other entities). you should take care of serialising your custom instances yourself, if you need it (which, in our opinion, can be avoided). we also don't save custom fields from Scenes for now. just set this data properly whenever creating a Scene.
// WARNING: RigidBodies are saved without the RigidJoint connections for now.
// TODO: this WorldManager class is extremely raw. it's okay for some basic operations but certainly needs some more work in later versions. maybe redesign this saving/loading system to improve its design, as this feels a bit suboptimal. better filling functions and user control over the world. the phyxel serialisation system should possibly be redesigned completely to save everything in different files, kinda like layers: materials, temperatures, colours, etc. this way it's also easier to manage different sizes of different data types on different machines. remove other technical limitations by further redesign
class WorldManager {
    std::filesystem::path folderPath;
    Scene* scene;
    unsigned sizeInChunksX;
    unsigned sizeInChunksY;
    unsigned sceneUpperLeftChunkX;
    unsigned sceneUpperLeftChunkY;

    void loadSize() {
        auto filename = folderPath.string() + "/meta.dat";
        std::ifstream bf(filename, std::ios::in | std::ios::binary);

        if (!bf)
            throw std::string("loading world size failed: unable to open " + filename);

        bf.read((char*)&sizeInChunksX, sizeof(unsigned));
        bf.read((char*)&sizeInChunksY, sizeof(unsigned));

        bf.close();
    }

    void setConfigJSON(json& config) {
        // sizes
        config["PHX_NUM_CHUNKS_X"] = PHX_NUM_CHUNKS_X;
        config["PHX_NUM_CHUNKS_Y"] = PHX_NUM_CHUNKS_Y;
        config["PHX_CHUNK_SIZE_X"] = PHX_CHUNK_SIZE_X;
        config["PHX_CHUNK_SIZE_Y"] = PHX_CHUNK_SIZE_Y;

        // physics
        config["PHX_ENABLE_CHEMISTRY"] = PHX_ENABLE_CHEMISTRY;
        config["PHX_ENABLE_CUSTOMGRAVITY"] = PHX_ENABLE_CUSTOMGRAVITY;
        config["PHX_ENABLE_TEMPERATURE"] = PHX_ENABLE_TEMPERATURE;
        config["PHX_ENABLE_ELECTRICITY"] = PHX_ENABLE_ELECTRICITY;
        config["PHX_ENABLE_BURNING"] = PHX_ENABLE_BURNING;
        config["PHX_ENABLE_RIGIDBODIES"] = PHX_ENABLE_RIGIDBODIES;

        // technical
        config["PHX_ENABLE_PARALLEL_UPDATE"] = PHX_ENABLE_PARALLEL_UPDATE;
        
        /// additional parameters
        config["PHX_GAS_DIFF"] = PHX_GAS_DIFF;
        config["PHX_LIQ_DIFF"] = PHX_LIQ_DIFF;
        #if PHX_ENABLE_TEMPERATURE
        config["PHX_MAX_TEMP"] = PHX_MAX_TEMP;
        config["PHX_DEFAULT_TEMP"] = PHX_DEFAULT_TEMP;
        config["PHX_ENABLE_ENV_TEMP"] = PHX_ENABLE_ENV_TEMP;
        config["PHX_MIN_TEMP"] = PHX_MIN_TEMP;
        config["PHX_MIN_DELTA_TEMP"] = PHX_MIN_DELTA_TEMP;
        #if PHX_ENABLE_BURNING
        config["PHX_BURNING_HEAT"] = PHX_BURNING_HEAT;
        #endif

        #if PHX_ENABLE_ELECTRICITY
        config["PHX_ELECTRIC_HEAT"] = PHX_ELECTRIC_HEAT;
        #endif
        #endif

        #if PHX_ENABLE_RIGIDBODIES
        config["PHX_BOX2D_FREEFALL_ACCELERATION"] = PHX_BOX2D_FREEFALL_ACCELERATION;
        config["PHX_METERS2PHYXELS"] = PHX_METERS2PHYXELS;
        config["PHX_PHYXELS2METERS"] = PHX_PHYXELS2METERS;
        #endif
        config["PHX_PHYXELS2METERS"] = PHX_PHYXELS2METERS;
        #if PHX_ENABLE_ELECTRICITY
        config["PHX_ELECTRIC_SPREAD"] = PHX_ELECTRIC_SPREAD;
        #endif
    }

    void saveConfig() {
        json config;
        setConfigJSON(config);
        std::ofstream file(folderPath.string() + "/phyxel_config.json");
        file << config.dump();
        file.close();
    }

    bool checkConfig() {
        std::ifstream file(folderPath.string() + "/phyxel_config.json");
        if (!file) {
            file.close();
            saveConfig();
            return true;
        }
        json currentConfig;
        setConfigJSON(currentConfig);
        json recordedConfig;
        file >> recordedConfig;
        file.close();

        // changing scene size should be fine unless it's bigger than the world
        recordedConfig["PHX_NUM_CHUNKS_X"] = PHX_NUM_CHUNKS_X;
        recordedConfig["PHX_NUM_CHUNKS_Y"] = PHX_NUM_CHUNKS_Y;

        return currentConfig == recordedConfig;
    }

    void setMaterialJSON(json& materials) {
        materials = json::array();

        for (unsigned i = 0, e = MaterialsList::getSize(); i < e; ++i) {
            MaterialObj mat = MaterialsList::get(i);
            json matJSON;
            matJSON["name"] = mat->name;
            matJSON["mass"] = mat->mass;
            matJSON["invMass"] = mat->invMass;
            matJSON["type"] = mat->type;
            matJSON["viscosity"] = mat->viscosity;
            matJSON["isPassive"] = mat->isPassive;
            matJSON["isRemovable"] = mat->isRemovable;
            // electricity
            #if PHX_ENABLE_ELECTRICITY
            matJSON["resistance"] = mat->resistance;
            #endif
            // state transition & temperature
            #if PHX_ENABLE_TEMPERATURE
            matJSON["thermalDiff"] = mat->thermalDiff;
            matJSON["stateMinTemp"] = mat->stateMinTemp;
            matJSON["stateMaxTemp"] = mat->stateMaxTemp;
            matJSON["lowerTempState"] = mat->lowerTempState->getID();
            matJSON["higherTempState"] = mat->higherTempState->getID();
            #endif
            // visuals
            json colorJSON = json::array();
            for (Color& c : mat->colors)
                colorJSON.push_back(json::array({c.r, c.g, c.b, c.a}));
            matJSON["colors"] = colorJSON;

            // chemistry
            #if PHX_ENABLE_CHEMISTRY
            json chemTableJSON = json::array();
            if (auto* chemTable = mat->chemTable)
                for (auto& [material, reaction] : *chemTable) {
                    json reactionJSON;
                    reactionJSON["material"] = material->getID();
                    reactionJSON["newSelf"] = reaction.newSelf->getID();
                    reactionJSON["newOther"] = reaction.newOther->getID();
                    #if PHX_ENABLE_TEMPERATURE
                    reactionJSON["deltaSelfTemperature"] = reaction.deltaSelfTemperature;
                    reactionJSON["deltaOtherTemperature"] = reaction.deltaOtherTemperature;
                    reactionJSON["requiredTemperature"] = reaction.requiredTemperature;
                    #endif
                    reactionJSON["prob"] = reaction.prob;
                    chemTableJSON.push_back(reactionJSON);
                }
            matJSON["chemTable"] = chemTableJSON;
            #endif

            // burning
            #if PHX_ENABLE_BURNING
            json fireColorJSON = json::array();
            for (Color& c : mat->fireColors)
                fireColorJSON.push_back(json::array({c.r, c.g, c.b, c.a}));
            matJSON["fireColors"] = fireColorJSON;
            matJSON["isFlammable"] = mat->isFlammable;
            matJSON["containsO2"] = mat->containsO2;
            matJSON["burningFrames"] = mat->burningFrames;
            if (auto* burntState = mat->burntState)
                matJSON["burntState"] = burntState->getID();
            if (auto* burningGas = mat->burningGas)
                matJSON["burningGas"] = burningGas->getID();
            #if PHX_ENABLE_TEMPERATURE
            matJSON["burningTemperature"] = mat->burningTemperature;
            #endif
            #endif

            // custom fields
            if (mat->customFields) {
                matJSON["customFields"] = mat->customFields->getAsJSON();
            }
            materials.push_back(matJSON);
        }
    }

    void saveMaterialsList() {
        json materials;
        setMaterialJSON(materials);
        std::ofstream file(folderPath.string() + "/materials.json");
        file << materials.dump();
        file.close();
    }

    void loadMaterialsList() {
        std::ifstream file(folderPath.string() + "/materials.json");
        if (!file) {
            file.close();
            saveMaterialsList();
            return;
        }
        MaterialsList::clear();
        json recordedMatList;
        file >> recordedMatList;

        size_t i = 0;
        for (auto& matJSON : recordedMatList) {
            std::vector<Color> colors;
            for (auto& colorJSON : matJSON["colors"])
                colors.push_back({colorJSON[0], colorJSON[1], colorJSON[2], colorJSON[3]});
            MaterialsList::addMaterial(
                matJSON["name"],
                matJSON["mass"],
                matJSON["type"], 
                colors[0],
                #if PHX_ENABLE_TEMPERATURE
                matJSON["thermalDiff"],
                #endif
                #if PHX_ENABLE_BURNING
                matJSON["isFlammable"],
                #endif
                matJSON["isPassive"]);
            MaterialObj mat = MaterialsList::get(i);

            mat->viscosity = matJSON["viscosity"];
            mat->isRemovable = matJSON["isRemovable"];

            // visuals (the first color has already been added)
            for (auto c = colors.begin() + 1, e = colors.end(); c != e; ++c)
                mat->addColor(*c);

            // electricity
            #if PHX_ENABLE_ELECTRICITY
            mat->resistance = matJSON["resistance"];
            #endif

            #if PHX_ENABLE_TEMPERATURE
            mat->stateMinTemp = matJSON["stateMinTemp"];
            mat->stateMaxTemp = matJSON["stateMaxTemp"];
            #endif

            #if PHX_ENABLE_BURNING
            for (auto& colorJSON : matJSON["fireColors"])
                mat->addFireColor({colorJSON[0], colorJSON[1], colorJSON[2], colorJSON[3]});
            #endif
            ++i;
        }

        i = 0;
        for (auto& matJSON : recordedMatList) {
            MaterialObj mat = MaterialsList::get(i);

            #if PHX_ENABLE_TEMPERATURE
            MaterialObj lowerTempState = !matJSON["lowerTempState"].empty() ? MaterialsList::get(matJSON["lowerTempState"]) : nullptr;
            MaterialObj higherTempState = !matJSON["higherTempState"].empty() ? MaterialsList::get(matJSON["higherTempState"]) : nullptr;
            mat->setStateTransitions(matJSON["stateMinTemp"], matJSON["stateMaxTemp"], lowerTempState, higherTempState);
            #endif

            #if PHX_ENABLE_CHEMISTRY
            auto& chemTableJson = matJSON["chemTable"];
            if (!chemTableJson.empty())
                for (auto& reactionJSON : chemTableJson) {
                    mat->addReaction(
                    MaterialsList::get(reactionJSON["material"]),
                    MaterialsList::get(reactionJSON["newSelf"]),
                    MaterialsList::get(reactionJSON["newOther"]),
                    #if PHX_ENABLE_TEMPERATURE
                    reactionJSON["deltaSelfTemperature"],
                    reactionJSON["deltaOtherTemperature"],
                    reactionJSON["requiredTemperature"],
                    #endif
                    reactionJSON["prob"]);
                }
            #endif

            #if PHX_ENABLE_BURNING
            MaterialObj burntState = !matJSON["burntState"].empty() ? MaterialsList::get(matJSON["burntState"]) : nullptr;
            MaterialObj burningGas = !matJSON["burningGas"].empty() ? MaterialsList::get(matJSON["burningGas"]) : nullptr;
            mat->setBurningParams(matJSON["containsO2"], matJSON["burningFrames"], burntState, burningGas
            #if PHX_ENABLE_TEMPERATURE
            , matJSON["burningTemperature"]
            #endif
            );
            #endif

            if (!matJSON["customFields"].empty())
                mat->customFields->setFromJSON(matJSON["customFields"]);
            ++i;
        }

        file.close();
    }

    void fillWorldChunkIfEmpty(unsigned X, unsigned Y, MaterialObj m, float* tauPtr); // defined in "Core/Utils.hpp"
    
    static unsigned savedEntityNumber;

    void saveEntity(json& obj) { // determines in which chunk the object lies and saves it there
        if (obj["X"].is_null()) return; // object is incomplete. sometimes this happens for some reason. TODO: proper debug and investigation

        unsigned chunkX = std::floor((float)(obj["X"]) / PHX_CHUNK_SIZE_X);
        unsigned chunkY = std::floor((float)(obj["Y"]) / PHX_CHUNK_SIZE_Y);

        // we calculate global (world) coordinates to save to disk as opposed to the scene coordinates in the json
        obj["worldX"] = (float)(obj["X"]) + sceneUpperLeftChunkX*PHX_CHUNK_SIZE_X;
        obj["worldY"] = (float)(obj["Y"]) + sceneUpperLeftChunkY*PHX_CHUNK_SIZE_Y;

        std::stringstream ss_ett;
        ss_ett << "/entity_" << time(NULL) << '_' << obj["id"] << '_' << savedEntityNumber << ".json";
        ++savedEntityNumber;

        std::stringstream ss_chunk;
        ss_chunk << "/chunk_" << sceneUpperLeftChunkX+chunkX << "_" << sceneUpperLeftChunkY+chunkY;

        std::ofstream file(folderPath.string() + ss_chunk.str() + ss_ett.str());
        file << obj.dump();
        file.close();
    }
    // creates a particle or a rigid body in the scene, deletes the file
    template <typename ParticlesCustomFieldsSpec = void, typename RigidBodiesCustomFieldsSpec = void>
    void loadEntity(const std::string& path); // defined in "Core/Utils.hpp"
    void clearSceneEntities(); // defined in "Core/Utils.hpp"

    void saveScene(); // defined in "Core/Utils.hpp"
    template <typename ParticlesCustomFieldsSpec = void, typename RigidBodiesCustomFieldsSpec = void, typename ChunkCustomFieldsSpec = void>
    void loadScene(); // defined in "Core/Utils.hpp"

    void saveChunk(unsigned posInChunksX, unsigned posInChunksY); // defined in "Core/Utils.hpp"
    template<typename ChunkCustomFieldsSpec = void>
    void loadChunk(unsigned posInChunksX, unsigned posInChunksY); // defined in "Core/Utils.hpp"
public:
    WorldManager(const std::filesystem::path& path, Scene* scene1, 
                 unsigned sceneUpperLeftChunkX, unsigned sceneUpperLeftChunkY);

    WorldManager(const std::filesystem::path& path, Scene* scene1, 
                 unsigned sceneUpperLeftChunkX, unsigned sceneUpperLeftChunkY,
                 unsigned sizeInChunksX, unsigned sizeInChunksY);

    void resize(unsigned newSizeInChunksX, unsigned newSizeInChunksY) {
        sizeInChunksX = newSizeInChunksX;
        sizeInChunksY = newSizeInChunksY;

        std::string filename = folderPath.string() + "/meta.dat";
        std::ofstream bf(filename, std::ios::out | std::ios::binary);

        bf.write((char*)&sizeInChunksX, sizeof(unsigned));
        bf.write((char*)&sizeInChunksY, sizeof(unsigned));
        bf.close();
    }

    #if PHX_ENABLE_TEMPERATURE==0
    // fills the empty chunks in the world with the given material. basically initialisation
    void fillIfEmpty(MaterialObj m) {
        for (unsigned i = 0; i < sizeInChunksX; ++i)
            for (unsigned j = 0; j < sizeInChunksX; ++j)
                fillWorldChunkIfEmpty(i, j, m, NULL);
    }
    #endif
    #if PHX_ENABLE_TEMPERATURE
    // fills the empty chunks in the world with the given temperature and material. basically initialisation
    void fillIfEmpty(MaterialObj m, float tau) {
        for (unsigned i = 0; i < sizeInChunksX; ++i)
            for (unsigned j = 0; j < sizeInChunksX; ++j)
                fillWorldChunkIfEmpty(i, j, m, &tau);
    }
    #endif

    void save() {
        saveMaterialsList();
        saveScene();
    }

    template <typename ParticlesCustomFieldsSpec = void, typename RigidBodiesCustomFieldsSpec = void, typename ChunkCustomFieldsSpec = void>
    void load() { // if you want to load custom data (`customFields`) of particles/rigid bodies here, please specify the types (classes you defined for the custom data) in the template arguments. you may otherwise ignore the template arguments
        loadMaterialsList();
        loadScene<ParticlesCustomFieldsSpec, RigidBodiesCustomFieldsSpec, ChunkCustomFieldsSpec>();
    }

    template <typename ParticlesCustomFieldsSpec = void, typename RigidBodiesCustomFieldsSpec = void, typename ChunkCustomFieldsSpec = void>
    void repositionScene(unsigned newSceneUpperLeftChunkX, unsigned newSceneUpperLeftChunkY) { // saves the scene, moves it to another point in the world, loads the contents of the scene from there properly
        // TODO: causes problems with rigid bodies and an infinite update loop
        saveScene();
        sceneUpperLeftChunkX = newSceneUpperLeftChunkX;
        sceneUpperLeftChunkY = newSceneUpperLeftChunkY;
        loadScene<ParticlesCustomFieldsSpec, RigidBodiesCustomFieldsSpec, ChunkCustomFieldsSpec>();
    }

    // TODO: implement the optimised version of `repositionScene` here. it should move the chunks around, not saving & loading unneeded information
    // void shiftScene() { // save + move + load, but it's all optimised
    //     // specific save and load functions for scene-affected areas only should be saved here. they should be public
    // }

    unsigned getSceneUpperLeftChunkX() {
        return sceneUpperLeftChunkX;
    }

    unsigned getSceneUpperLeftChunkY() {
        return sceneUpperLeftChunkY;
    }
};

unsigned WorldManager::savedEntityNumber = 0;

}