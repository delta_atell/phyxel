// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definition of our PhyxelData class, which is what the scene consists of

#pragma once
#include "../phyxel_config.h"
#include "MaterialsList.hpp"
#include "CustomFields.hpp"

#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
    #include "../Entities/Objects.hpp"
#endif

namespace phx {

class Scene; // defined in "Core/Scene.hpp"
class Chunk; // defined in "Core/Chunk.hpp"
#if PHX_ENABLE_RIGIDBODIES
class RigidBody; // defined in "Entities/Objects.hpp"
#endif

class PhyxelData {
  // technical update flags (bit fields)
  unsigned int lockMask : 2; // used to lock phyxels during updates, changing not recommended
                    // locks are one-time flags (only valid for one update step) that suggest a certain behaviour when swapping
                    // 1 means locked (swap interaction not allowed)
                    // 2 means it should be skipped
                    // 3 is both 1 and 2
  bool wasUpdated : 1; // basically a flag that is alternating each time the movement update sees this phyxel
  // electricity-related bit fields
  #if PHX_ENABLE_ELECTRICITY
  bool isTurning : 1; // whether the charge should turn direction now or not
  #endif
  // rigid-body-related bit fields
  #if PHX_ENABLE_RIGIDBODIES
    #if PHX_ENABLE_ELECTRICITY
    bool isChargeFromBody : 1; // whether the charge is related to the rigid body or to the phyxel itself
    #endif
  bool isRigidFree : 1; // whether the phyxel is occupied by a rigid body or not
  #endif

  // rigid bodies
  #if PHX_ENABLE_RIGIDBODIES
  std::vector<b2Body*> box2DTmpBodies; // "rigid bodies" attached to this phyxel so that real rigids don't fall through it
  RigidBody* coveringRigid; // the rigid body that covers this phyxel (or NULL if isRigidFree is true)
  std::vector<bool> deleteTmpBody; // is true when the attached tmp body is not in use anymore and should be deleted
  #endif

  PhyxelData* neighbours[4] = {NULL}; // pointers to the neighbouring phyxels
                                      // 0 - down, 1 - right, 2 - up, 3 - left
  Chunk* myChunk = NULL;

  // friends
  friend class Scene;
  friend class Chunk;
  #if PHX_ENABLE_RIGIDBODIES
  friend class RigidBody;
  #endif
public:
  // main fields
  PRIVATE_DEF(MaterialObj material); // the material of the phyxel
  MaterialObj getMaterial() {
      return material;
  }
  void setMaterial(MaterialObj m, int rnd=0);

  Color color;
  void setColor(Color val) {
      color = val;
  }

  // temperature
  #if PHX_ENABLE_TEMPERATURE
  PRIVATE_DEF(float temperature); // welp, that's just the temperature
  float getTemperature() {
      return temperature;
  }
  void setTemperature(float tau) {
      temperature = tau;
      #if PHX_ENABLE_ENV_TEMP==0
      flagActiveTemperature();
      #endif
  }
  void addTemperature(float delta_tau) {
      setTemperature(temperature + delta_tau);
  }
  #endif

  // electricity
  #if PHX_ENABLE_ELECTRICITY
  PRIVATE_DEF(float charge = 0); // welp, that's just the charge
  PRIVATE_DEF(float chargeLifetime = 0); // cumulative resistance gained, when >1 then death
  PRIVATE_DEF(unsigned int chargeDir : 4); // which direction is forbidden to go

  float getCharge() const {
    return charge;
  }
  void setCharge(float c); // defined in "Core/Utils.hpp"
  void addCharge(float c) {
      setCharge(charge + c);
  }
  #endif

  // custom force fields
  #if PHX_ENABLE_CUSTOMGRAVITY
  PRIVATE_DEF(unsigned int forceDir : 3);
  unsigned getForce() {
      return forceDir;
  }
  void setForce(uint8_t dir) {
      forceDir = dir;
      flagAreaActive();
  }
  #endif

  // burning
  #if PHX_ENABLE_BURNING
  PRIVATE_DEF(uint8_t burningStage = 0); // not burning 0 -> 1 -> 2 fully burning
  PRIVATE_DEF(time_t burningTimer); // technical
  uint8_t getBurningStage() {
      return burningStage;
  }
  #endif

  // rigid bodies
  #if PHX_ENABLE_RIGIDBODIES
  RigidBody* getCoveringRigid() {
    return coveringRigid;
  }
  bool getIsRigidFree() {
    return isRigidFree;
  }
  #if PHX_ENABLE_ELECTRICITY
  bool getIsChargeFromBody() {
    return isChargeFromBody;
  }
  #endif
  #endif

  // custom data (not used by Phyxel, can be used in any way for your custom fields, be careful with memory deallocation though)
  CustomFields* customFields = NULL;

  PhyxelData():
    PhyxelData(NULL, Color{0,0,0,0},
    #if PHX_ENABLE_TEMPERATURE
    PHX_MIN_TEMP,
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    PHX_FORCE_DOWN,
    #endif
    0) {}

  PhyxelData(MaterialObj material, Color color,
    #if PHX_ENABLE_TEMPERATURE
    float temperature,
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    uint8_t forceDir,
    #endif
    uint8_t lockMask) {
      this->material = material;
      this->color = color;
      this->lockMask = lockMask;
      wasUpdated = true;
      #if PHX_ENABLE_TEMPERATURE
      this->temperature = temperature;
      #endif
      #if PHX_ENABLE_CUSTOMGRAVITY
      this->forceDir = forceDir;
      #endif
      #if PHX_ENABLE_BURNING
      burningStage = 0;
      #endif
      #if PHX_ENABLE_ELECTRICITY
      chargeDir = PHX_CHARGE_DIR_NODIR;
      isTurning = false;
      #endif

      #if PHX_ENABLE_RIGIDBODIES
      box2DTmpBodies = std::vector<b2Body*>();
      deleteTmpBody = std::vector<bool>();
      isRigidFree = true;
      coveringRigid = NULL;
        #if PHX_ENABLE_ELECTRICITY
        isChargeFromBody = false;
        #endif
      #endif
    }

    ~PhyxelData() {
      delete customFields;
    }

    #if PHX_ENABLE_BURNING
    void tryToBurn() {
        burningStage = std::max((uint8_t)(material->isFlammable), burningStage);
        flagAreaActive();
    }
    #endif

    // some other technical getters
    PhyxelData* getNbrDown() const {
        return NBR_DOWN;
    }
    PhyxelData* getNbrRight() const {
        return NBR_RIGHT;
    }
    PhyxelData* getNbrUp() const {
        return NBR_UP;
    }
    PhyxelData* getNbrLeft() const {
        return NBR_LEFT;
    }
    uint8_t getLockMask() const {
      return lockMask;
    }

    // smart updates
    void flagAreaActive();
    #if PHX_ENABLE_ELECTRICITY
    void flagActiveElectricity();
    #endif
    #if PHX_ENABLE_CHEMISTRY
    void flagActiveChemistry();
    #endif
    #if PHX_ENABLE_ENV_TEMP==0
    void flagActiveTemperature();
    #endif
};

}
