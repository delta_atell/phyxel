// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definition of a chemical reaction. this file is only included when PHX_ENABLE_CHEMISTRY is set to 1.

#pragma once

namespace phx {

class Material; // defined in "Core/Material.hpp"

struct ChemicalReaction {
    Material* newSelf; // change self to this, type is MaterialObj (ignore Material*)
    Material* newOther; // change other to this, type is MaterialObj (ignore Material*)
    #if PHX_ENABLE_TEMPERATURE
    float deltaSelfTemperature; // add this to the temperature
    float deltaOtherTemperature; // add this to the temperature
    float requiredTemperature; // temperature required to be in self for the reation to start
    #endif
    uint16_t prob; // probability of the reaction (0-8191) on each timestep
};

}