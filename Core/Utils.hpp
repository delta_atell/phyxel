// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains the definitions for all the update methods of the Scene class

#pragma once

#include "Scene.hpp"
#include "../Maths/phyxel_maths.hpp"
#include <cmath>
#if PHX_ENABLE_PARALLEL_UPDATE
#include <thread>
#endif

namespace phx {

#if PHX_ENABLE_TEMPERATURE
void Chunk::updateTemperature(unsigned X, unsigned Y) { // only updates temperatures
    int rnd = rand();
    #if PHX_ENABLE_ENV_TEMP==0
    bool res_right = false;
    bool res_bottom = false;
    #endif
    #if PHX_ENABLE_RIGIDBODIES // a sofisticated yet still slow temperature update that includes rigid bodies
        int8_t binrand = rnd & 3;
        unsigned idx_cur = sizeX*Y+X;
        float* tau_right;
        float therm_diff_right;
        if (phyxels[idx_cur].neighbours[1]->isRigidFree) {
            tau_right = &(phyxels[idx_cur].neighbours[1]->temperature);
            therm_diff_right = phyxels[idx_cur].neighbours[1]->material->thermalDiff;
        } else {
            tau_right = &(phyxels[idx_cur].neighbours[1]->coveringRigid->temperature);
            therm_diff_right = phyxels[idx_cur].neighbours[1]->coveringRigid->material->thermalDiff;
        }
        MaterialObj* cur = NULL;
        float therm_diff_cur;
        float* tau_cur = NULL;
        if (phyxels[idx_cur].coveringRigid) {
            // FIXME TODO: this place in the code (as well as potentially another similar piece of code below) can cause a segfault if one uses `World::repositionScene` on a scene with rigid bodies. this is likely to be easily fixed by using smart pointers for all the entities everywhere inside the Phyxel Engine as suggested in a TODO in the `Entity.hpp` file.
            cur = &(phyxels[idx_cur].coveringRigid->material); // current phyxel that we're looking at
            tau_cur = &phyxels[idx_cur].coveringRigid->temperature; // current temperature
            therm_diff_cur = phyxels[idx_cur].coveringRigid->material->thermalDiff;
        } else {
            cur = &(phyxels[idx_cur].material); // current phyxel that we're looking at
            tau_cur = &phyxels[idx_cur].temperature; // current temperature
            therm_diff_cur = phyxels[idx_cur].material->thermalDiff;
        }
        float tau_initial = *tau_cur; // initial temperature

        float delta_tau = (tau_initial - *tau_right);
        #if PHX_ENABLE_ENV_TEMP==0
            res_right |= (abs(delta_tau) > PHX_MIN_DELTA_TEMP && therm_diff_right != 0);
        #endif
        delta_tau *= ((binrand&1) ? therm_diff_cur : therm_diff_right);
        *tau_cur -= delta_tau;
        *tau_right += delta_tau;

        float* tau_lower;
        float therm_diff_lower;
        if (phyxels[idx_cur].neighbours[0]->isRigidFree) {
            tau_lower = &(phyxels[idx_cur].neighbours[0]->temperature);
            therm_diff_lower = phyxels[idx_cur].neighbours[0]->material->thermalDiff;
        } else {
            tau_lower = &(phyxels[idx_cur].neighbours[0]->coveringRigid->temperature);
            therm_diff_lower = phyxels[idx_cur].neighbours[0]->coveringRigid->material->thermalDiff;
        }
        delta_tau = (tau_initial - *tau_lower);
        #if PHX_ENABLE_ENV_TEMP==0
            res_bottom |= (abs(delta_tau) > PHX_MIN_DELTA_TEMP && therm_diff_lower != 0);
        #endif
        delta_tau *= (binrand&2 ? therm_diff_cur : therm_diff_lower);
        *tau_cur -= delta_tau;
        *tau_lower += delta_tau;

        #if PHX_ENABLE_ENV_TEMP
        *tau_cur -= (tau_initial - envTemperature)*envThermalDiff;
        #endif

        if (*tau_cur >= (*cur)->stateMaxTemp) {
            if (phyxels[idx_cur].isRigidFree) {
                phyxels[idx_cur].setMaterial((*cur)->higherTempState, rnd);
            } else {
                *cur = (*cur)->higherTempState;
                phyxels[idx_cur].coveringRigid->phyxelate();
                phyxels[idx_cur].coveringRigid = NULL;
                phyxels[idx_cur].isRigidFree = true;
            }
        } else if (*tau_cur <= (*cur)->stateMinTemp) {
            setMaterial(idx_cur, (*cur)->lowerTempState, rnd);
            if (phyxels[idx_cur].isRigidFree) {
                phyxels[idx_cur].setMaterial((*cur)->lowerTempState, rnd);
            } else {
                *cur = (*cur)->lowerTempState;
                phyxels[idx_cur].coveringRigid->phyxelate();
                phyxels[idx_cur].coveringRigid = NULL;
                phyxels[idx_cur].isRigidFree = true;
            }
        }
    #else // simple old temperature update without rigid bodies
        int8_t binrand = rnd % 2;
        unsigned idx_cur = sizeX*Y+X;
        auto cur = phyxels[idx_cur].material; // current phyxel that we're looking at
        float tau_cur = phyxels[idx_cur].temperature; // current temperature
        float delta_tau = (tau_cur - phyxels[idx_cur].neighbours[1]->temperature);
        #if PHX_ENABLE_ENV_TEMP==0
            res_right |= (abs(delta_tau) > PHX_MIN_DELTA_TEMP && phyxels[idx_cur].neighbours[1]->material->thermalDiff != 0);
        #endif
        delta_tau *= (binrand ? phyxels[idx_cur].neighbours[1] : phyxels + idx_cur)->material->thermalDiff;
        phyxels[idx_cur].temperature -= delta_tau;
        phyxels[idx_cur].neighbours[1]->temperature += delta_tau;
        delta_tau = (tau_cur - phyxels[idx_cur].neighbours[0]->temperature);
        #if PHX_ENABLE_ENV_TEMP==0
            res_bottom |= (abs(delta_tau) > PHX_MIN_DELTA_TEMP && phyxels[idx_cur].neighbours[0]->material->thermalDiff != 0);
        #endif
        delta_tau *= ((binrand ? phyxels[idx_cur].neighbours[0] : phyxels + idx_cur)->material->thermalDiff);
        phyxels[idx_cur].temperature -= delta_tau;
        phyxels[idx_cur].neighbours[0]->temperature += delta_tau;

        #if PHX_ENABLE_ENV_TEMP
        phyxels[idx_cur].temperature -= (tau_cur - envTemperature)*envThermalDiff;
        #endif

        tau_cur = phyxels[idx_cur].temperature;
        if (tau_cur >= cur->stateMaxTemp) {
            cur = cur->higherTempState;
            phyxels[idx_cur].setMaterial(cur, rnd);
        } else if (tau_cur <= cur->stateMinTemp) {
            cur = cur->lowerTempState;
            phyxels[idx_cur].setMaterial(cur, rnd);
        }
    #endif // PHX_ENABLE_RIGIDBODIES
    #if PHX_ENABLE_ENV_TEMP==0
    if (phyxels[idx_cur].material->thermalDiff != 0) {
        if (res_right)
            phyxels[idx_cur].NBR_RIGHT->flagActiveTemperature();
        if (res_bottom)
            phyxels[idx_cur].NBR_DOWN->flagActiveTemperature();
        if ((res_right || res_bottom) && phyxels[idx_cur].NBR_UP->NBR_LEFT->material->thermalDiff != 0)
            phyxels[idx_cur].NBR_UP->NBR_LEFT->flagActiveTemperature();
    }
    #endif
}

#if PHX_ENABLE_ENV_TEMP==0
void PhyxelData::flagActiveTemperature() {
    unsigned offset = (this - myChunk->phyxels);
    myChunk->flagActiveTemperatureLocal((offset & (PHX_CHUNK_SIZE_X - 1)), (offset >> myChunk->scene.logChunkSizeX));
}
#endif
#endif // PHX_ENABLE_TEMPERATURE

#if PHX_ENABLE_ELECTRICITY
void Chunk::updateElectricity(unsigned X, unsigned Y) { // only updates electricity
    static std::function<PhyxelData*(PhyxelData*)> get_nbr[] = {
        [](PhyxelData * self){return self->NBR_DOWN;},
        [](PhyxelData * self){return self->NBR_DOWN->NBR_RIGHT;},
        [](PhyxelData * self){return self->NBR_RIGHT;},
        [](PhyxelData * self){return self->NBR_RIGHT->NBR_UP;},
        [](PhyxelData * self){return self->NBR_UP;},
        [](PhyxelData * self){return self->NBR_UP->NBR_LEFT;},
        [](PhyxelData * self){return self->NBR_LEFT;},
        [](PhyxelData * self){return self->NBR_LEFT->NBR_DOWN;}
    };
    static uint8_t idx_order[] = {0, 2, 4, 6, 1, 3, 5, 7};

    auto cur_phx = phyxels+sizeX*Y+X;

    //bool keepActive = false;
    if (cur_phx->charge != 0) {
        flagActiveElectricityLocal(X, Y);
        //keepActive = true;
        #if PHX_ENABLE_RIGIDBODIES
        auto phx_body = cur_phx->coveringRigid;
        #endif
        auto cur_mat = cur_phx->material;

        // update lifetime & kill charges
        #if PHX_ENABLE_RIGIDBODIES
        cur_phx->chargeLifetime += ((/*cur_phx->isChargeFromBody && */phx_body) ? phx_body->material->resistance : cur_mat->resistance);
        #else
        cur_phx->chargeLifetime += cur_mat->resistance;
        #endif
        if (cur_phx->chargeLifetime >= 1.) {
            cur_phx->charge = 0;
            cur_phx->chargeLifetime = 0;
            cur_phx->isTurning = false;
            #if PHX_ENABLE_RIGIDBODIES
            cur_phx->isChargeFromBody = false;
            #endif
            return;
        }

        if (cur_phx->isTurning) {
            // pick a new dir that is not backwards
            phx::shuffle<uint8_t>(idx_order, 4);
            phx::shuffle<uint8_t>(idx_order + 4, 5);
            int idx_shift = 0;
            uint8_t dir = 255;
            uint8_t forbidden_dir = (cur_phx->chargeDir != 0)*((cur_phx->chargeDir+3)&7+1);
            for (uint8_t i = 0; i < 8; ++i) {
                if (idx_order[i]+1 != forbidden_dir) {
                    dir = idx_order[i];
                } else {
                    dir = cur_phx->chargeDir-1;
                }
                auto try_phx = get_nbr[dir](cur_phx);
                auto try_mat = try_phx->material;
                #if PHX_ENABLE_RIGIDBODIES
                if (!(try_phx->isRigidFree) && try_phx->coveringRigid->material->resistance < 1 && try_phx->charge==0) {
                    try_phx->isChargeFromBody = true;
                    break;
                } else
                #endif
                if (try_mat->resistance < 1 && try_phx->charge==0) break;
            }

            // roll-out loop
            auto loop_phx = cur_phx;
            for (unsigned _ = 0; _ < PHX_ELECTRIC_SPREAD; ++_) {
                auto loop_next_phx = get_nbr[dir](loop_phx);
                #if PHX_ENABLE_RIGIDBODIES
                if (!(loop_next_phx->isRigidFree) && loop_next_phx->coveringRigid->material->resistance < 1) {
                    loop_phx->isChargeFromBody = true;
                    loop_phx = loop_next_phx;
                }
                else
                #endif
                if (loop_next_phx->material->resistance < 1) loop_phx = loop_next_phx;
                else {
                    bool free_found = false;
                    forbidden_dir = (dir != 255)*((dir+4)&7+1);
                    for (uint8_t i = 0; i < 8; ++i) {
                        if (idx_order[i]+1 != forbidden_dir) {
                            dir = idx_order[i];
                        } else continue;
                        auto try_phx = get_nbr[dir](cur_phx);
                        auto try_mat = try_phx->material;
                        #if PHX_ENABLE_RIGIDBODIES
                        if (!(try_phx->isRigidFree) && try_phx->coveringRigid->material->resistance < 1 && try_phx->charge==0) {
                            try_phx->isChargeFromBody = true;
                            free_found = true;
                            break;
                        } else
                        #endif
                        if (try_mat->resistance < 1 && try_phx->charge==0) {
                            free_found = true;
                            break;
                        }
                    }
                    if (!free_found) break;
                }

                #if PHX_ENABLE_TEMPERATURE
                #if PHX_ENABLE_RIGIDBODIES
                if (!(loop_phx->isRigidFree)) loop_phx->coveringRigid->temperature += PHX_ELECTRIC_HEAT * cur_phx->charge;
                else
                #endif
                loop_phx->temperature += PHX_ELECTRIC_HEAT * cur_phx->charge;
                #endif
                loop_phx->charge = cur_phx->charge;
                loop_phx->flagActiveElectricity();
                loop_phx->chargeLifetime = cur_phx->chargeLifetime;
                loop_phx->chargeDir = dir+1;
            }
            loop_phx->isTurning = true;
        }
    }
}

void PhyxelData::setCharge(float c) {
    charge = c;
    isTurning = true;

    // smart update
    flagActiveElectricity();
}

void PhyxelData::flagActiveElectricity() {
    unsigned offset = (this - myChunk->phyxels);
    unsigned X = (offset & (PHX_CHUNK_SIZE_X - 1));
    unsigned Y = (offset >> myChunk->scene.logChunkSizeX);
    myChunk->flagActiveElectricityLocal(X, Y);
}
#endif // PHX_ENABLE_ELECTRICITY

void PhyxelData::flagAreaActive() {
    unsigned offset = (this - myChunk->phyxels);
    unsigned X = myChunk->phyxelOffsetX + (offset & (PHX_CHUNK_SIZE_X - 1));
    unsigned Y = myChunk->phyxelOffsetY + (offset >> myChunk->scene.logChunkSizeX);

    myChunk->flagActive(X, Y);
    NBR_LEFT->myChunk->flagActive(X-1, Y);
    NBR_RIGHT->myChunk->flagActive(X+1, Y);
    NBR_UP->myChunk->flagActive(X, Y-1);
    NBR_DOWN->myChunk->flagActive(X, Y+1);

    NBR_LEFT->NBR_UP->myChunk->flagActive(X-1, Y-1);
    NBR_RIGHT->NBR_UP->myChunk->flagActive(X+1, Y-1);
    NBR_LEFT->NBR_DOWN->myChunk->flagActive(X-1, Y+1);
    NBR_RIGHT->NBR_DOWN->myChunk->flagActive(X+1, Y+1);
}

#if PHX_ENABLE_CHEMISTRY
void Chunk::updateChemistry(unsigned X, unsigned Y) { // only updates chemistry
    unsigned index_cur = sizeX*Y+X;
    auto cur_material = phyxels[index_cur].material; // the material of the current phyxel we're looking at
    auto table = cur_material->chemTable; // can be NULL
    #if PHX_ENABLE_TEMPERATURE
    float tau_cur = phyxels[index_cur].temperature; // current temperature
    #endif
    
    if (
        table
        #if PHX_ENABLE_RIGIDBODIES
        || (phyxels[index_cur].coveringRigid && !(phyxels[index_cur].coveringRigid->toDelete) && phyxels[index_cur].coveringRigid->material->chemTable)
        #endif
    ) {
        int rnd = rand() & 8191;
        int8_t rnd_axis = rnd % 2, dir = (rand()%2)*2 - 1;

        PhyxelData* cur_neighbours[4] = {
            phyxels[index_cur].NBR_DOWN,
            phyxels[index_cur].NBR_RIGHT,
            phyxels[index_cur].NBR_UP,
            phyxels[index_cur].NBR_LEFT
        };
        phx::shuffle<PhyxelData*>(cur_neighbours, 4);

        #if PHX_ENABLE_RIGIDBODIES
        bool from_rigid = false; // whether the self in the reaction comes from the phyxel or the rigid that covers it
        for (uint8_t phyx_or_rigid = 0; phyx_or_rigid < 2; ++phyx_or_rigid) { // this shows where the chemTable comes from 0 - phyxel check, 1 - rigid check
            if (phyx_or_rigid) {
                if (phyxels[index_cur].isRigidFree || phyxels[index_cur].coveringRigid->toDelete) continue;
                table = phyxels[index_cur].coveringRigid->material->chemTable;
                from_rigid = true;
                #if PHX_ENABLE_TEMPERATURE
                tau_cur = phyxels[index_cur].coveringRigid->temperature;
                #endif
            }
            if (!table) continue;
        #endif

        bool notFlaggedActive = true;
        for (uint8_t i = 0; i < 4; ++i) { // loop over neighbours
            #if PHX_ENABLE_RIGIDBODIES
            bool for_rigid = (cur_neighbours[i]->coveringRigid && !(cur_neighbours[i]->coveringRigid->toDelete)); // is reaction caused by the material of the attached rigid body or no (meaning whether the Other component of the reaction came from the material of the phyxel or the rigid body above it) // FIXME: this check repeats the check above
            if (from_rigid && for_rigid && (cur_neighbours[i]->coveringRigid == phyxels[index_cur].coveringRigid)) {
                continue; // we're not checking interactions of the body with its own phyxels
            }
            auto found = (for_rigid ? table->find(cur_neighbours[i]->coveringRigid->material) : table->find(cur_neighbours[i]->material));
            if (for_rigid && found == table->end()) {
                found = table->find(cur_neighbours[i]->material);
                for_rigid = false;
            }
            #else
            auto found = table->find(cur_neighbours[i]->material);
            #endif
            if (found == table->end()) continue;
            if (notFlaggedActive) {
                phyxels[index_cur].flagActiveChemistry();
                notFlaggedActive = false;
            }
            ChemicalReaction* react = &(found->second);
            if (
                #if PHX_ENABLE_TEMPERATURE
                (react->requiredTemperature <= tau_cur) &&
                #endif
                rnd <= react->prob
            ) {
                cur_material = react->newSelf;
                #if PHX_ENABLE_RIGIDBODIES
                if (from_rigid && phyxels[index_cur].coveringRigid && react->newSelf != phyxels[index_cur].coveringRigid->material) { // FIXME: phyxels[index_cur].coveringRigid ?? it might be checked above ONCE
                    phyxels[index_cur].coveringRigid->phyxelate();
                    phyxels[index_cur].coveringRigid = NULL;
                    phyxels[index_cur].isRigidFree = true;
                    setMaterial(index_cur, cur_material, rnd);
                } else if (!from_rigid) {
                    setMaterial(index_cur, cur_material, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    phyxels[index_cur].addTemperature(react->deltaSelfTemperature);
                    #endif
                }
                if (for_rigid && react->newOther != cur_neighbours[i]->coveringRigid->material) {
                    cur_neighbours[i]->coveringRigid->phyxelate();
                    cur_neighbours[i]->coveringRigid = NULL;
                    cur_neighbours[i]->isRigidFree = true;
                    cur_neighbours[i]->setMaterial(react->newOther, rnd);
                } else if (!for_rigid) {
                    cur_neighbours[i]->setMaterial(react->newOther, rnd);
                    #if PHX_ENABLE_TEMPERATURE
                    cur_neighbours[i]->addTemperature(react->deltaOtherTemperature);
                    #endif
                }
                #else
                setMaterial(index_cur, cur_material, rnd);
                cur_neighbours[i]->setMaterial(react->newOther, rnd);
                #if PHX_ENABLE_TEMPERATURE
                phyxels[index_cur].addTemperature(react->deltaSelfTemperature);
                cur_neighbours[i]->addTemperature(react->deltaOtherTemperature);
                #endif
                #endif
                return;
            }
        }
        #if PHX_ENABLE_RIGIDBODIES
        } // closes the for
        #endif
    }
    return;
}

void Chunk::flagAreaActiveChemistry(unsigned X, unsigned Y) {
    if (X >= PHX_SCENE_SIZE_X || Y >= PHX_SCENE_SIZE_Y) return;
    unsigned _X = X - phyxelOffsetX;
    unsigned _Y = Y - phyxelOffsetY;
    flagActiveChemistryLocal(_X, _Y);
    auto cur = phyxels + _Y*sizeX + _X;
    if (X>0) cur->NBR_LEFT->myChunk->flagActiveChemistry(X-1, Y);
    cur->NBR_RIGHT->myChunk->flagActiveChemistry(X+1, Y);
    if (Y>0) cur->NBR_UP->myChunk->flagActiveChemistry(X, Y-1);
    cur->NBR_DOWN->myChunk->flagActiveChemistry(X, Y+1);
}

void PhyxelData::flagActiveChemistry() {
    unsigned offset = (this - myChunk->phyxels);
    myChunk->flagActiveChemistryLocal((offset & (PHX_CHUNK_SIZE_X - 1)), (offset >> myChunk->scene.logChunkSizeX));
}
#endif // PHX_ENABLE_CHEMISTRY

#if PHX_ENABLE_BURNING
void Chunk::updateBurning(unsigned X, unsigned Y) { // only updates what's burning
    bool res;
    auto cur = phyxels + sizeX*Y+X; // current phyxel that we're looking at
    auto cur_material = cur->material;

    if (!cur_material->isFlammable)
        return;

    int rnd = rand();
    PhyxelData* nbrs8[] {
        cur->NBR_LEFT, // phyxels + index_cur-1,
        cur->NBR_RIGHT, // phyxels + index_cur+1,
        cur->NBR_UP, // phyxels + index_cur-sizeX,
        cur->NBR_DOWN, // phyxels + index_cur+sizeX,
        cur->NBR_UP->NBR_RIGHT, // phyxels + index_cur-sizeX+1,
        cur->NBR_UP->NBR_LEFT, // phyxels + index_cur-sizeX-1,
        cur->NBR_DOWN->NBR_RIGHT, // phyxels + index_cur+sizeX+1,
        cur->NBR_DOWN->NBR_LEFT // phyxels + index_cur+sizeX-1
    };
    switch(cur->burningStage) {
        #if PHX_ENABLE_TEMPERATURE
        case 0: // not burning
        if(cur->temperature >= cur_material->burningTemperature) {
            cur->burningStage = 1;
            cur->burningTimer = 10;
            return;
        }
        return;
        #endif

        case 1: // ready to burn but no oxygen nearby
        res = true;
        for (uint8_t i=0; i<8; ++i) {
            MaterialObj* nbrMat;
            uint8_t* nbrStage;
            #if PHX_ENABLE_RIGIDBODIES
            if (nbrs8[i]->isRigidFree) {
            #endif
                nbrMat = &nbrs8[i]->material;
                nbrStage = &nbrs8[i]->burningStage;
            #if PHX_ENABLE_RIGIDBODIES
            } else {
                auto* nbrRigid = nbrs8[i]->coveringRigid;
                nbrMat = &nbrRigid->material;
                nbrStage = &nbrRigid->burningStage;
            }
            #endif
            if((*nbrMat)->containsO2) {
                cur->burningStage = 2;
                cur->burningTimer = 10;
                return;
            }
            if(res&&(*nbrStage)==2) {
                res = false;
                cur->burningTimer = 10;
            }
        }
        if (!res) return;
        cur->burningStage = (bool)(--cur->burningTimer);
        cur->myChunk->flagActive(X + phyxelOffsetX, Y + phyxelOffsetY);
        return;

        default: // burning
        cur_material->customBurning(&scene, phyxelOffsetX+X, phyxelOffsetY+Y); // the visuals are handled here. customBurning is a functor field and can be changed freely.
        #if PHX_ENABLE_TEMPERATURE
        cur->temperature += PHX_BURNING_HEAT;
        #endif
        cur->flagAreaActive();
        res = (rnd%9) == 0; // res=true means we haven't found oxygen yet
        for (uint8_t i = 0; i < 8; ++i) {
            MaterialObj* nbrMat;
            uint8_t* nbrStage;
            time_t* nbrTimer;
            #if PHX_ENABLE_RIGIDBODIES
            if (nbrs8[i]->isRigidFree) {
            #endif
                nbrMat = &nbrs8[i]->material;
                nbrStage = &nbrs8[i]->burningStage;
                nbrTimer = &nbrs8[i]->burningTimer;
            #if PHX_ENABLE_RIGIDBODIES
            } else {
                auto* nbrRigid = nbrs8[i]->coveringRigid;
                nbrMat = &nbrRigid->material;
                nbrStage = &nbrRigid->burningStage;
                nbrTimer = &nbrRigid->burningTimer;
            }
            #endif

            if ((*nbrMat)->isFlammable && !*nbrStage) {
                *nbrStage = 1;
                *nbrTimer = 10;
            }
            if (res && (*nbrMat)->containsO2) {
                res = false;
                cur->burningTimer = 10;
                #if PHX_ENABLE_RIGIDBODIES
                if (nbrs8[i]->isRigidFree) {
                #endif
                    nbrs8[i]->setMaterial(cur_material->burningGas, rnd);
                #if PHX_ENABLE_RIGIDBODIES
                } else {
                    nbrs8[i]->coveringRigid->phyxelate();
                    nbrs8[i]->coveringRigid = NULL;
                    nbrs8[i]->isRigidFree = true;
                }
                #endif
            }
        }

        if (!res) {
            if (cur->burningStage == cur_material->burningFrames) {
                cur->burningStage = 0;
                cur->setMaterial(cur_material->burningGas, rnd);
            } else ++(cur->burningStage);
            return;
        }
        if((--(cur->burningTimer))==0) {
            cur->burningStage = 0;
            cur->setMaterial(cur_material->burntState, rnd);
        }
        return;
    }
}
#endif // PHX_ENABLE_BURNING

void Scene::updateParticles(void* custom_arguments = NULL) {
    static int8_t randVx[] = {0, 1, 1, 1, 0, -1, -1, -1};
    static int8_t randVy[] = {1, 1, 0, -1, -1, -1, 0, 1};
    MaterialObj tmpMat; Color tmpCol;
    int rnd;
    PhyxelData* nextphyx = NULL;
    for (unsigned i = 0; i < particles.size(); ++i) {
        auto p = particles[i];

        // DELETE PARTICLE IF NEEDED
        if (p->toDelete) {
            /// delete particle
            p->customDestructor(p);
            delete p;
            particles.erase(particles.begin()+i);
            --i;
            continue;
        }

        // UPDATE LIFETIME
        ++(p->lifetime);

        // CUSTOM UPDATE
        bool stop_this_update = p->customUpdate(p, custom_arguments);
        if (stop_this_update) continue;

        //
        rnd = rand();

        // VIRTUAL PARTICLES (THOSE LOOKING FOR A SPACE TO BE PLACED)
        if (p->isVirtual) {
            if (!p->material) { // virtual particles always need to have a valid material
                p->toDelete = true;
                continue;
            }

            auto _X = p->X + p->Vx;
            auto _Y = p->Y + p->Vy;
            PhyxelData* phyx = &((*this)((unsigned)_X, (unsigned)_Y));
            if (
                #if PHX_ENABLE_RIGIDBODIES
                !(phyx->isRigidFree) ||
                #endif
                p->material->type < phyx->material->type) {
                p->Vx = randVx[rnd&7];
                p->Vy = randVy[rnd&7];
                continue;
            }
            if (
                #if PHX_ENABLE_RIGIDBODIES
                phyx->isRigidFree &&
                #endif
                p->material->type > phyx->material->type ||
                    (p->material->type == phyx->material->type && phyx->material->isRemovable == true)) {
                tmpMat = phyx->material;
                tmpCol = phyx->color;
                phyx->material = p->material; // not with setMaterial since the attached box2D body doesn't have to be erased
                phyx->color = p->color;
                #if PHX_ENABLE_BURNING
                if (p->isBurning)
                    phyx->tryToBurn();
                #endif
                phyx->flagAreaActive();
                if(tmpMat->isRemovable) {
                    p->toDelete = true;
                    continue;
                }
                p->material = tmpMat;
                p->color = tmpCol;
                p->Vx = randVx[rnd&7];
                p->Vy = randVy[rnd&7];
            }
            p->X = _X; p->Y = _Y;
            continue;
        }
        if(!p->checkCollisions) {
            // POSITION UPDATE
            p->X += p->Vx * particlesTimeStep;
            p->Y += p->Vy * particlesTimeStep;
        } else if (p->material) {
            int n = ceil(std::max(abs(p->Vx), abs(p->Vy)) * particlesTimeStep); // TODO: check what the heck this is
            float dx = p->Vx * particlesTimeStep / n;
            float dy = p->Vy * particlesTimeStep / n;

            for(int _=0; _<n; ++_) {
                // COMPUTE THE NEXT POSITION
                auto _X = p->X + dx;
                auto _Y = p->Y + dy;
                nextphyx = &((*this)((unsigned)_X, (unsigned)_Y)); // the phyxel located where the particle is going to be

                // CHECKING COLLISIONS
                PhyxelData* thisphyx = &((*this)((unsigned)(p->X), (unsigned)(p->Y))); // the phyxel located directly where the particle is
                #if PHX_ENABLE_BURNING
                if (p->isBurning)
                    thisphyx->tryToBurn();
                #endif

                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    !nextphyx->isRigidFree ||
                    #endif
                    p->material->type <= nextphyx->material->type) {
                    if (
                        #if PHX_ENABLE_RIGIDBODIES
                        !thisphyx->isRigidFree ||
                        #endif
                        p->material->type <= thisphyx->material->type) {
                        p->isVirtual = true;
                        p->Vx = randVx[rnd&7];
                        p->Vy = randVy[rnd&7];
                        stop_this_update = true;
                        break;
                    }
                    if (
                        #if PHX_ENABLE_RIGIDBODIES
                        thisphyx->isRigidFree &&
                        #endif
                        !(thisphyx->material->isRemovable)
                    ) {
                        this->addVirtualParticle( // we pop the removed phyxel out as a virtual particle
                            thisphyx->material,
                            thisphyx->color,
                            (unsigned)(p->X), (unsigned)(p->Y),
                            1, 1
                        );
                    }
                    thisphyx->setMaterial(p->material);
                    thisphyx->color = p->color;
                    #if PHX_ENABLE_BURNING
                    if (p->isBurning)
                        nextphyx->tryToBurn();
                    #endif
                    /// delete particle
                    p->toDelete = true;
                    stop_this_update = true;
                    break;
                }

                // POSITION UPDATE
                p->X = _X;
                p->Y = _Y;
            }
        } else {
            int n = ceil(std::max(abs(p->Vx), abs(p->Vy)) * particlesTimeStep);
            float dx = p->Vx * particlesTimeStep / n;
            float dy = p->Vy * particlesTimeStep / n;

            for(int _=0; _<n; ++_) {
                // COMPUTE THE NEXT POSITION
                auto _X = p->X + dx;
                auto _Y = p->Y + dy;
                nextphyx = &((*this)((unsigned)_X, (unsigned)_Y)); // the phyxel located where the particle is going to be

                // CHECKING COLLISIONS
                PhyxelData* thisphyx = &((*this)((unsigned)(p->X), (unsigned)(p->Y))); // the phyxel located directly where the particle is
                #if PHX_ENABLE_BURNING
                if (p->isBurning)
                    thisphyx->tryToBurn();
                #endif

                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    !nextphyx->isRigidFree ||
                    #endif
                    PHX_MTYPE_LIQ <= nextphyx->material->type
                ) {
                    /// delete particle
                    p->toDelete = true;
                    stop_this_update = true;
                    break;
                }

                // POSITION UPDATE
                p->X = _X;
                p->Y = _Y;
            }
        }

        if(stop_this_update) continue;

        // VELOCITY UPDATE
        if (p->isAffectedByGravity) {
            #if PHX_ENABLE_CUSTOMGRAVITY==0
            p->Vy += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
            #endif
            #if PHX_ENABLE_CUSTOMGRAVITY
            switch(nextphyx->forceDir) {
            // DOWN IS DOWN
                case PHX_FORCE_DOWN:
                p->Vy += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS RIGHT
                case PHX_FORCE_RIGHT:
                p->Vx += particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS UP
                case PHX_FORCE_UP:
                p->Vy -= particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // DOWN IS LEFT
                case PHX_FORCE_LEFT:
                p->Vx -= particlesTimeStep * PHX_FREEFALL_ACCELERATION;
                break;

            // (1/sqrt(2) approximately equals to 0.7)
            // DOWN IS BETWEEN RIGHT AND DOWN
                case PHX_FORCE_DOWNRIGHT:
                p->Vx += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN RIGHT AND UP
                case PHX_FORCE_UPRIGHT:
                p->Vx += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN LEFT AND UP
                case PHX_FORCE_UPLEFT:
                p->Vx -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            // DOWN IS BETWEEN LEFT AND DOWN
                case PHX_FORCE_DOWNLEFT:
                p->Vx -= particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                p->Vy += particlesTimeStep * 0.7 * PHX_FREEFALL_ACCELERATION;
                break;
            }
            #endif
        }
    }
}

void Scene::updateInstances(void* custom_arguments = NULL) { // only updates other instances (not including rigid bodies)
    for (unsigned i = 0; i < instances.size(); ++i) {
        auto inst = instances[i];

        // DELETE INSTANCE IF NEEDED
        if (inst->toDelete) {
            /// delete instance
            delete inst;
            instances.erase(instances.begin()+i);
            --i;
            continue;
        }

        // UPDATE LIFETIME
        ++(inst->lifetime);

        // CUSTOM UPDATE
        inst->customUpdate(inst, custom_arguments);
    }
}

#if PHX_ENABLE_RIGIDBODIES
void Scene::rigidBodyFrameLoop(RigidBody* body) {
    for (auto & coord : body->framePhyxels) {
        unsigned newBodyX = coord.first;
        unsigned newBodyY = coord.second;
        auto& phyx = (*this)(newBodyX, newBodyY);
        phyx.flagAreaActive();
        #if PHX_ENABLE_TEMPERATURE
        #if PHX_ENABLE_ENV_TEMP==0
        phyx.flagActiveTemperature();
        #endif
        #endif
        if (phyx.material->type==PHX_MTYPE_SOL || phyx.material->type==PHX_MTYPE_POD) {
            bool createNewBody = true;
            unsigned foundIdx = 0;
            for (unsigned i = 0, e = phyx.box2DTmpBodies.size(); i < e; ++i) {
                auto* phyxBody = phyx.box2DTmpBodies[i];
                auto* fixData = reinterpret_cast<FixtureData*>(phyxBody->GetFixtureList()->GetUserData().pointer);
                if (reinterpret_cast<b2Fixture*>(fixData->data)==body->box2DBody->GetFixtureList()) {
                    createNewBody = false;
                    foundIdx = i;
                    break;
                }
            }
            if (createNewBody) {
                box2DStaticFixtureDef.userData.pointer = reinterpret_cast<uintptr_t>(new FixtureData(body->box2DBody->GetFixtureList(), 0));
                b2Body* newBody = box2DLayer->CreateBody(&box2DStaticBodyDef);
                newBody->CreateFixture(&box2DStaticFixtureDef); // what happened to the fixture
                newBody->SetTransform(
                        b2Vec2{static_cast<float>((newBodyX + 0.5) * PHX_PHYXELS2METERS), static_cast<float>((newBodyY + 0.5) * PHX_PHYXELS2METERS)}, 0.f);
                phyx.box2DTmpBodies.push_back(newBody);
                phyx.deleteTmpBody.push_back(false);
                continue;
            }
            phyx.deleteTmpBody[foundIdx] = false;
        }
    }
}

void Scene::updateRigidBodies(void* custom_arguments = NULL) { // only updates rigid bodies
    for (unsigned i = 0; i < rigidBodies.size(); ++i) {
        auto body = rigidBodies[i];

        /// CUSTOM GRAVITY
        b2Vec2 center = PHX_METERS2PHYXELS * body->box2DBody->GetPosition();

        /// DELETE INSTANCE IF NEEDED
        if (body->toDelete) {
            /// delete body
            delete body;
            rigidBodies.erase(rigidBodies.begin()+i);
            --i;
            //box2DLayer->Step(box2DTimeStep/1000., 1, 1);
            continue;
        }

        if (center.x > sizeX || center.y > sizeY) {
            // When we get to chunks, modify this to sort out the a management orf a a so when we get to chunks vsi podumayu
            // (write the body to disk)
            // TODO: do we need this or is this going to be done in `World::shiftScene`?
            body->toDelete = true;
            continue;
        }

        #if PHX_ENABLE_CUSTOMGRAVITY
        auto& center_phyx = (*this)((unsigned)center.x, (unsigned)center.y);
        double gravity = body->gravForce;
        double gravProj; // projection of gravitational force (takes into account Archimedes' force)

        switch(center_phyx.forceDir) {
        // DOWN IS DOWN
            case PHX_FORCE_DOWN:
            body->box2DBody->ApplyForce(
                b2Vec2(0, - gravity * body->backgroundMaterialMass
                     * body->material->invMass),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS RIGHT
            case PHX_FORCE_RIGHT:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                 * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, -gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS UP
            case PHX_FORCE_UP:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(0.0f, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // DOWN IS LEFT
            case PHX_FORCE_LEFT:
            gravProj = gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, -gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;

        // (1/sqrt(2) approximately equals to 0.7)
        // DOWN IS BETWEEN RIGHT AND DOWN
            case PHX_FORCE_DOWNRIGHT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN RIGHT AND UP
            case PHX_FORCE_UPRIGHT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(gravProj, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN LEFT AND UP
            case PHX_FORCE_UPLEFT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, - gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        // DOWN IS BETWEEN LEFT AND DOWN
            case PHX_FORCE_DOWNLEFT:
            gravProj = 0.7 * gravity * (1 - body->backgroundMaterialMass
                * body->material->invMass);
            body->box2DBody->ApplyForce(
                b2Vec2(-gravProj, gravProj - gravity),
                body->box2DBody->GetWorldCenter(), true);
            break;
        }
        #endif

        #if PHX_ENABLE_CUSTOMGRAVITY==0
        center = PHX_METERS2PHYXELS * body->box2DBody->GetWorldCenter();
        double gravity = PHX_BOX2D_FREEFALL_ACCELERATION*body->box2DBody->GetMass();
        body->box2DBody->ApplyForce(
            b2Vec2(0, - gravity * body->backgroundMaterialMass
                 * body->material->invMass),
            body->box2DBody->GetWorldCenter(), true);
        #endif

        // Slow down the object due to the medium's viscosity
        body->box2DBody->ApplyForce(
            - 0.3 * body->material->mass * body->box2DBody->GetLinearVelocity(),
            body->box2DBody->GetWorldCenter(), true);

        /// CLEAR THE COVERED PHYXELS
        for (const auto& c : body->coveredPhyxels) {
            auto& phyx = (*this)(c.first, c.second);
            phyx.isRigidFree = true;
            phyx.coveringRigid = NULL;
        }

        rigidBodyFrameLoop(body);

        /// UPDATE LIFETIME
        ++(body->lifetime);

        // CUSTOM UPDATE
        body->customUpdate(body, custom_arguments);

        // UPDATE INNER POSITION DETAILS
        body->recalculate();
    }
    // bodies step
    box2DLayer->Step(box2DTimeStep, box2DVelocityIterations, box2DPositionIterations);
}

void Chunk::cleanupTmpBodies(unsigned X, unsigned Y) {
    auto* phyx = phyxels + sizeX * Y + X;
    auto& bodies = phyx->box2DTmpBodies;
    auto& delBody = phyx->deleteTmpBody;
    for (unsigned i = 1, e = bodies.size()+1; i < e; ++i) {
        unsigned n = i - 1;
        if (delBody[n]) {
            auto* fixData = reinterpret_cast<FixtureData*>(bodies[n]->GetFixtureList()->GetUserData().pointer);
            delete fixData;
            scene.box2DLayer->DestroyBody(bodies[n]);
            bodies.erase(bodies.begin()+n);
            delBody.erase(delBody.begin()+n);
            --i;
            --e;
            continue;
        }
        delBody[n] = true;
    }
}
#endif // PHX_ENABLE_RIGIDBODIES

void Chunk::setPhyxelNeighbours() {
    // set up the neighbours for each phyxel
    for (unsigned X = 1; X < sizeX-1; ++X) {
        // upper edge
        {
            PhyxelData** nbrs = phyxels[X].neighbours;
            nbrs[0] = phyxels + sizeX + X;
            nbrs[1] = phyxels + X + 1;
            if (Chunk* nbrChunk = NBR_UP)
                nbrs[2] = nbrChunk->phyxels + (sizeY-1)*sizeX+X;
            else
                nbrs[2] = scene.bedrock;
            nbrs[3] = phyxels+ X - 1;
        }

        // inner part
        for (unsigned Y = 1; Y < sizeY - 1; ++Y) {
            PhyxelData** nbrs = phyxels[Y*sizeX+X].neighbours;
            nbrs[0] = phyxels + (Y+1)*sizeX+X;
            nbrs[1] = phyxels + Y*sizeX+(X+1);
            nbrs[2] = phyxels + (Y-1)*sizeX+X;
            nbrs[3] = phyxels + Y*sizeX+(X-1);
        }

        // bottom edge
        {
            PhyxelData** nbrs = phyxels[(sizeY-1)*sizeX+X].neighbours;
            if (Chunk* nbrChunk = NBR_DOWN)
                nbrs[0] = nbrChunk->phyxels + X;
            else
                nbrs[0] = scene.bedrock;
            nbrs[1] = phyxels + (sizeY-1)*sizeX+(X+1);
            nbrs[2] = phyxels + (sizeY-2)*sizeX+X;
            nbrs[3] = phyxels + (sizeY-1)*sizeX+(X-1);
        }
    }

    for (unsigned Y = 1; Y < sizeY - 1; ++Y) {
        //  left edge
        {
            PhyxelData** nbrs = phyxels[Y*sizeX].neighbours;
            nbrs[0] = phyxels + (Y+1)*sizeX;
            nbrs[1] = phyxels + Y*sizeX+1;
            nbrs[2] = phyxels + (Y-1)*sizeX;
            if (Chunk* nbrChunk = NBR_LEFT)
                nbrs[3] = nbrChunk->phyxels + (Y+1)*sizeX-1;
            else
                nbrs[3] = scene.bedrock;
        }

        // right edge
        {
            PhyxelData** nbrs = phyxels[(Y+1)*sizeX-1].neighbours;
            nbrs[0] = phyxels + (Y+2)*sizeX-1;
            if (Chunk* nbrChunk = NBR_RIGHT)
                nbrs[1] = nbrChunk->phyxels + Y*sizeX;
            else
                nbrs[1] = scene.bedrock;
            nbrs[2] = phyxels + Y*sizeX-1;
            nbrs[3] = phyxels + (Y+1)*sizeX-2;
        }
    }

    // left upper corner
    {
        PhyxelData** nbrs = phyxels[0].neighbours;
        nbrs[0] = phyxels + sizeX;
        nbrs[1] = phyxels + 1;
        if (Chunk* nbrChunk = NBR_UP)
            nbrs[2] = nbrChunk->phyxels + (sizeY-1)*sizeX;
        else
            nbrs[2] = scene.bedrock;
        if (Chunk* nbrChunk = NBR_LEFT)
            nbrs[3] = nbrChunk->phyxels + sizeX-1;
        else
            nbrs[3] = scene.bedrock;
    }

    // right upper corner
    {
        PhyxelData** nbrs = phyxels[sizeX-1].neighbours;
        nbrs[0] = phyxels + 2*sizeX-1;
        if (Chunk* nbrChunk = NBR_RIGHT)
            nbrs[1] = nbrChunk->phyxels;
        else
            nbrs[1] = scene.bedrock;
        if (Chunk* nbrChunk = NBR_UP)
            nbrs[2] = nbrChunk->phyxels + sizeY*sizeX-1;
        else
            nbrs[2] = scene.bedrock;
        nbrs[3] = phyxels + sizeX-2;
    }

    // left bottom corner
    {
        PhyxelData** nbrs = phyxels[(sizeY-1)*sizeX].neighbours;
        if (Chunk* nbrChunk = NBR_DOWN)
            nbrs[0] = &nbrChunk->phyxels[0];
        else
            nbrs[0] = scene.bedrock;
        nbrs[1] = phyxels + (sizeY-1)*sizeX + 1;
        nbrs[2] = phyxels + (sizeY-2)*sizeX;
        if (Chunk* nbrChunk = NBR_LEFT)
            nbrs[3] = nbrChunk->phyxels + sizeY*sizeX-1;
        else
            nbrs[3] = scene.bedrock;
    }

    // right bottom corner
    {
        PhyxelData** nbrs = phyxels[sizeY*sizeX-1].neighbours;
        if (Chunk* nbrChunk = NBR_DOWN)
            nbrs[0] = nbrChunk->phyxels + sizeX-1;
        else
            nbrs[0] = scene.bedrock;
        if (Chunk* nbrChunk = NBR_RIGHT)
            nbrs[1] = nbrChunk->phyxels + (sizeY-1)*sizeX;
        else
            nbrs[1] = scene.bedrock;
        nbrs[2] = phyxels + (sizeY-1)*sizeX - 1;
        nbrs[3] = phyxels + sizeY*sizeX - 2;
    }
}

void Chunk::updatePhyxelMovement(unsigned X, unsigned Y, bool loopDir) {
    int8_t dir, binrand;
    int rnd;
    PhyxelData *up = NULL, *up_plus = NULL, *up_minus = NULL, *cur_plus = NULL,
         *cur_minus = NULL, *down = NULL, *down_plus = NULL, *down_minus = NULL;

    PhyxelData* cur = phyxels + sizeX * Y + X; // current phyxel that we're looking at
    auto* mat_cur = cur->material;
    binrand = rand() % 2;

    cur->wasUpdated = loopDir;

    if (mat_cur->isPassive) {
        cur->lockMask = 0;
        return;
    }
    if (cur->lockMask>>1) {
        cur->lockMask-=2;
        cur->flagAreaActive();
        return;
    }

    dir = binrand * 2 - 1; // randomly pick a direction to check first
    rnd = rand() & 8191;

    /// GRAVITY
    #if PHX_ENABLE_CUSTOMGRAVITY
    bool isNonDiagonal = true; // tells whether the force field is aligned diagonally or not. this is used in our new liquids' mechanics
    switch (cur->forceDir) {
    // DOWN IS DOWN
        case PHX_FORCE_DOWN:
        down = cur->neighbours[0];
        up = cur->neighbours[2];
        cur_plus = cur->neighbours[2-dir];
        cur_minus = cur->neighbours[2+dir];
        down_plus = cur_plus->neighbours[0];
        down_minus = cur_minus->neighbours[0];
        up_plus = cur_plus->neighbours[2];
        up_minus = cur_minus->neighbours[2];
        break;

    // DOWN IS RIGHT
        case PHX_FORCE_RIGHT:
        down = cur->neighbours[1];
        up = cur->neighbours[3];
        cur_plus = cur->neighbours[1-dir];
        cur_minus = cur->neighbours[1+dir];
        down_plus = cur_plus->neighbours[1];
        down_minus = cur_minus->neighbours[1];
        up_plus = cur_plus->neighbours[3];
        up_minus = cur_minus->neighbours[3];
        break;

    // DOWN IS UP
    case PHX_FORCE_UP:
        down = cur->neighbours[2];
        up = cur->neighbours[0];
        cur_plus = cur->neighbours[2-dir];
        cur_minus = cur->neighbours[2+dir];
        down_plus = cur_plus->neighbours[2];
        down_minus = cur_minus->neighbours[2];
        up_plus = cur_plus->neighbours[0];
        up_minus = cur_minus->neighbours[0];
        break;

    // DOWN IS LEFT
    case PHX_FORCE_LEFT:
        down = cur->neighbours[3];
        up = cur->neighbours[1];
        cur_plus = cur->neighbours[1-dir];
        cur_minus = cur->neighbours[1+dir];
        down_plus = cur_plus->neighbours[3];
        down_minus = cur_minus->neighbours[3];
        up_plus = cur_plus->neighbours[1];
        up_minus = cur_minus->neighbours[1];
        break;

    // DOWN IS BETWEEN RIGHT AND DOWN
    case PHX_FORCE_DOWNRIGHT:
        isNonDiagonal = false;
        down = cur->neighbours[0]->neighbours[1];
        up = cur->neighbours[2]->neighbours[3];
        down_plus = cur->neighbours[1-binrand];
        down_minus = cur->neighbours[binrand];
        cur_plus = down_plus->neighbours[2+binrand];
        cur_minus = down_minus->neighbours[3-binrand];
        up_plus = cur_plus->neighbours[2+binrand];
        up_minus = cur_minus->neighbours[3-binrand];
        break;
    // DOWN IS BETWEEN RIGHT AND UP
    case PHX_FORCE_UPRIGHT:
        isNonDiagonal = false;
        down = cur->neighbours[1]->neighbours[2];
        up = cur->neighbours[3]->neighbours[0];
        down_plus = cur->neighbours[2-binrand];
        down_minus = cur->neighbours[1+binrand];
        cur_plus = down_plus->neighbours[3*!binrand];
        cur_minus = down_minus->neighbours[3*binrand];
        up_plus = cur_plus->neighbours[3*!binrand];
        up_minus = cur_minus->neighbours[3*binrand];
        break;
    // DOWN IS BETWEEN LEFT AND UP
    case PHX_FORCE_UPLEFT:
        isNonDiagonal = false;
        down = cur->neighbours[2]->neighbours[3];
        up = cur->neighbours[0]->neighbours[1];
        down_plus = cur->neighbours[2+binrand];
        down_minus = cur->neighbours[3-binrand];
        cur_plus = down_plus->neighbours[1-binrand];
        cur_minus = down_minus->neighbours[binrand];
        up_plus = cur_minus->neighbours[1-binrand];
        up_minus = cur_plus->neighbours[binrand];
        break;
    // DOWN IS BETWEEN LEFT AND DOWN
    case PHX_FORCE_DOWNLEFT:
        isNonDiagonal = false;
        down = cur->neighbours[3]->neighbours[0];
        up = cur->neighbours[1]->neighbours[2];
        down_plus = cur->neighbours[3*!binrand];
        down_minus = cur->neighbours[3*binrand];
        cur_plus = down_plus->neighbours[2-binrand];
        cur_minus = down_minus->neighbours[1+binrand];
        up_plus = cur_minus->neighbours[2-binrand];
        up_minus = cur_plus->neighbours[1+binrand];
        break;
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY==0
    down = cur->neighbours[0];
    up = cur->neighbours[2];
    cur_plus = cur->neighbours[2-dir];
    cur_minus = cur->neighbours[2+dir];
    down_plus = cur_plus->neighbours[0];
    down_minus = cur_minus->neighbours[0];
    up_plus = cur_plus->neighbours[2];
    up_minus = cur_minus->neighbours[2];
    #endif
    /// END GRAVITY

    auto* mat_down = down->material;
    auto* mat_up = up->material;
    auto* mat_cur_plus = cur_plus->material;
    auto* mat_cur_minus = cur_minus->material;
    auto* mat_down_plus = down_plus->material;
    auto* mat_down_minus = down_minus->material;

    // GASES
    if (mat_cur->type == PHX_MTYPE_GAS) {
        rnd = rand() & 1023;
        cur->lockMask = 0;
        if (
            #if PHX_ENABLE_RIGIDBODIES
            up->isRigidFree &&
            #endif
            mat_up->isPassive && 
            mat_up->type==PHX_MTYPE_GAS &&
            //mat_up != mat_cur &&   <-- current material can't be passive
            mat_up->mass > mat_cur->mass
        ) {
            flagActive(phyxelOffsetX + X, phyxelOffsetY + Y);
            if (scene.gasDiffusion <= rnd * (scene.minDeltaMass + mat_up->mass - mat_cur->mass)) {
                swap(*cur, *up, loopDir);
                return;
            }
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            down->isRigidFree &&
            #endif
            mat_down != mat_cur &&
            mat_down->type==PHX_MTYPE_GAS
        ) {
            flagActive(phyxelOffsetX + X, phyxelOffsetY + Y);
            if (scene.gasDiffusion <= rnd * (scene.minDeltaMass + mat_cur->mass - mat_down->mass)) {
                swap(*cur, *down, loopDir);
                return;
            }
        }

        if (scene.gasDiffusion <= rnd) {
            if (
                #if PHX_ENABLE_RIGIDBODIES
                cur_plus->isRigidFree &&
                #endif
                mat_cur_plus->type==PHX_MTYPE_GAS && mat_cur_plus != mat_cur &&
                mat_cur_plus->mass >= mat_cur->mass
            ) {
                swapNoLock(*cur, *cur_plus);
                return;
            } else if (
                #if PHX_ENABLE_RIGIDBODIES
                cur_minus->isRigidFree &&
                #endif
                mat_cur_minus->type==PHX_MTYPE_GAS && mat_cur_minus != mat_cur &&
                mat_cur_minus->mass >= mat_cur->mass
            ) {
                swapNoLock(*cur, *cur_minus);
                return;
            }
        }
        return;
    }
    // END GASES

    // POWDERS
    if (mat_cur->type == PHX_MTYPE_POD) {
        if (
            #if PHX_ENABLE_RIGIDBODIES
            down->isRigidFree &&
            #endif
        mat_down->type==PHX_MTYPE_GAS || (mat_down->type==PHX_MTYPE_LIQ && mat_down->mass <= mat_cur->mass)) {
            swap(*cur, *down, loopDir);
            return;
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            cur_plus->isRigidFree &&
            #endif
        mat_cur_plus->type!=PHX_MTYPE_POD && (
            #if PHX_ENABLE_RIGIDBODIES
            down_plus->isRigidFree &&
            #endif
        mat_down_plus->type==PHX_MTYPE_GAS || (mat_down_plus->type==PHX_MTYPE_LIQ && mat_down_plus->mass <= mat_cur->mass))) {
            swap(*cur, *down_plus, loopDir);
            return;
        }
        if (
            #if PHX_ENABLE_RIGIDBODIES
            cur_minus->isRigidFree &&
            #endif
        mat_cur_minus->type!=PHX_MTYPE_POD && (
            #if PHX_ENABLE_RIGIDBODIES
            down_minus->isRigidFree &&
            #endif
        mat_down_minus->type==PHX_MTYPE_GAS || (mat_down_minus->type==PHX_MTYPE_LIQ && mat_down_minus->mass <= mat_cur->mass))) {
            swap(*cur, *down_minus, loopDir);
            return;
        }
        cur->lockMask = 0;
        return;
    }
    // END POWDERS

    // LIQUIDS
    if (mat_cur->type == PHX_MTYPE_LIQ) {
        rnd = rand()&8191;
        // falling
        #if PHX_ENABLE_RIGIDBODIES
        if (down->isRigidFree)
        #endif
        if (mat_down->type==PHX_MTYPE_GAS || (mat_down->type==PHX_MTYPE_LIQ && mat_down->mass < mat_cur->mass)
        ||(mat_down->type==PHX_MTYPE_POD && mat_down->mass <= mat_cur->mass)) {
            swap(*cur, *down, loopDir);
            return;
        }
        if (rnd < mat_cur->viscosity) {
            if (mat_cur_minus->type==PHX_MTYPE_GAS || mat_cur_plus->type==PHX_MTYPE_GAS)
                flagActive(phyxelOffsetX + X, phyxelOffsetY + Y);
            return;
        }
        if (scene.gasDiffusion <= rnd) {
            if(
            #if PHX_ENABLE_RIGIDBODIES
            cur_plus->isRigidFree &&
            #endif
            mat_cur_plus->type==PHX_MTYPE_LIQ && mat_cur_plus!=mat_cur && mat_cur_plus->mass <= mat_cur->mass) {
                swapNoLock(*cur, *cur_plus);
                return;
            }
            if(
            #if PHX_ENABLE_RIGIDBODIES
            cur_minus->isRigidFree &&
            #endif
            mat_cur_minus->type==PHX_MTYPE_LIQ && mat_cur_minus!=mat_cur && mat_cur_minus->mass <= mat_cur->mass) {
                swapNoLock(*cur, *cur_minus);
                return;
            }
        }
        // spreading
        #if PHX_ENABLE_CUSTOMGRAVITY
        if (isNonDiagonal) {
        #endif
        // spreading 2.0 (for non-diagonal gravity only), also works when no custom gravity/force fields are enabled
        rnd &= 1023;
        unsigned travelLimit = inv_log_normal_CDF(rnd); // the travel limit is generated according to a log-normal distribution
        if (travelLimit < 2) {
            if (mat_cur_minus->type==PHX_MTYPE_GAS || mat_cur_plus->type==PHX_MTYPE_GAS)
                flagActive(phyxelOffsetX + X, phyxelOffsetY + Y);
            return;
        }
        auto* iter_plus = cur_plus;
        auto* iter_minus = cur_minus;
        auto* iter_down_plus = down_plus;
        auto* iter_down_minus = down_minus;

        PhyxelData* iter_plus_prev = cur;
        PhyxelData* iter_minus_prev = cur;

        unsigned index_plus, index_minus;

        index_plus = 2 - dir;
        index_minus = 2 + dir;
        #if PHX_ENABLE_CUSTOMGRAVITY
        switch (cur->forceDir) {
            case PHX_FORCE_RIGHT:
            case PHX_FORCE_LEFT:
                index_plus = 1 - dir;
                index_minus = 1 + dir;
            break;
        }
        #endif

        PhyxelData* phyxToSwap = NULL;
        bool goLower = false;
        unsigned i_plus = 1;
        for (; i_plus < travelLimit; ++i_plus) {
            #if PHX_ENABLE_RIGIDBODIES
            if (iter_down_plus->isRigidFree)
            #endif
            if (iter_down_plus->material->type==PHX_MTYPE_GAS ||
            (iter_down_plus->material->type==PHX_MTYPE_POD &&
            iter_down_plus->material->mass <= mat_cur->mass)) {
                phyxToSwap = iter_down_plus;
                goLower = true;
                break;
            }

            if (
            #if PHX_ENABLE_RIGIDBODIES
            !iter_plus->isRigidFree ||
            #endif
            iter_plus->material->type!=PHX_MTYPE_GAS) {
                --i_plus;
                break;
            }

            phyxToSwap = iter_plus;
            iter_plus = iter_plus->neighbours[index_plus];
            iter_down_plus = iter_down_plus->neighbours[index_plus];
        }

        bool noObstacleFound = true;
        for (unsigned i = 1; i <= i_plus; ++i) {
            #if PHX_ENABLE_RIGIDBODIES
            if (iter_down_minus->isRigidFree)
            #endif
            if (iter_down_minus->material->type==PHX_MTYPE_GAS ||
            (iter_down_minus->material->type==PHX_MTYPE_POD &&
            iter_down_minus->material->mass <= mat_cur->mass)) {
                phyxToSwap = iter_down_minus;
                goLower = true;
                noObstacleFound = false;
                break;
            }

            if (
            #if PHX_ENABLE_RIGIDBODIES
            !iter_minus->isRigidFree ||
            #endif
            iter_minus->material->type!=PHX_MTYPE_GAS) {
                noObstacleFound = false;
                break;
            }
            iter_minus = iter_minus->neighbours[index_minus];
            iter_down_minus = iter_down_minus->neighbours[index_minus];
        }

        if (noObstacleFound)
            for (unsigned i = i_plus + 1; i <= travelLimit; ++i) {
                #if PHX_ENABLE_RIGIDBODIES
                if (iter_down_minus->isRigidFree)
                #endif
                if (iter_down_minus->material->type==PHX_MTYPE_GAS ||
                (iter_down_minus->material->type==PHX_MTYPE_POD &&
                iter_down_minus->material->mass <= mat_cur->mass)) {
                    phyxToSwap = iter_down_minus;
                    goLower = true;
                    break;
                }

                if (
                #if PHX_ENABLE_RIGIDBODIES
                !iter_minus->isRigidFree ||
                #endif
                iter_minus->material->type!=PHX_MTYPE_GAS)
                    break;

                phyxToSwap = iter_minus;
                iter_minus = iter_minus->neighbours[index_minus];
                iter_down_minus = iter_down_minus->neighbours[index_minus];
            }

        if (!phyxToSwap) {
            cur->lockMask = 0;
            return;
        }

        if (goLower) {
            swap(*cur, *phyxToSwap, loopDir);
            cur->lockMask = 0;
            --phyxToSwap->lockMask;
            return;
        }
        swapNoLock(*cur, *phyxToSwap);
        cur->lockMask = 0;
        return;
        #if PHX_ENABLE_CUSTOMGRAVITY
        // old speading mechanics, still used for diagonally aligned force fields
        } else { // closes the if (is Vertical) {
            if (mat_cur_plus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                (down_plus->isRigidFree) &&
                #endif
                (mat_down_plus->type==PHX_MTYPE_GAS || (mat_down_plus->type==PHX_MTYPE_POD && mat_down_plus->mass <= mat_cur->mass))) {
                swap(*cur, *down_plus, loopDir);
                return;
            }
            if (mat_cur_plus->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                down_minus->isRigidFree &&
                #endif
                (mat_down_minus->type==PHX_MTYPE_GAS || (mat_down_minus->type==PHX_MTYPE_POD && mat_down_minus->mass <= mat_cur->mass))) {
                swap(*cur, *down_minus, loopDir);
                return;
            }
            if (
                #if PHX_ENABLE_RIGIDBODIES
                down->isRigidFree &&
                #endif
                mat_down->type==PHX_MTYPE_LIQ) {
                if (mat_cur->mass > mat_down->mass && scene.verticalLiqDiffusion <= rnd * (mat_cur->mass - mat_down->mass)) {
                    swap(*cur, *down, loopDir);
                    return;
                }
            }
            if (up_plus->material->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                cur_plus->isRigidFree &&
                #endif
                (mat_cur_plus->type==PHX_MTYPE_GAS || (mat_cur_plus->type==PHX_MTYPE_POD && mat_cur_plus->mass <= mat_cur->mass))) {
                swapNoLock(*cur, *cur_plus);
                cur->lockMask = 0;
                return;
            }
            if (up_minus->material->type!=PHX_MTYPE_LIQ &&
                #if PHX_ENABLE_RIGIDBODIES
                cur_minus->isRigidFree &&
                #endif
                (mat_cur_minus->type==PHX_MTYPE_GAS || (mat_cur_minus->type==PHX_MTYPE_POD && mat_cur_minus->mass <= mat_cur->mass))) {
                swapNoLock(*cur, *cur_minus);
                cur->lockMask = 0;
                return;
            }

            if (scene.verticalLiqDiffusion <= rnd) {
                if (
                    #if PHX_ENABLE_RIGIDBODIES
                    (cur_plus->isRigidFree) &&
                    #endif
                    mat_cur_plus->type==PHX_MTYPE_LIQ) {
                    swapNoLock(*cur, *cur_plus);
                    cur->lockMask = 0;
                } else if (
                    #if PHX_ENABLE_RIGIDBODIES
                    cur_minus->isRigidFree &&
                    #endif
                    mat_cur_minus->type==PHX_MTYPE_LIQ) {
                    swapNoLock(*cur, *cur_minus);
                    cur->lockMask = 0;
                }
                return;
            }
            cur->lockMask = 0;
            return;
        }
        #endif
    }
    // END LIQUIDS
    /// END MOVEMENT
    return;
}

void PhyxelData::setMaterial(MaterialObj m, int rnd) {
    flagAreaActive();
    material = m;
    color = m->colors[rnd%(m->colors.size())];
    #if PHX_ENABLE_RIGIDBODIES
    for (auto* body : box2DTmpBodies) {
        auto* fixData = reinterpret_cast<FixtureData*>(body->GetFixtureList()->GetUserData().pointer);
        delete fixData;
        myChunk->scene.box2DLayer->DestroyBody(body);
    }
    box2DTmpBodies.clear();
    deleteTmpBody.clear();
    #endif
}

void Chunk::swapNoLock(PhyxelData& phyxel1, PhyxelData& phyxel2) { // swap phyxels without locking them
    scene.swapNoLock(phyxel1, phyxel2);
}

template<>
void Chunk::updateAllPhyxels<true>() { // combined (temperature + chemistry + burning + electricity + movement) update for every phyxel in the Chunk
    // smart updates
    #if PHX_ENABLE_ELECTRICITY
    UpdateRegion<int> oldElectricityUpdateRegion = electricityUpdateRegion;
    electricityUpdateRegion.reset();
    #endif
    #if PHX_ENABLE_CHEMISTRY
    UpdateRegion<int> oldChemistryUpdateRegion = chemistryUpdateRegion;
    chemistryUpdateRegion.reset();
    #endif
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0
    UpdateRegion<int> oldTemperatureUpdateRegion = temperatureUpdateRegion;
    temperatureUpdateRegion.reset();
    #endif
    #endif
                
    UpdateRegion<int> oldOtherUpdateRegion = otherUpdateRegion;
    otherUpdateRegion.reset();

    for (unsigned Y = 0; Y < sizeY; ++Y)
        for (unsigned X = 0; X < sizeX; ++X) {
            phyxels[sizeX*Y+X].wasUpdated = 0;
            phyxels[sizeX*Y+X].material->customUpdate(&scene, phyxelOffsetX+X, phyxelOffsetY+Y, true); // custom update step
            #if PHX_ENABLE_TEMPERATURE
            #if PHX_ENABLE_ENV_TEMP
            updateTemperature(X, Y);
            #endif
            #endif
        }
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0
    for (unsigned Y = oldTemperatureUpdateRegion.upperBoundary; Y <= oldTemperatureUpdateRegion.lowerBoundary; ++Y)
        for (unsigned X = oldTemperatureUpdateRegion.leftBoundary; X <= oldTemperatureUpdateRegion.rightBoundary; ++X)
            updateTemperature(X, Y);
    #endif
    #endif
    #if PHX_ENABLE_ELECTRICITY
    for (unsigned Y = oldElectricityUpdateRegion.upperBoundary; Y <= oldElectricityUpdateRegion.lowerBoundary; ++Y)
        for (unsigned X = oldElectricityUpdateRegion.leftBoundary; X <= oldElectricityUpdateRegion.rightBoundary; ++X)
            updateElectricity(X, Y);
    #endif
    #if PHX_ENABLE_CHEMISTRY
    for (unsigned Y = oldChemistryUpdateRegion.upperBoundary; Y <= oldChemistryUpdateRegion.lowerBoundary; ++Y)
        for (unsigned X = oldChemistryUpdateRegion.leftBoundary; X <= oldChemistryUpdateRegion.rightBoundary; ++X)
            updateChemistry(X, Y);
    #endif
    for (unsigned Y = oldOtherUpdateRegion.upperBoundary; Y <= oldOtherUpdateRegion.lowerBoundary; ++Y)
        for (unsigned X = oldOtherUpdateRegion.leftBoundary; X <= oldOtherUpdateRegion.rightBoundary; ++X) {
            #if PHX_ENABLE_BURNING
            updateBurning(X, Y);
            #endif
            updatePhyxelMovement(X, Y);
        }
}
template<>
void Chunk::updateAllPhyxels<false>() { // combined (temperature + chemistry + burning + electricity + movement) update for every phyxel in the Chunk (another loop direction)
    // smart updates
    #if PHX_ENABLE_ELECTRICITY
    UpdateRegion<int> oldElectricityUpdateRegion = electricityUpdateRegion;
    electricityUpdateRegion.reset();
    #endif
    #if PHX_ENABLE_CHEMISTRY
    UpdateRegion<int> oldChemistryUpdateRegion = chemistryUpdateRegion;
    chemistryUpdateRegion.reset();
    #endif
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0
    UpdateRegion<int> oldTemperatureUpdateRegion = temperatureUpdateRegion;
    temperatureUpdateRegion.reset();
    #endif
    #endif
                
    UpdateRegion<int> oldOtherUpdateRegion = otherUpdateRegion;
    otherUpdateRegion.reset();

    for (int Y = sizeY-1; Y >= 0; --Y)
        for (int X = sizeX-1; X >= 0; --X) {
            phyxels[sizeX*Y+X].wasUpdated = 1;
            #if PHX_ENABLE_RIGIDBODIES
            cleanupTmpBodies(X, Y); // only in this loop direction
            #endif
            phyxels[sizeX*Y+X].material->customUpdate(&scene, phyxelOffsetX+X, phyxelOffsetY+Y, false); // custom update step
            #if PHX_ENABLE_TEMPERATURE
            #if PHX_ENABLE_ENV_TEMP
            updateTemperature(X, Y);
            #endif
            #endif
        }
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0
    for (int Y = oldTemperatureUpdateRegion.lowerBoundary; Y >= oldTemperatureUpdateRegion.upperBoundary; --Y)
        for (int X = oldTemperatureUpdateRegion.rightBoundary; X >= oldTemperatureUpdateRegion.leftBoundary; --X)
            updateTemperature(X, Y);
    #endif
    #endif
    #if PHX_ENABLE_ELECTRICITY
    for (int Y = oldElectricityUpdateRegion.lowerBoundary; Y >= oldElectricityUpdateRegion.upperBoundary; --Y)
        for (int X = oldElectricityUpdateRegion.rightBoundary; X >= oldElectricityUpdateRegion.leftBoundary; --X)
            updateElectricity(X, Y);
    #endif
    #if PHX_ENABLE_CHEMISTRY
    for (int Y = oldChemistryUpdateRegion.lowerBoundary; Y >= oldChemistryUpdateRegion.upperBoundary; --Y)
        for (int X = oldChemistryUpdateRegion.rightBoundary; X >= oldChemistryUpdateRegion.leftBoundary; --X)
            updateChemistry(X, Y);
    #endif
    for (int Y = oldOtherUpdateRegion.lowerBoundary; Y >= oldOtherUpdateRegion.upperBoundary; --Y)
        for (int X = oldOtherUpdateRegion.rightBoundary; X >= oldOtherUpdateRegion.leftBoundary; --X) {
            #if PHX_ENABLE_BURNING
            updateBurning(X, Y);
            #endif
            updatePhyxelMovement(X, Y, false);
        }
}
void Scene::updateAll( // updates everything, can be called with 0 arguments like scene.updateAll(). combined (temperature + chemistry + burning + electricity + movement) & balanced bidirectional loop update for every phyxel in the scene & update of other entities (particles, rigids, other instances). in most cases, it's the only thing you need
    void* custom_arguments_particles= NULL,
    void* custom_arguments_instances= NULL
    #if PHX_ENABLE_RIGIDBODIES
    ,void* custom_arguments_rigids= NULL
    #endif
) {
    for (unsigned i = 0; i < numChunksX*numChunksY; ++i)
        chunks[i]->updateAllPhyxels<false>();
    for (unsigned i = 0; i < numChunksX*numChunksY; ++i)
        chunks[i]->updateAllPhyxels<true>();
    updateParticles(custom_arguments_particles);
    updateInstances(custom_arguments_instances);
    #if PHX_ENABLE_RIGIDBODIES
    updateRigidBodies(custom_arguments_rigids);
    #endif
}
#if PHX_ENABLE_PARALLEL_UPDATE
void Scene::updateAllParallel( // updates everything, can be called with 1 argument like scene.updateAllParallel(N). combined (temperature + chemistry + burning + electricity + movement) & balanced bidirectional loop update for every phyxel in the scene that is run in parallel for chunks & update of other entities (particles, rigids, other instances). in most cases, it's the only thing you need
    unsigned n_threads,
    void* custom_arguments_particles = NULL,
    void* custom_arguments_instances = NULL
    #if PHX_ENABLE_RIGIDBODIES
    ,void* custom_arguments_rigids = NULL
    #endif
    ,bool use_threads = true // (debug)
) {
    Chunk** chunks = this->chunks;
    auto numChunksX = this->numChunksX;
    std::function<void(unsigned, unsigned)> functors[] = {
        [chunks, numChunksX](unsigned i, unsigned j) {chunks[i*numChunksX+j]->updateAllPhyxels<false>();},
        [chunks, numChunksX](unsigned i, unsigned j) {chunks[i*numChunksX+j]->updateAllPhyxels<true>();}
    };

    if (use_threads) {
        // multithreaded execution
        for (uint8_t b = 0; b < 2; ++b)
            for (uint8_t k = 0; k < 4; ++k) { // for 4 different sets of chunks that don't neighbour each other
                std::vector<std::thread> my_threads(n_threads);
                unsigned t = 0; // thread index
                for (unsigned i = bool(k&1); i < numChunksY; i += 2)
                    for (unsigned j = bool(k&2); j < numChunksX; j += 2) {
                        if (t/n_threads) { // max number of threads reached. wait for them to finish
                            std::for_each(my_threads.begin(), my_threads.end(), std::mem_fn(&std::thread::join));
                            t = 0; // start over
                        }
                        my_threads[t] = std::thread(functors[b], i, j);
                        ++t;
                    }
                std::for_each(my_threads.begin(), my_threads.begin()+t, std::mem_fn(&std::thread::join));
            }
    } else {
        // single-threaded execution (debug)
        for (uint8_t b = 0; b < 2; ++b)
            for (uint8_t k = 0; k < 4; ++k) { // for 4 different sets of chunks that don't neighbour each other
                unsigned t = 0; // thread index
                for (unsigned i = bool(k&1); i < numChunksY; i += 2)
                    for (unsigned j = bool(k&2); j < numChunksX; j += 2) {
                        if (t/n_threads) {
                            t = 0; // start over
                        }
                        functors[b](i, j);
                        ++t;
                    }
            }
    }

    updateParticles(custom_arguments_particles);
    updateInstances(custom_arguments_instances);
    #if PHX_ENABLE_RIGIDBODIES
    updateRigidBodies(custom_arguments_rigids);
    #endif
}
#endif

#if PHX_ENABLE_WORLD_SAVE
WorldManager::WorldManager(const std::filesystem::path& path, Scene* scene1, 
                 unsigned sceneUpperLeftChunkX, unsigned sceneUpperLeftChunkY): 
    scene(scene1), 
    sceneUpperLeftChunkX(sceneUpperLeftChunkX), sceneUpperLeftChunkY(sceneUpperLeftChunkY) {
    scene1->world = this;
    folderPath = path;
    std::filesystem::create_directory(folderPath);
    loadSize();
    if (!checkConfig())
        throw std::string("config mismatch in linking WorldManager to " + folderPath.string());
    if (PHX_NUM_CHUNKS_X > sizeInChunksX || PHX_NUM_CHUNKS_Y > sizeInChunksY)
        throw std::string("attached scene is bigger than the world! resize and fill the world first.");
}

WorldManager::WorldManager(const std::filesystem::path& path, Scene* scene1, 
                unsigned sceneUpperLeftChunkX, unsigned sceneUpperLeftChunkY,
                unsigned sizeInChunksX, unsigned sizeInChunksY): 
    scene(scene1), 
    sceneUpperLeftChunkX(sceneUpperLeftChunkX), sceneUpperLeftChunkY(sceneUpperLeftChunkY) {
    scene1->world = this;
    folderPath = path;
    std::filesystem::create_directory(folderPath);
    if (!checkConfig())
        throw std::string("config mismatch in linking WorldManager to " + folderPath.string());
    resize(sizeInChunksX, sizeInChunksY);
    if (PHX_NUM_CHUNKS_X > sizeInChunksX || PHX_NUM_CHUNKS_Y > sizeInChunksY)
        throw std::string("attached scene is bigger than the world! resize and fill the world first.");
}

template <typename ParticlesCustomFieldsSpec, typename RigidBodiesCustomFieldsSpec>
void WorldManager::loadEntity(const std::string& path) {
    std::ifstream file(path);
    json obj;
    file >> obj;
    file.close();

    // we calculate scene coordinates for the entity based on the global (world) coordinates saved to disk
    obj["X"] = (float)(obj["worldX"]) - sceneUpperLeftChunkX*PHX_CHUNK_SIZE_X;
    obj["Y"] = (float)(obj["worldY"]) - sceneUpperLeftChunkY*PHX_CHUNK_SIZE_Y;
    if (obj["entityType"] == "particle") {
        Particle* p = scene->addParticle(nullptr, Color(), 0, 0, 0, 0);
        p->setFromJSON(obj);
        p->setCustomFieldsFromJSON<ParticlesCustomFieldsSpec>(obj["customFields"]);
    }
    #if PHX_ENABLE_RIGIDBODIES
    else if (obj["entityType"] == "rigid_body") {
        RigidBody* rb = scene->addRigidBody(obj["X"], obj["Y"], obj["sizeX"], obj["sizeY"], MaterialsList::get(obj["materialID"]));
        rb->setFromJSON(obj);
        rb->setCustomFieldsFromJSON<RigidBodiesCustomFieldsSpec>(obj["customFields"]);
    }
    #endif

    std::filesystem::remove(path);
}

void WorldManager::saveScene() { // TODO: save & load custom fields from scene
    // save scene-specific values
    json someSavedSceneValues;

    someSavedSceneValues["particlesTimeStep"] = scene->particlesTimeStep;
    #if PHX_ENABLE_TEMPERATURE
    someSavedSceneValues["envTemperature"] = scene->envTemperature;
    someSavedSceneValues["envThermalDiff"] = scene->envThermalDiff;
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    if (scene->defaultMaterial)
        someSavedSceneValues["defaultMaterialID"] = scene->defaultMaterial->getID();
    #endif

    std::string filename = folderPath.string() + "/scene.json";
    std::ofstream file(filename, std::ios::out);
    file << someSavedSceneValues;
    file.close();

    // save data from chunks and entities
    for (unsigned x = 0; x < PHX_NUM_CHUNKS_X; ++x)
        for (unsigned y = 0; y < PHX_NUM_CHUNKS_Y; ++y)
            saveChunk(x, y);
    for (auto* p : scene->particles) {
        auto pJSON = p->getAsJSON();
        saveEntity(pJSON);
    }
    #if PHX_ENABLE_RIGIDBODIES
    for (auto* rb : scene->rigidBodies) {
        auto rbJSON = rb->getAsJSON();
        saveEntity(rbJSON);
    }
    #endif
}

void WorldManager::clearSceneEntities() {
    for (auto* p : scene->particles)
        p->toDelete = true;
    #if PHX_ENABLE_RIGIDBODIES
    for (auto* rb : scene->rigidBodies)
        rb->toDelete = true;
    #endif
}

template <typename ParticlesCustomFieldsSpec, typename RigidBodiesCustomFieldsSpec, typename ChunkCustomFieldsSpec>
void WorldManager::loadScene() {
    json someSavedSceneValues;
    std::string filename = folderPath.string() + "/scene.json";
    std::ifstream file(filename, std::ios::in);
    file >> someSavedSceneValues;
    file.close();

    scene->particlesTimeStep = someSavedSceneValues["particlesTimeStep"];
    #if PHX_ENABLE_TEMPERATURE
    scene->envTemperature = someSavedSceneValues["envTemperature"];
    scene->envThermalDiff = someSavedSceneValues["envThermalDiff"];
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    if (!someSavedSceneValues["defaultMaterialID"].is_null())
        scene->defaultMaterial = MaterialsList::get(someSavedSceneValues["defaultMaterialID"]);
    #endif

    // first, remove the existing entities in the scene
    clearSceneEntities();

    // load main data
    for (unsigned x = 0; x < PHX_NUM_CHUNKS_X; ++x)
        for (unsigned y = 0; y < PHX_NUM_CHUNKS_Y; ++y)
            loadChunk<ChunkCustomFieldsSpec>(x, y);
    // also load the entities saved in the chunk folder. these need to be separate loops, as we want to load all the phyxels first
    for (unsigned x = 0; x < PHX_NUM_CHUNKS_X; ++x)
        for (unsigned y = 0; y < PHX_NUM_CHUNKS_Y; ++y) {
            std::stringstream ss;
            ss << folderPath.string() << "/chunk_" << sceneUpperLeftChunkX+x << "_" << sceneUpperLeftChunkY+y;
            std::filesystem::path chunkPath = ss.str();

            for (const auto& entry : std::filesystem::directory_iterator(chunkPath))
                if (std::filesystem::is_regular_file(entry.status())) {
                    std::string filename = entry.path().filename().string();
                    if (filename.find("entity_") == 0 && filename.size() > 5 && filename.substr(filename.size() - 5) == ".json") {
                        loadEntity<ParticlesCustomFieldsSpec, RigidBodiesCustomFieldsSpec>(entry.path().string());
                    }
                }
        }
}

void WorldManager::fillWorldChunkIfEmpty(unsigned X, unsigned Y, MaterialObj m, float* tauPtr) {
    // check directory
    std::stringstream ss;
    ss << folderPath.string() << "/chunk_" << X << "_" << Y;
    std::filesystem::path chunkFolderPath = ss.str();
    std::filesystem::create_directory(chunkFolderPath);
    std::filesystem::path phyxelsPath = ss.str() + "/phyxels.dat";
    if (std::filesystem::exists(phyxelsPath)) return;

    // helper variables
    uint_fast32_t ui = 0;
    float f = 0;
    bool b = 0;
    uint8_t ui8 = 0;
    uint_fast32_t mID = (m ? m->getID() : 0);
    Color c;

    // save chunk
    std::ofstream bf(phyxelsPath, std::ios::out | std::ios::binary);
    for (unsigned i = 0; i < PHX_CHUNK_SIZE_X*PHX_CHUNK_SIZE_Y; ++i) {
        #if PHX_ENABLE_ELECTRICITY
            bf.write((char*)&b, sizeof(b));
        #endif
        bf.write((char*)&mID, sizeof(mID));
        bf.write((char*)&c, sizeof(phx::Color)); // fixed type size, so it's cross-platform safe
        #if PHX_ENABLE_TEMPERATURE
            bf.write((char*)tauPtr, sizeof(float));
        #endif
        #if PHX_ENABLE_ELECTRICITY
            bf.write((char*)&(f), sizeof(f));
            bf.write((char*)&(f), sizeof(f));
            bf.write((char*)&ui, sizeof(ui));
        #endif
        #if PHX_ENABLE_CUSTOMGRAVITY
            bf.write((char*)&ui, sizeof(ui));
        #endif
        #if PHX_ENABLE_BURNING
            bf.write((char*)&(ui8), sizeof(ui8)); // fixed type size, so it's cross-platform safe
            bf.write((char*)&ui, sizeof(ui));
        #endif
    }
    bf.close();
}

void WorldManager::saveChunk(unsigned posInChunksX, unsigned posInChunksY) { // local (scene-level) coordinates
    // create directory
    std::stringstream ss;
    ss << folderPath.string() << "/chunk_" << sceneUpperLeftChunkX+posInChunksX << "_" << sceneUpperLeftChunkY+posInChunksY;
    std::filesystem::path chunkFolderPath = ss.str();
    std::filesystem::create_directory(chunkFolderPath);
    // save chunk
    scene->chunks[PHX_NUM_CHUNKS_X*posInChunksY + posInChunksX]->save(chunkFolderPath.string());
}

void Chunk::save(const std::string& dirName) {
    std::string phyxels_filename = dirName + "/phyxels.dat";
    std::ofstream bf(phyxels_filename, std::ios::out | std::ios::binary);

    for (unsigned i = 0; i < PHX_CHUNK_SIZE_X*PHX_CHUNK_SIZE_Y; ++i) {
        // helper variables
        uint_fast32_t ui;
        float f;
        bool b;

        // save phyxel data field by field
        #if PHX_ENABLE_ELECTRICITY
            b = phyxels[i].isTurning;
            bf.write((char*)&b, sizeof(b));
        #endif
        #if PHX_ENABLE_RIGIDBODIES
            // this should be loaded with the rigid bodies, there's no need to save this separately
            // #if PHX_ENABLE_ELECTRICITY
                // b = phyxels[i].isChargeFromBody;
                // bf.write((char*)&b, sizeof(b));
            // #endif
            // b = phyxels[i].isRigidFree;
            // bf.write((char*)&b, sizeof(b));
        #endif
        ui = phyxels[i].material->getID();
        bf.write((char*)&ui, sizeof(ui));
        bf.write((char*)&(phyxels[i].color), sizeof(phx::Color)); // fixed type size, so it's cross-platform safe
        #if PHX_ENABLE_TEMPERATURE
            f = phyxels[i].temperature;
            bf.write((char*)&(f), sizeof(f));
        #endif
        #if PHX_ENABLE_ELECTRICITY
            f = phyxels[i].charge;
            bf.write((char*)&(f), sizeof(f));
            f = phyxels[i].chargeLifetime;
            bf.write((char*)&(f), sizeof(f));
            ui = phyxels[i].chargeDir;
            bf.write((char*)&ui, sizeof(ui));
        #endif
        #if PHX_ENABLE_CUSTOMGRAVITY
            ui = phyxels[i].forceDir;
            bf.write((char*)&ui, sizeof(ui));
        #endif
        #if PHX_ENABLE_BURNING
            bf.write((char*)&(phyxels[i].burningStage), sizeof(phyxels[i].burningStage)); // fixed type size, so it's cross-platform safe
            ui = phyxels[i].burningTimer;
            bf.write((char*)&ui, sizeof(ui));
        #endif
    }
    bf.close();

    if (phyxels[0].customFields) { // either the custom fields are defined for every phyxel or for none.
        std::string custom_filename = dirName + "/phyxels_customdata.dat";
        std::ofstream bf_c(custom_filename, std::ios::out | std::ios::binary);
        for (unsigned i = 0; i < PHX_CHUNK_SIZE_X*PHX_CHUNK_SIZE_Y; ++i) {
            if (phyxels[i].customFields)
                phyxels[i].customFields->appendToBitfile(bf_c);
            else {
                std::stringstream ss_err;
                ss_err << "unable to write to \"" << custom_filename << "\": phyxel number " << i << " had customFields==NULL; remember: either custom fields are defined for every phyxel or for none";
                throw std::string(ss_err.str());
            }
        }
        bf_c.close();
    }

    if (customFields) { // save chunk's custom fields
        json chunk_custom_fields = customFields->getAsJSON();
        std::ofstream file(dirName + "/chunk_customdata.json");
        file << chunk_custom_fields.dump();
        file.close();
    }
}

template<typename ChunkCustomFieldsSpec>
void WorldManager::loadChunk(unsigned posInChunksX, unsigned posInChunksY) { // local (scene-level) coordinates
    // check if the directory exists
    std::stringstream ss;
    ss << folderPath.string() << "/chunk_" << sceneUpperLeftChunkX+posInChunksX << "_" << sceneUpperLeftChunkY+posInChunksY;
    std::filesystem::path chunkFolderPath = ss.str();
    if (!std::filesystem::exists(chunkFolderPath)) {
        std::stringstream ss_err;
        ss_err << "unable to read from \"" << chunkFolderPath << "\": can't find";
        throw std::string(ss_err.str());
    }
    // save chunk
    scene->chunks[PHX_NUM_CHUNKS_X*posInChunksY + posInChunksX]->load<ChunkCustomFieldsSpec>(chunkFolderPath.string());
}

template<typename MyCustomFieldsSpec>
void Chunk::loadCustomFields(const std::string& dirName) {
    std::string custom_filename = dirName + "/chunk_customdata.json";
    if (std::filesystem::exists(custom_filename)) { // chunk's custom fields
        json chunk_custom_fields;
        std::ifstream file(custom_filename);
        file >> chunk_custom_fields;
        file.close();
        if (!chunk_custom_fields.empty()) {
            if (!(std::is_void<MyCustomFieldsSpec>::value || customFields))
                customFields = new MyCustomFieldsSpec();
            customFields->setFromJSON(chunk_custom_fields);
        }
    }
}

template<>
void Chunk::loadCustomFields<void>(const std::string& dirName) {
    std::string custom_filename = dirName + "/chunk_customdata.json";
    if (std::filesystem::exists(custom_filename)) { // chunk's custom fields
        json chunk_custom_fields;
        std::ifstream file(custom_filename);
        file >> chunk_custom_fields;
        file.close();
        if (!chunk_custom_fields.empty() && customFields) {
            customFields->setFromJSON(chunk_custom_fields);
        }
    }
}

template<typename MyCustomFieldsSpec>
void Chunk::load(const std::string& dirName) {
    std::string phyxels_filename = dirName + "/phyxels.dat";
    std::ifstream bf(phyxels_filename, std::ios::in | std::ios::binary);

    for (unsigned i = 0; i < PHX_CHUNK_SIZE_X*PHX_CHUNK_SIZE_Y; ++i) {
        // helper variables
        uint_fast32_t ui;
        float f;
        bool b;

        // load phyxel data field by field
        #if PHX_ENABLE_ELECTRICITY
            bf.read((char*)&b, sizeof(b));
            phyxels[i].isTurning = b;
        #endif

        #if PHX_ENABLE_RIGIDBODIES
            // this should be loaded with the rigid bodies, there's no need to save & load this separately
            // #if PHX_ENABLE_ELECTRICITY
                // b = phyxels[i].isChargeFromBody;
                // bf.write((char*)&b, sizeof(b));
            // #endif
            // b = phyxels[i].isRigidFree;
            // bf.write((char*)&b, sizeof(b));
        #endif
        bf.read((char*)&ui, sizeof(ui));
        phyxels[i].setMaterial(MaterialsList::get(ui), 0);
        bf.read((char*)&(phyxels[i].color), sizeof(phx::Color)); // fixed type size, so it's cross-platform safe
        #if PHX_ENABLE_TEMPERATURE
            bf.read((char*)&(f), sizeof(f));
            phyxels[i].setTemperature(f);
        #endif
        #if PHX_ENABLE_ELECTRICITY
            bf.read((char*)&(f), sizeof(f));
            phyxels[i].setCharge(f);
            bf.read((char*)&(f), sizeof(f));
            phyxels[i].chargeLifetime = f;
            bf.read((char*)&ui, sizeof(ui));
            phyxels[i].chargeDir = ui;
        #endif
        #if PHX_ENABLE_CUSTOMGRAVITY
            bf.read((char*)&ui, sizeof(ui));
            phyxels[i].setForce(ui);
        #endif
        #if PHX_ENABLE_BURNING
            bf.read((char*)&(phyxels[i].burningStage), sizeof(phyxels[i].burningStage)); // fixed type size, so it's cross-platform safe
            bf.read((char*)&ui, sizeof(ui));
            phyxels[i].burningTimer = ui;
        #endif
    }
    bf.close();

    std::string custom_filename = dirName + "/phyxels_customdata.dat";
    if (std::filesystem::exists(custom_filename)) { // phyxels' custom fields
        std::ifstream bf_c(custom_filename, std::ios::in | std::ios::binary);
        for (unsigned i = 0; i < PHX_CHUNK_SIZE_X*PHX_CHUNK_SIZE_Y; ++i) {
            phyxels[i].customFields->readSectionFromBitfile(bf_c);
        }
        bf_c.close();
    }

    loadCustomFields<MyCustomFieldsSpec>(dirName); // chunk's custom fields
}
#endif

}
