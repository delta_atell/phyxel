// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this declares (and partially defines the Chunk)

#pragma once
#include <cstdlib>
#include <vector>
#include <unordered_set>

#include "../phyxel_config.h"
#if PHX_ENABLE_RIGIDBODIES // requires Box2D to be installed and linked (designed for v2.4.1)
    #include <box2d/box2d.h>
#endif
#include "Color.hpp"
#include "Material.hpp"
#if PHX_ENABLE_CHEMISTRY
    #include "Chemistry.hpp"
#endif
#include "PhyxelData.hpp"
#include "../Entities/Particle.hpp"
#include "CustomFields.hpp"

namespace phx {

/// a Chunk is everything we update each frame
class Chunk {
public:
    template <typename T> struct UpdateRegion { // defines update boundaries inside a chunk
        T leftBoundary;
        T rightBoundary;
        T upperBoundary;
        T lowerBoundary;

        UpdateRegion() {
            reset();
        }
        template <typename U> UpdateRegion(const UpdateRegion<U>& r) {
            leftBoundary = r.leftBoundary;
            rightBoundary = r.rightBoundary;
            upperBoundary = r.upperBoundary;
            lowerBoundary = r.lowerBoundary;
        }
        void reset() {
            leftBoundary = PHX_CHUNK_SIZE_X;
            rightBoundary = 0;
            upperBoundary = PHX_CHUNK_SIZE_Y;
            lowerBoundary = 0;
        }
        // template <typename U> UpdateRegion<T>& operator=(const UpdateRegion<U>& r) {
        //     leftBoundary = r.leftBoundary;
        //     rightBoundary = r.rightBoundary;
        //     upperBoundary = r.upperBoundary;
        //     lowerBoundary = r.lowerBoundary;
        // }
    };
private:
    // these should be private and forbidden to use
    Chunk(const Chunk&) = delete;
    Chunk(Chunk&&) = delete;
    Chunk& operator=(const Chunk&) = delete;
    Chunk& operator=(Chunk&&) = delete;
    Scene& scene;
    unsigned phyxelOffsetX; // phyxelOffsetX + x_coord_in_chunk = x_coord_in_scene
    unsigned phyxelOffsetY; // phyxelOffsetY + y_coord_in_chunk = y_coord_in_scene 
    PhyxelData* phyxels;
    unsigned sizeX;
    unsigned sizeY;

    // pointers to the neighbouring chunks
    // 0 - down, 1 - right, 2 - up, 3 - left
    Chunk* neighbours[4] = {NULL};

    // smart updates (only update what's needed)
    #if PHX_ENABLE_ELECTRICITY
    // X and Y boundaries of the rectangle in which electricity is updated
    UpdateRegion<unsigned> electricityUpdateRegion;
    #endif
    #if PHX_ENABLE_CHEMISTRY
    // X and Y boundaries of the rectangle in which chemistry is updated
    UpdateRegion<unsigned> chemistryUpdateRegion;
    #endif
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0 // (smart updates are only turned on for the temperature system if there's no environment temperature update)
    // X and Y boundaries of the rectangle in which temperature is updated
    UpdateRegion<unsigned> temperatureUpdateRegion;
    #endif
    #endif
    // X and Y boundaries of the rectangle in which phyxel movement and burning are updated
    UpdateRegion<unsigned> otherUpdateRegion;

    #if PHX_ENABLE_RIGIDBODIES
    void cleanupTmpBodies(unsigned X, unsigned Y); // remove temporary technical bodies that are not used anymore, defined in "Core/Utils.hpp"
    #endif

    friend class WorldManager;
    friend class Scene;
    friend class Entity;
    friend class PhyxelData;

    void setPhyxelNeighbours(); // defined in "Core/Utils.hpp"
    #if PHX_ENABLE_WORLD_SAVE
    void save(const std::string& dirName); // defined in "Core/Utils.hpp"
    template<typename MyCustomFieldsSpec = void>
    void loadCustomFields(const std::string& dirName); // defined in "Core/Utils.hpp"
    template<typename MyCustomFieldsSpec = void>
    void load(const std::string& dirName); // defined in "Core/Utils.hpp"
    #endif
public:
    CustomFields* customFields = NULL;

    #if PHX_ENABLE_TEMPERATURE
    float envTemperature=PHX_DEFAULT_TEMP;
    float envThermalDiff=0.00001;
    #endif

    // constructor
    Chunk(Scene& scene1, unsigned phyxelOffsetX, unsigned phyxelOffsetY): 
        scene(scene1), phyxelOffsetX(phyxelOffsetX), phyxelOffsetY(phyxelOffsetY) {
        this->sizeX = PHX_CHUNK_SIZE_X;
        this->sizeY = PHX_CHUNK_SIZE_Y;

        unsigned tmp_size = sizeY*sizeX;
        phyxels = new PhyxelData[tmp_size];
        for (unsigned i = 0; i < tmp_size; ++i) {
            phyxels[i].myChunk = this;
        }
    }

    // destructor
    ~Chunk() {
        delete customFields;
        delete [] phyxels;
    }

    // getters for the most common properties. use Chunk(X,Y).property or Chunk[i].property if there's no getter provided
    const MaterialObj & getMaterial(unsigned i) const {
        return phyxels[i].material;
    }
    const MaterialObj & getMaterial(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].material;
    }
    const Color& getColor(unsigned i) const {
        return phyxels[i].color;
    }
    const Color& getColor(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].color;
    }
    #if PHX_ENABLE_TEMPERATURE
    float getTemperature(unsigned i) const {
        return phyxels[i].temperature;
    }
    float getTemperature(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].temperature;
    }
    #endif
    #if PHX_ENABLE_ELECTRICITY
    float getCharge(unsigned i) const {
        return phyxels[i].charge;
    }
    float getCharge(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].charge;
    }
    #endif
    #if PHX_ENABLE_RIGIDBODIES
    bool getIsRigidFree(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].isRigidFree;
    }
    bool getIsRigidFree(unsigned i) const {
        return phyxels[i].isRigidFree;
    }
    Color getRigidColor(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].coveringRigid->color;
    }
    Color getRigidColor(unsigned i) const {
        return phyxels[i].coveringRigid->color;
    }
    RigidBody* getCoveringRigid(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].coveringRigid;
    }
    RigidBody* getCoveringRigid(unsigned i) const {
        return phyxels[i].coveringRigid;
    }
    #endif
    const PhyxelData & operator()(unsigned X, unsigned Y) const { // double indexing
        return phyxels[sizeX*Y+X];
    }
    const PhyxelData & operator[](unsigned idx) const { // single indexing
        return phyxels[idx];
    }
    unsigned getSizeX() const {
        return sizeX;
    }
    unsigned getSizeY() const {
        return sizeY;
    }
    Chunk* getNbrDown() const {
        return NBR_DOWN;
    }
    Chunk* getNbrRight() const {
        return NBR_RIGHT;
    }
    Chunk* getNbrUp() const {
        return NBR_UP;
    }
    Chunk* getNbrLeft() const {
        return NBR_LEFT;
    }
    #if PHX_ENABLE_CUSTOMGRAVITY
    uint8_t getForce(unsigned i) const {
        return phyxels[i].forceDir;
    }
    uint8_t getForce(unsigned X, unsigned Y) const {
        return phyxels[sizeX*Y+X].forceDir;
    }
    #endif

    // setters for the most common properties. use Chunk(X,Y).property or Chunk[i].property if there's no setter provided
    void setMaterial(unsigned X, unsigned Y, MaterialObj m, int rnd=0) {
        setMaterial(Y*sizeX+X, m, rnd);
    }
    void setMaterial(unsigned index, MaterialObj m, int rnd=0) {
        phyxels[index].setMaterial(m, rnd);
    }
    void setColor(unsigned X, unsigned Y, Color val) {
        setColor(sizeX*Y+X, val);
    }
    void setColor(unsigned i, Color val) {
        phyxels[i].setColor(val);
    }
    PhyxelData& operator()(unsigned X, unsigned Y) { // double indexing
        return phyxels[sizeX*Y+X];
    }
    PhyxelData& operator[](unsigned idx) { // single indexing
        return phyxels[idx];
    }
    #if PHX_ENABLE_TEMPERATURE
    void setTemperature(unsigned X, unsigned Y, float tau) {
        setTemperature(sizeX*Y+X, tau);
    }
    void setTemperature(unsigned i, float tau) {
        phyxels[i].setTemperature(tau);
    }

    void addTemperature(unsigned X, unsigned Y, float delta_tau) {
        addTemperature(sizeX*Y+X, delta_tau);
    }
    void addTemperature(unsigned i, float delta_tau) {
        phyxels[i].addTemperature(delta_tau);
    }

    #endif
    #if PHX_ENABLE_ELECTRICITY
    void setCharge(unsigned X, unsigned Y, float dQ) {
        setCharge(sizeX*Y+X, dQ);
    }
    void setCharge(unsigned i, float dQ) {
        phyxels[i].setCharge(dQ);
    }
    void addCharge(unsigned X, unsigned Y, float dQ) { // NOTE: remove these maybe?? logic fails
        addCharge(sizeX*Y+X, dQ);
    }
    void addCharge(unsigned i, float dQ) {
        phyxels[i].addCharge(dQ);
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void setForce(unsigned X, unsigned Y, uint8_t forceDir) {
        setForce(sizeX*Y+X, forceDir);
    }
    void setForce(unsigned i, uint8_t forceDir) {
        phyxels[i].setForce(forceDir);
    }
    #endif

    // swaps
    void swap(PhyxelData& phyxel1, PhyxelData& phyxel2, bool loopDir) { // swap phyxels (only the necessary parts) /// THIS FUNCTION ISN'T SYMMETRIC
        phyxel1.lockMask = 1;
        if (phyxel2.lockMask & 1) {
            phyxel1.flagAreaActive();
            phyxel2.flagAreaActive();
            return;
        }
        swapNoLock(phyxel1, phyxel2);

        if (phyxel1.wasUpdated!=phyxel2.wasUpdated)
            phyxel2.lockMask = 3;
        else
            phyxel2.lockMask = 1;
    }
    void swapNoLock(PhyxelData& phyxel1, PhyxelData& phyxel2);

    // smart update flags
    void flagActive(unsigned _X, unsigned _Y) { // expects global (scene) coordinates, also flags chemistry
        unsigned X = _X - phyxelOffsetX;
        unsigned Y = _Y - phyxelOffsetY;
        otherUpdateRegion.leftBoundary = std::min(otherUpdateRegion.leftBoundary, X);
        otherUpdateRegion.rightBoundary = std::max(otherUpdateRegion.rightBoundary, X);
        otherUpdateRegion.upperBoundary = std::min(otherUpdateRegion.upperBoundary, Y);
        otherUpdateRegion.lowerBoundary = std::max(otherUpdateRegion.lowerBoundary, Y);
        #if PHX_ENABLE_CHEMISTRY
        flagAreaActiveChemistry(_X, _Y);
        #endif
    }
    #if PHX_ENABLE_TEMPERATURE
    #if PHX_ENABLE_ENV_TEMP==0 // (smart updates are only turned on for the temperature system if there's no environment temperature update)
    void flagActiveTemperatureLocal(unsigned X, unsigned Y) { // expects local (chunk) coordinates
        temperatureUpdateRegion.leftBoundary = std::min(temperatureUpdateRegion.leftBoundary, X);
        temperatureUpdateRegion.rightBoundary = std::max(temperatureUpdateRegion.rightBoundary, X);
        temperatureUpdateRegion.upperBoundary = std::min(temperatureUpdateRegion.upperBoundary, Y);
        temperatureUpdateRegion.lowerBoundary = std::max(temperatureUpdateRegion.lowerBoundary, Y);
    }
    #endif
    #endif
    #if PHX_ENABLE_ELECTRICITY
    void flagActiveElectricity(unsigned X, unsigned Y) { // expects global (scene) coordinates
        X -= phyxelOffsetX;
        Y -= phyxelOffsetY;
        flagActiveElectricityLocal(X, Y);
    }
    void flagActiveElectricityLocal(unsigned X, unsigned Y) { // expects local (chunk) coordinates
        electricityUpdateRegion.leftBoundary = std::min(electricityUpdateRegion.leftBoundary, X);
        electricityUpdateRegion.rightBoundary = std::max(electricityUpdateRegion.rightBoundary, X);
        electricityUpdateRegion.upperBoundary = std::min(electricityUpdateRegion.upperBoundary, Y);
        electricityUpdateRegion.lowerBoundary = std::max(electricityUpdateRegion.lowerBoundary, Y);
    }
    #endif
    #if PHX_ENABLE_CHEMISTRY
    void flagActiveChemistry(unsigned X, unsigned Y) { // expects global (scene) coordinates
        X -= phyxelOffsetX;
        Y -= phyxelOffsetY;
        flagActiveChemistryLocal(X, Y);
    }
    void flagActiveChemistryLocal(unsigned X, unsigned Y) { // expects local (chunk) coordinates
        chemistryUpdateRegion.leftBoundary = std::min(chemistryUpdateRegion.leftBoundary, X);
        chemistryUpdateRegion.rightBoundary = std::max(chemistryUpdateRegion.rightBoundary, X);
        chemistryUpdateRegion.upperBoundary = std::min(chemistryUpdateRegion.upperBoundary, Y);
        chemistryUpdateRegion.lowerBoundary = std::max(chemistryUpdateRegion.lowerBoundary, Y);
    }
    void flagAreaActiveChemistry(unsigned X, unsigned Y); // defined in "Core/Utils.hpp"
    #endif

    // fill in the Chunk (call it after init)
    void fill(MaterialObj defaultMaterial) {
        for(unsigned i=0; i<sizeX*sizeY; ++i)
            setMaterial(i, defaultMaterial, 0);
    }
    #if PHX_ENABLE_TEMPERATURE
    void fillTemperature(float tau) {
        for(unsigned i=0; i<sizeX*sizeY; ++i)
            phyxels[i].temperature = tau;
    }
    #endif
    #if PHX_ENABLE_CUSTOMGRAVITY
    void fillForceField(uint8_t forceDir) {
        for(unsigned i=0; i<sizeX*sizeY; ++i)
            phyxels[i].forceDir = forceDir;
    }
    #endif

    // updates (defined in "Utils.hpp")
    template<bool loop_dir>
    void updateAllPhyxels(); // combined (temperature + chemistry + burning + electricity + movement) update for every phyxel in the Chunk
    #if PHX_ENABLE_TEMPERATURE
    void updateTemperature(unsigned X, unsigned Y); // only updates temperatures
    #endif
    #if PHX_ENABLE_ELECTRICITY
    void updateElectricity(unsigned X, unsigned Y); // only updates electricity
    #endif
    #if PHX_ENABLE_CHEMISTRY
    void updateChemistry(unsigned X, unsigned Y); // only updates chemistry
    #endif
    #if PHX_ENABLE_BURNING
    void updateBurning(unsigned X, unsigned Y); // only updates fire
    #endif
    void updatePhyxelMovement(unsigned X, unsigned Y, bool loopDir=true); // update phyxels' movement
};
}
