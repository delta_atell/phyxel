// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

#pragma once
#include "../phyxel_config.h"
#include "../Core/Material.hpp"
#include <functional>

namespace phx {

class Scene; // defined in "Core/Scene.hpp"

/// this struct is what our particles are. there are several pre-defined types of behaviour for particles. use custom updates adn destructors to expand it. the particle may escape the scene which will cause a segfault, make sure to delete such particles before they escape the scene or handle them properly
struct Particle: public Entity {
    // main fields
    MaterialObj material; // the material of the Particle, can be NULL if the particle is not "real"
    Color color;
    bool isAffectedByGravity;
    bool checkCollisions = true;
    bool isVirtual = false; // virtual particles have another, non-physical behaviour. in general, you should not create virtual particles, they are kept for technical use. virtual particles should always have a definite material that is NOT A SOLID. virtual particles are essentially particles that are trying to find a place to occupy.
    std::function<bool(Particle*, void*)> customUpdate = [](Particle*, void*){return false;}; // a functor to handle custom update routine. it's called prior to all the in-built particle physics. return true here to stop the automatic update of the particle.
    std::function<void(Particle*)> customDestructor = [](Particle*){return;}; // a functor to handle custom destructor routine

    // coordinates (in phyxels)
    float X;
    float Y;

    // velocity
    float Vx;
    float Vy;

    // burning
    #if PHX_ENABLE_BURNING
    bool isBurning;
    #endif

    Particle() = delete;
    Particle(MaterialObj material, Color color, float X, float Y,
        float Vx, float Vy,
        Scene* scene,
        bool isAffectedByGravity = true,
        bool checkCollisions = true
        #if PHX_ENABLE_BURNING
        ,bool isBurning = false
        #endif
    ): Entity(scene) {
        this->scene = scene;
        this->material = material;
        this->color = color;
        this->X = X;
        this->Y = Y;
        this->Vx = Vx;
        this->Vy = Vy;
        this->checkCollisions = checkCollisions;
        this->isAffectedByGravity = isAffectedByGravity;
        #if PHX_ENABLE_BURNING
        this->isBurning = isBurning;
        #endif
    }

    #if PHX_ENABLE_WORLD_SAVE
    virtual json getAsJSON() { // WARNING: the functor data in `customUpdate` and `customDestructor` is not preserved in the JSON and thus is not saved to the disk in the `WorldManager`. you can represent some important parts of this data in your application with custom fields (defined in the `Entity` base class) which are put into the JSON.
        json self = Entity::getAsJSON();
        self["entityType"] = "particle";
        self["materialID"] = material->getID();
        self["color"] = json::array({color.r, color.g, color.b, color.a});
        self["X"] = X;
        self["Y"] = Y;
        self["Vx"] = Vx;
        self["Vy"] = Vy;

        self["checkCollisions"] = checkCollisions;
        self["isAffectedByGravity"] = isAffectedByGravity;
        #if PHX_ENABLE_BURNING
        self["isBurning"] = isBurning;
        #endif
        return self;
    }
    virtual void setFromJSON(const json &self) { // does NOT set the custom fields, since `customFields` is a pointer to a user-defined type. use `setCustomFieldsFromJSON` (defined in "Entities/EntityUtils.hpp") separately and specify your `CustomFields`-based type
        Entity::setFromJSON(self);

        material = MaterialsList::get(self["materialID"]);

        color.r = self["color"][0];
        color.g = self["color"][1];
        color.b = self["color"][2];
        color.a = self["color"][3];

        X = self["X"];
        Y = self["Y"];
        Vx = self["Vx"];
        Vy = self["Vy"];

        checkCollisions = self["checkCollisions"];
        isAffectedByGravity = self["isAffectedByGravity"];

        #if PHX_ENABLE_BURNING
        isBurning = self["isBurning"];
        #endif
    } 
    #endif 
};

}
