// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// declare Entity class

// TODO: use smart pointers for entities in the scene, so that we log and unlog them properly automatically and avoid most segfaults related to them.

#pragma once
#include "../phyxel_config.h"
#include "../Core/CustomFields.hpp"

namespace phx {
class Scene; // defined in "Core/Scene.hpp"

/// Phyxel entities are: Particles, Instances, RigidBodies
class Entity {
    Entity() = delete;
    Entity(const Entity& e); // defined in "Entities/EntityUtils.hpp"
    Entity(Entity&& e) noexcept; // defined in "Entities/EntityUtils.hpp"
    // Entity& operator=(const Entity& e); // defined in "Entities/EntityUtils.hpp"
    // Entity& operator=(Entity&& e) noexcept; // defined in "Entities/EntityUtils.hpp"

    static unsigned nextID;
    unsigned id;
protected:
    // scene pointer
    Scene* scene;
public:
    bool toDelete = false; // do NOT explicitly delete entities from the scene, just set this field to true instead deallocate the necessary customFields manually before that
    time_t lifetime = 0; // the number of update steps the entity lived through

    // custom data (not used in any way by Phyxel)
    CustomFields* customFields = NULL;

    Entity(Scene* scene); // defined in "Entities/EntityUtils.hpp"
    virtual ~Entity(); // defined in "Entities/EntityUtils.hpp"
    unsigned getID() const {
        return id;
    }
    Scene* getScene() const {
        return scene;
    }

    #if PHX_ENABLE_WORLD_SAVE
    virtual json getAsJSON(); // defined in "Entities/EntityUtils.hpp"
    virtual void setFromJSON(const json &self); // defined in "Entities/EntityUtils.hpp". does NOT set the custom fields, since `customFields` is a pointer to a user-defined type. use `setCustomFieldsFromJSON` separately and specify your `CustomFields`-based type
    template <typename MyCustomFieldsSpec = void>
    void setCustomFieldsFromJSON(const json &custom_fields); // defined in "Entities/EntityUtils.hpp". specify your `CustomFields`-based type in the template
    #endif
};

unsigned Entity::nextID = 0;

};