// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file contains definitions for some methods from the rigid body system that cannot be placed in the "Entities/Objects.hpp" file. this file is only included when PHX_ENABLE_RIGIDBODIES is set to 1.

#pragma once
#include "Objects.hpp"

namespace phx {

RigidBody::RigidBody(Scene* scene, b2World* box2DLayer, b2Body* body, float sizeX, float sizeY, MaterialObj material):
    AbstractInstance(scene), box2DBody(body), box2DLayer(box2DLayer), sizeX(sizeX), sizeY(sizeY), material(material) { // it's not recommended to create objects manually, add them to the scene using Scene::addRigidBody instead
    if (!scene->defaultMaterial) throw std::string("scene->defaultMaterial is NULL, should be a gas with isRemovable=true"); // safety check
    backgroundMaterialMass = scene->defaultMaterial->mass;
    color = material->colors[rand()%(material->colors.size())];
    recalculate();
}

RigidBody::~RigidBody() {
    // remove it from all the joints
    for (unsigned j = 0; j < joints.size(); ++j) { // FIXME: can we use another structure and not be quadratic here?
        auto joint = joints[j];
        auto& temp = ((joint->firstBody==this) ?
        joint->secondBody->joints : joint->firstBody->joints);
        auto it = std::find(temp.begin(), temp.end(), joint);
        if (it != temp.end())
            temp.erase(it);
        delete joint;
    }

    // delete the corresponding box2D body
    delete reinterpret_cast<FixtureData*>(box2DBody->GetUserData().pointer);
    box2DLayer->DestroyBody(box2DBody);
//    PhyxelData* phyxels = &((*scene)(0, 0));

    // clear myself from all the covered phyxels
    for (const auto& c : coveredPhyxels) {
        auto& phyx = (*scene)(c.first, c.second);
        phyx.isRigidFree = true;
        phyx.coveringRigid = NULL;
    }
}

void RigidBody::recalculate() { // shouldn't really be called by the user, it's regularly called automatically
    s = sin(box2DBody->GetAngle()) * 0.5f;
    c = cos(box2DBody->GetAngle()) * 0.5f;

    float aX = c * sizeX;
    float bX = - s * sizeY;
    float aY = s * sizeX;
    float bY = c * sizeY;

    auto pos = box2DBody->GetPosition();
    float centerX = pos.x * PHX_METERS2PHYXELS;
    float centerY = pos.y * PHX_METERS2PHYXELS;
    vX[0] = centerX + aX + bX;
    vX[1] = centerX - aX + bX;
    vX[2] = centerX - aX - bX;
    vX[3] = centerX + aX - bX;

    vY[0] = centerY + aY + bY;
    vY[1] = centerY - aY + bY;
    vY[2] = centerY - aY - bY;
    vY[3] = centerY + aY - bY;

    edgeX[0] = -2 * aX;
    edgeX[1] = -2 * bX;
    edgeX[2] = 2 * aX;
    edgeX[3] = 2 * bX;
    edgeY[0] = -2 * aY;
    edgeY[1] = -2 * bY;
    edgeY[2] = 2 * aY;
    edgeY[3] = 2 * bY;

    // recalculate covered phyxels
    coveredPhyxels.clear();
    framePhyxels.clear();

    uint8_t sortVerts[4];
    sortVerts[0] = 0;
    for (uint8_t i = 1; i < 4; ++i) {
        if (vX[sortVerts[0]] > vX[i])
            sortVerts[0] = i;
    }
    sortVerts[1] = (sortVerts[0] + 1) & 3;
    sortVerts[2] = (sortVerts[1] + 1) & 3;
    sortVerts[3] = (sortVerts[2] + 1) & 3;

    unsigned x_left = std::max(0.0f, floor(vX[sortVerts[0]]));
    unsigned x_right = std::max(0.0f, floor(vX[sortVerts[2]]));

    std::vector<unsigned> upperYBound;
    std::vector<unsigned> lowerYBound;

    unsigned x_bottom = std::max(0.0f, floor(vX[sortVerts[3]])); // max is for it not to become negative
    unsigned x_top = std::max(0.0f, floor(vX[sortVerts[1]])); // max is for it not to become negative

    float k1, k2, p;
    float vY1, vY3;

    if (edgeX[sortVerts[0]] < 1) {
        /// y = k * x + p
        /// k is the same for cases 1, 4 and 2, 3 respectively
        /// so they are cached since the computation involves division
        // left bottom quarter:
        k1 = edgeY[sortVerts[3]] / edgeX[sortVerts[3]];
        p = vY[sortVerts[0]] - k1 * vX[sortVerts[0]];
        for (unsigned x = x_left+1; x <= x_bottom; ++x) {
            upperYBound.push_back((k1*x+p));
        }
        // left upper quarter:
        for (unsigned x = x_left+1; x <= x_top; ++x) {
            lowerYBound.push_back(vY[sortVerts[1]]);
        }

        upperYBound.push_back(vY[sortVerts[3]]);
        lowerYBound.push_back(vY[sortVerts[1]]);

        // right bottom quarter:
        for (unsigned x = x_bottom+1; x <= x_right; ++x) {
            upperYBound.push_back(vY[sortVerts[3]]);
        }
        // right upper quarter:
        p = vY[sortVerts[1]] - k1 * vX[sortVerts[1]];
        for (unsigned x = x_top+1; x <= x_right; ++x) {
            lowerYBound.push_back((k1*x+p));
        }

    } else if (edgeX[sortVerts[1]] < 1) {
        vY1 = vY[sortVerts[1]];
        vY3 = vY[sortVerts[3]];
        /// y = k * x + p
        /// k is the same for cases 1, 4 and 2, 3 respectively
        /// so they are cached since the computation involves division
        // left bottom quarter:
        for (unsigned x = x_left+1; x <= x_bottom; ++x) {
            upperYBound.push_back(vY3);
        }
        // left upper quarter:
        k2 = edgeY[sortVerts[0]] / edgeX[sortVerts[0]];
        p = vY[sortVerts[0]] - k2 * vX[sortVerts[0]];
        for (unsigned x = x_left+1; x <= x_top; ++x) {
            lowerYBound.push_back((k2*x+p));
        }

        upperYBound.push_back(vY[sortVerts[3]]);
        lowerYBound.push_back(vY[sortVerts[1]]);

        // right bottom quarter:
        p = vY[sortVerts[3]] - k2 * vX[sortVerts[3]];
        for (unsigned x = x_bottom+1; x <= x_right; ++x) {
            upperYBound.push_back((k2*x+p));
        }
        // right upper quarter:
        for (unsigned x = x_top+1; x <= x_right; ++x) {
            lowerYBound.push_back(vY1);
        }
    } else {
        /// y = k * x + p
        /// k is the same for cases 1, 4 and 2, 3 respectively
        /// so they are cached since the computation involves division
        // left bottom quarter:
        k1 = edgeY[sortVerts[3]] / edgeX[sortVerts[3]];
        p = vY[sortVerts[0]] - k1 * vX[sortVerts[0]];
        for (unsigned x = x_left+1; x <= x_bottom; ++x) {
            upperYBound.push_back((k1*x+p));
        }
        // left upper quarter:
        k2 = edgeY[sortVerts[0]] / edgeX[sortVerts[0]];
        p = vY[sortVerts[0]] - k2 * vX[sortVerts[0]];
        for (unsigned x = x_left+1; x <= x_top; ++x) {
            lowerYBound.push_back((k2*x+p));
        }

        upperYBound.push_back(vY[sortVerts[3]]);
        lowerYBound.push_back(vY[sortVerts[1]]);

        // right bottom quarter:
        p = vY[sortVerts[3]] - k2 * vX[sortVerts[3]];
        for (unsigned x = x_bottom+1; x <= x_right; ++x) {
            upperYBound.push_back((k2*x+p));
        }
        // right upper quarter:
        p = vY[sortVerts[1]] - k1 * vX[sortVerts[1]];
        for (unsigned x = x_top+1; x <= x_right; ++x) {
            lowerYBound.push_back((k1*x+p));
        }
    }

    unsigned numOfFramePhyxels = 0;
    float sumOfMasses = 0;
    unsigned idx;

    unsigned x_left_1 = x_left-1;
    if (x_left_1 < PHX_SCENE_SIZE_X) {
        for (unsigned y = lowerYBound[0]; y <= std::min(upperYBound[0], (unsigned)(PHX_SCENE_SIZE_Y-1)); ++y) { 
            auto& curphx = (*scene)(x_left_1, y);
            framePhyxels.push_back(std::make_pair(x_left_1, y));
            if (curphx.getIsRigidFree() && curphx.material->type<=PHX_MTYPE_LIQ) {
                sumOfMasses += curphx.material->mass;
                ++numOfFramePhyxels;
            }
        }
    }

    for (unsigned x = x_left; x <= x_right; ++x) {
        if (x < PHX_SCENE_SIZE_X) {
            {
                unsigned lowerYBoundXDiff = lowerYBound[x-x_left]-1;
                if (lowerYBoundXDiff < PHX_SCENE_SIZE_Y) {
                    auto& curphx = (*scene)(x, lowerYBoundXDiff);
                    framePhyxels.push_back(std::make_pair(x, lowerYBoundXDiff));
                    if (curphx.getIsRigidFree() && curphx.material->type<=PHX_MTYPE_LIQ) {
                        sumOfMasses += curphx.material->mass;
                        ++numOfFramePhyxels;
                    }
                }
            }
            for (unsigned y = lowerYBound[x-x_left]; y <= upperYBound[x-x_left]; ++y) {
                if (y < PHX_SCENE_SIZE_Y) {
                    coveredPhyxels.push_back(std::make_pair(x, y));
                    auto & phyx = (*scene)(x, y);
                    phyx.isRigidFree = false;
                    phyx.coveringRigid = this;
                    if (phyx.material->type == PHX_MTYPE_LIQ) { // for liquids only
                        scene->addVirtualParticle( // we pop the phyxel out as a virtual particle
                            phyx.material,
                            phyx.color,
                            x, y,
                            1, 1
                        );
                        scene->setMaterial(x, y, scene->defaultMaterial, 0); // set vacuum
                    }
                }
            }
            {
                unsigned upperYBoundXDiff = upperYBound[x-x_left]+1;
                if (upperYBoundXDiff < PHX_SCENE_SIZE_Y) {
                    auto& curphx = (*scene)(x, upperYBoundXDiff);
                    framePhyxels.push_back(std::make_pair(x, upperYBoundXDiff));
                    if (curphx.getIsRigidFree() && curphx.material->type<=PHX_MTYPE_LIQ) {
                        sumOfMasses += curphx.material->mass;
                        ++numOfFramePhyxels;
                    }
                }
            }
        }
    }
    { //
        auto x_right_1 = x_right + 1;
        if (x_right_1 < PHX_SCENE_SIZE_X) {
            for (unsigned y = lowerYBound[x_right-x_left]; y <= upperYBound[x_right-x_left]; ++y) {
                if (y < PHX_SCENE_SIZE_Y) {
                    auto& curphx = (*scene)(x_right_1, y);

                    framePhyxels.push_back(std::make_pair(x_right_1, y));
                    if (curphx.getIsRigidFree() && curphx.material->type<=PHX_MTYPE_LIQ) {
                        sumOfMasses += curphx.material->mass;
                        ++numOfFramePhyxels;
                    }
                }
            }
        }
    } //

    if (numOfFramePhyxels) {
        backgroundMaterialMass = sumOfMasses / numOfFramePhyxels;
    } else {
        backgroundMaterialMass = 0;
    }

    #if PHX_ENABLE_BURNING
    updateBurning();
    #endif
}

void RigidBody::phyxelate() {
    this->toDelete = true;
    for (auto & c : this->coveredPhyxels) {
        auto& phyx = (*scene)(c.first, c.second);
        if (phyx.material->type >= PHX_MTYPE_POD) continue;
        if (!(phyx.material->isRemovable)) {
            scene->addVirtualParticle( // we pop the phyxel out as a virtual particle
                phyx.material,
                phyx.color,
                c.first, c.second,
                1, 1
            );
        }
        scene->setMaterial(c.first, c.second, this->material, rand());
    }
}

#if PHX_ENABLE_BURNING
void RigidBody::updateBurning() {
    bool res;
    if (material->isFlammable) {
        int rnd = rand();
        switch(burningStage) {
            #if PHX_ENABLE_TEMPERATURE
            case 0: // not burning
            if(temperature >= material->burningTemperature) {
                burningStage = 1;
                burningTimer = 10;
                return;
            }
            break;
            #endif

            case 1: // ready to burn but no oxygen nearby
            res = 1;
            for(const auto& pair : framePhyxels) {
                MaterialObj* nbrMat;
                uint8_t* nbrStage;
                auto& nbr = (*scene)(pair.first, pair.second);
                if (nbr.isRigidFree) {
                    nbrMat = &nbr.material;
                    nbrStage = &nbr.burningStage;
                } else {
                    auto* nbrRigid = nbr.coveringRigid;
                    nbrMat = &nbrRigid->material;
                    nbrStage = &nbrRigid->burningStage;
                }
                if((*nbrMat)->containsO2) {
                    burningStage = 2;
                    burningTimer = 10;
                    return;
                }
                if(res&&(*nbrStage)==2) {
                    res = 0;
                    burningTimer = 10;
                }
            }
            if (!res) return;
            burningStage = ((--burningTimer)!=0);
            break;

            default: // burning
            for (auto& pair : coveredPhyxels) {
                material->customBurning(scene, pair.first, pair.second);
            }
            #if PHX_ENABLE_TEMPERATURE
            temperature += PHX_BURNING_HEAT;
            #endif
            res = (rnd%9) == 0; // res=1 means we haven't found oxygen yet
            for (const auto& pair : framePhyxels) {
                auto& nbr = (*scene)(pair.first, pair.second);
                MaterialObj* nbrMat;
                uint8_t* nbrStage;
                time_t* nbrTimer;
                if (nbr.isRigidFree) {
                    nbrMat = &nbr.material;
                    nbrStage = &nbr.burningStage;
                    nbrTimer = &nbr.burningTimer;
                } else {
                    auto* nbrRigid = nbr.coveringRigid;
                    nbrMat = &nbrRigid->material;
                    nbrStage = &nbrRigid->burningStage;
                    nbrTimer = &nbrRigid->burningTimer;
                }

                if ((*nbrMat)->isFlammable && !*nbrStage) {
                    *nbrStage = 1;
                    *nbrTimer = 10;
                }
                if (res && (*nbrMat)->containsO2) {
                    res = 0;
                    burningTimer = 10;
                    nbr.setMaterial(material->burningGas, rnd);
                }
            }

            if (!res) {
                if (burningStage == material->burningFrames) {
                    burningStage = 0;
                    material = material->burningGas;
                    phyxelate();
                } else ++burningStage;
                return;
            }
            if((--burningTimer)==0) {
                burningStage = 0;
                material = material->burntState;
                phyxelate();
                // setMaterial(index_cur, material->burntState, rnd);
            }
            break;
        }
    }
}
#endif

RigidJoint::RigidJoint(b2World* box2DLayer, RigidBody* b1, RigidBody* b2):
    box2DLayer(box2DLayer), firstBody(b1), secondBody(b2) { // use linkRigidBodies for the sake of safety and stability, it has more chance to be included in the future
    b2WeldJointDef weldJointDef;
    auto joint_vector = b1->box2DBody->GetPosition() - b2->box2DBody->GetPosition();
    double sn = sin(b2->box2DBody->GetAngle());
    double cs = cos(b2->box2DBody->GetAngle());
    weldJointDef.bodyA = b1->box2DBody;
    weldJointDef.bodyB = b2->box2DBody;
    weldJointDef.localAnchorA.Set(0.0f, 0.0f);
    weldJointDef.localAnchorB.Set(
        joint_vector.x * cs + joint_vector.y * sn,
        -joint_vector.x * sn + joint_vector.y * cs
    );
    weldJointDef.referenceAngle = b2->box2DBody->GetAngle() - b1->box2DBody->GetAngle();
    box2DJoint = box2DLayer->CreateJoint(&weldJointDef);
    b1->joints.push_back(this);
    b2->joints.push_back(this);
}

RigidJoint* linkRigidBodies(RigidBody* b1, RigidBody* b2) { // links two rigid bodies into a single physical instance
    if (b1->box2DLayer != b2->box2DLayer) throw std::string("tried to create a joint of rigids of different Box2D layers (worlds)");
    return new RigidJoint(b1->box2DLayer, b1, b2); // you can ignore this and not store this pointer
}

}
