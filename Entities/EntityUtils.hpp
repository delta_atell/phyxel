// THIS FILE IS A PART OF THE PHYXEL LIBRARY
// PHYXEL 1.3
// (c) Atell Krasnopolski, Petro Zarytskyi, MIT LICENSE

// this file has the definitions of some methods for the Entity system

#pragma once
#include "Entity.hpp"

namespace phx {

Entity::Entity(const Entity& e): scene(e.scene) {
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}
Entity::Entity(Entity&& e) noexcept: scene(e.scene) {
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}
// Entity& Entity::operator=(const Entity& e) {
//     this->scene = e.scene;
//     id = nextID;
//     scene->logEntity(this);
//     ++nextID;
//     return (*this);
// }
// Entity& Entity::operator=(Entity&& e) noexcept {
//     this->scene = e.scene;
//     id = nextID;
//     scene->logEntity(this);
//     ++nextID;
//     return (*this);
// }
Entity::Entity(Scene* scene): scene(scene) {
    id = nextID;
    scene->logEntity(this);
    ++nextID;
}

Entity::~Entity() {
    delete customFields;
    scene->unlogEntity(this);
}

#if PHX_ENABLE_WORLD_SAVE
json Entity::getAsJSON() {
    json self;
    self["id"] = id;
    self["lifetime"] = lifetime;
    self["toDelete"] = toDelete;
    self["entityType"] = "entity"; // no other known entity type by default
    if (customFields)
        self["customFields"] = customFields->getAsJSON();
    else 
        self["customFields"] = json::array();
    return self;
}
void Entity::setFromJSON(const json &self) {
    lifetime = self["lifetime"];
    toDelete = self["toDelete"];
}
template <typename MyCustomFieldsSpec>
void Entity::setCustomFieldsFromJSON(const json &custom_fields) {
    if (!custom_fields.empty()) {
        if (!customFields)
            customFields = new MyCustomFieldsSpec();
        customFields->setFromJSON(custom_fields);
    }
}
template<>
void Entity::setCustomFieldsFromJSON<void>(const json &custom_fields) {
    if (!custom_fields.empty() && customFields) 
        customFields->setFromJSON(custom_fields);
}
#endif 

};